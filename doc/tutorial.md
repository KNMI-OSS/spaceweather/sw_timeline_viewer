KNMI Space Weather Back-Office Data Store Tutorial
==================================================

Introduction
============

This document is a tutorial to start working with the the KNMI Space Weather Back-Office Data Store.

Tutorial
========

Checkout from repository
------------------------

```
$ mkdir tutorial
$ cd tutorial
$ git clone git@gitlab.com:KNMI/rd/projects/spaceweather/space-weather-backoffice-data-store.git
$ git clone git@gitlab.com:KNMI/rd/projects/spaceweather/space-weather-backoffice-viewer.git
```

Initialize the data store
-------------------------

Setup the configuration file and the datasets.

```
$ cd space-weather-backoffice-data-store
$ cp datastore.cfg.example datastore.cfg
$ vi datastore.cfg
$ datastore/datastore.py
2021-06-21 10:35:28 [I] Adding dataset: rdataset_solar_wind_mag_ace.json
2021-06-21 10:35:28 [I] Adding dataset: rdataset_kp_index_forecast_metoffice.json
2021-06-21 10:35:28 [I] Adding dataset: rdataset_xray_flux_rt.json
2021-06-21 10:35:28 [I] Adding dataset: rdataset_kp_index_forecast_all_metoffice.json
2021-06-21 10:35:28 [I] Adding dataset: rdataset_solar_wind_plasma_rt.json
2021-06-21 10:35:29 [I] Adding dataset: rdataset_kp_index_forecast_all_noaa.json
2021-06-21 10:35:29 [I] Adding dataset: rdataset_solar_wind_plasma_ace.json
2021-06-21 10:35:29 [I] Adding dataset: rdataset_kp_index.json
2021-06-21 10:35:29 [I] Adding dataset: rdataset_solar_wind_plasma_dscovr.json
2021-06-21 10:35:29 [I] Adding dataset: rdataset_solar_wind_mag_dscovr.json
2021-06-21 10:35:29 [I] Adding dataset: rdataset_xray_flux_goes16.json
2021-06-21 10:35:30 [I] Adding dataset: rdataset_solar_wind_mag_rt.json
2021-06-21 10:35:30 [I] Adding dataset: rdataset_kp_index_forecast_noaa.json
2021-06-21 10:35:30 [I] Adding dataset: rdataset_xray_flux_goes17.json
$ cd ..
```

Start the server
----------------

```
$ cd space-weather-backoffice-viewer
$ ./sw_viewer.py &

KNMI Space Weather Back-Office Data Server
version 0.0.2

Loading configuration from ../space-weather-backoffice-data-store/datastore/datastore.cfg
Using database ../space-weather-backoffice-data-store/datastore.db
Starting automatic Datastore update

Starting API server on http://pc190612.knmi.nl:9000
Stop server using: kill 1102375

Logging now goes to sw_viewer.log

$ cd ..
```

Check the server
----------------

```
$ curl http://pc190612.knmi.nl:9000/hapi/capabilities
{
    "outputFormats": [
        "csv",
        "json"
    ],
    "HAPI": "2.1.1",
    "status": {
        "code": 1200,
        "message": "OK"
    }
}

```
