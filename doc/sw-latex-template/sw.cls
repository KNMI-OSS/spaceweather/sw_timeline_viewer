\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{sw-latex-template/sw}[2020/12/01 SW official document class]

% Written by Maarten Sneep (maarten.sneep@knmi.nl), 2006, 2011.
% Updated and revised by Antje Ludewig (antje.ludewig@knmi.nl), 2012, 2013
% Adapted for OMI by Antje Ludewig in 2019
% Adapted for SW by Mark ter Linden in 2020


%%%%%%Revision%%%% %
%%% For clarity the revised version of this class first loads all packages and then sets the values. This is relevant for the hyperref package.
%%%%%%%%%%%%%%%%%%%%


\LoadClass{article}

%: Support packages
\RequirePackage{ifthen} % some basic packages
\RequirePackage{forloop} % easy booleans
\RequirePackage{xkeyval}  % to declare options
\RequirePackage{ifpdf} % reliable checking for pdftex or tex.
\RequirePackage{graphicx,color} % for the logo and other images and setting colors.
\RequirePackage[table]{xcolor}
\RequirePackage{colortbl} % colored cells in tables
\RequirePackage{fancyhdr} % headers and footers
\RequirePackage{fnpara} % running footnotes
\RequirePackage{lastpage} % obtain a reference to the last page in the document.
\RequirePackage{pdflscape} % use  pdflscape to rotate pages, together with longtable rotated long tables can be produced

\RequirePackage{array} % make the row-height in tables a little bit larger
\RequirePackage{multicol} % multicolumn
\RequirePackage[T1]{fontenc} % Input, output, fonts
\RequirePackage[utf8]{inputenc}
\RequirePackage{mathptmx} % load times package for math, helvetica for body.
\RequirePackage[scaled]{helvet}
\RequirePackage{xspace} % To get empty space depending what's cswng next when using macros. in tools package
\RequirePackage{upgreek}  % Provides with $\upmu$ an upright \mu 
\RequirePackage{textcomp}  % more symbols and typestting
\RequirePackage{gensymb}  % Provides degree sign with \degree and \celsius for degree celsius
\RequirePackage{pifont} % Postscript ZipfDingbats font, \ding{number} gives the symbol, see wikipedia for a list
\RequirePackage{microtype} % Since we rely on pdflatex, we might as well use microtype for sublte improvements of the layout.
\RequirePackage[USenglish]{babel} %: Hyphenation language US English
\RequirePackage{booktabs}   %gives nice tables.
\RequirePackage{dcolumn}    % align table columns at the decimal point, part of tools package
\RequirePackage{longtable}  %multipage tables (TODO: verify capwidth (might be for a rotated longtable).
\RequirePackage{geometry} % page layout and sizing
\RequirePackage{afterpage} % Provides command to flush floats (e.g. figures): \afterpage{\clearpage}
\RequirePackage{hyphenat}
\RequirePackage{amsmath,amssymb} % include amsmath for better math typesetting
\RequirePackage{url,sw-latex-template/doi} %Typeset URL and doi fields.
\RequirePackage[american,iso]{isodate} % set the format of the \today command to YYYY-MM-DD
\RequirePackage{mdwlist,paralist} % for compact lists.
\RequirePackage{rotating} % for the sideways figures
\RequirePackage{enumerate} % Better enumeration (less spacious, ironically)
\RequirePackage{enumitem} % More options for enumeration,itemize and description
\RequirePackage[titles]{tocloft} % Easy formatting of list of figures/tables/table of contents.
\RequirePackage{caption} %: Caption format
\RequirePackage{lineno} % for numbering lines
\RequirePackage{makeidx}
\RequirePackage{datetime}
% for hyperlinks (internally and externally)
\RequirePackage{hyperref}
%%%%%%%%%%%%%
%%DO NOT LOAD ANY PACKAGES AFTER HYPERREF!

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%From here on values for the varios packages and styles are set.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% Number TBx items
\newboolean{swNumberTBD}
\setboolean{swNumberTBD}{true}

% line numbers
\newboolean{swNumberLines}
\setboolean{swNumberLines}{false}

%copyright statement
\newboolean{copyrightKNMI}
\setboolean{copyrightKNMI}{true}

% stuff all options in the documentclass, but catch the 10pt and 12pt
% size options, we only allow 11 pt and no landscape.


\DeclareOptionX{11pt}[]{\ClassWarning{sw}{The OMI documents use 10pt only.}}
\DeclareOptionX{12pt}[]{\ClassWarning{sw}{The OMI documents use 10pt only.}}
\DeclareOptionX{landscape}[]{\ClassWarning{sw}{The OMI documents are all in portrait mode.}}
\DeclareOptionX{a4paper}[]{\PassOptionsToPackage{a4paper}{geometry}\PassOptionsToClass{a4paper}{article}}
\DeclareOptionX{letterpaper}[]{\PassOptionsToPackage{letterpaper}{geometry}\PassOptionsToClass{letter}{article}}
\DeclareOptionX{numtbd}[]{\setboolean{swNumberTBD}{true}}
\DeclareOptionX{linenumbers}[]{\setboolean{swNumberLines}{true}}
\DeclareOptionX{copyrightCC}[]{\setboolean{copyrightKNMI}{false}}

% all non-captured class option goes straight to the article class.
\DeclareOption*{%
	\PassOptionsToClass{\CurrentOption}{article}%
}

% Set the defaults and process
\PassOptionsToClass{10pt,oneside}{article}
\ProcessOptionsX*\relax

\ifpdf
% The OMI logo is in PDF version 1.5 %CHECK
% By default pdflatex produces version 1.4, and complains about version 1.5
% by setting this, we also produce 1.5, and silence the complaint as a side effect.
\pdfoptionpdfminorversion=5
\fi

% set default typeface to sans (helvetica)
\renewcommand*\familydefault{\sfdefault}

\setlength{\LTcapwidth}{\textheight} % longtable capwidth
\setlength{\LTleft}{0pt}
\setlength{\LTright}{0pt}

\isodate   % set the format of hte \today command to YYYY-MM-DD


% Define list of TBD items.
\newcommand{\listoftbdsname}{List of <TBx>s}
\newlistof{tbdcnt}{tbd}{\listoftbdsname}

\newcommand{\listoftbd}{%
    \ifthenelse{\value{tbdaux} > 0}{%
    \addcontentsline{toc}{section}{\listoftbdsname}%
    \listoftbdcnt%
    \clearpage}{\clearpage}}

%: Line numbers
\ifthenelse{\boolean{swNumberLines}}{\AtBeginDocument{\linenumbers}}{}

%: Count items in list
% count the number of index items in each of the indices
% store the result in the aux file, and print only if there are items in the particular index.

\newcounter{tbdaux}
\newcounter{indexcnt}
\newcounter{indexaux}
\newcounter{figureaux}
\newcounter{tableaux}

\setcounter{tbdaux}{0}
\setcounter{tbdcnt}{0}
\setcounter{indexaux}{0}
\setcounter{indexcnt}{0}
\setcounter{figureaux}{0}
\setcounter{tableaux}{0}

% write the number of index items to the aux file, and (therefore) set the aux counter upon 
% passing the \begin{document} line.

\def\sw@idxcounters{%
   \immediate\write\@auxout{\string
   \setcounter{indexaux}{\theindexcnt}}
   \immediate\write\@auxout{\string
   \setcounter{tbdaux}{\thetbdcnt}}
   \immediate\write\@auxout{\string
   \setcounter{figureaux}{\thefigure}}
   \immediate\write\@auxout{\string
   \setcounter{tableaux}{\thetable}}}
   
% make sure to store the final counter value at the end of the doc.
\AtEndDocument{\sw@idxcounters}

%: Index
% enable the general index
\makeindex

% This index uses the standard (two column layout).
\newcommand{\printGeneralIndex}{\ifthenelse{\value{indexaux} > 0}{%
 \clearpage  \phantomsection \addcontentsline{toc}{section}{Index}
\printindex }{}}

% support commands, increment the counters as well.
\newcommand{\idx}[1]{\index{#1}\addtocounter{indexcnt}{1}}


\DeclareCaptionFormat{sw}{\parbox{\textwidth}{\textbf{#1}#2#3}}
\captionsetup[figure]{format=sw}
\captionsetup[table]{format=sw}

%: Page layout
% switch to normal size to get all relative measurements correct.
\normalsize

% page size & margins, including the heading.
\geometry{includehead,portrait,bindingoffset=0pt,
	left=25.0mm,right=25.0mm,bottom=25.0mm,top=15.0mm,
	headheight=2\baselineskip, headsep=\baselineskip}

% add extra space to tables
% \setlength{\extrarowheight}{3pt}
% increase spacing in tables.
\renewcommand{\arraystretch}{1.2}

%: Title business
% default title is "Untitled"
\newcommand*{\sw@title}{\textit{Untitled}}
\newcommand*{\sw@shorttitle}{\textit{Untitled}}

% The title command has an optional argument.
% re-create the title command. Bluntly overwrite the old one with \def
\def\title{\sw@set@title}

% the real new version, which eats the parameters. Allow for an optional command
% to set the short title (used in hte page headings).
\newcommand*{\sw@set@title}[2][]{%
\ifthenelse{\equal{#1}{}}{%
	\renewcommand*{\sw@title}{#2}%
	\renewcommand*{\sw@shorttitle}{#2}%
}{%
	\renewcommand*{\sw@title}{#2}%
	\renewcommand*{\sw@shorttitle}{#1}%
}}

%: Page header
% set page header, see the fancyhdr documentation for details.
% top-left: title//issue number, date, status.
\lhead{\begin{minipage}[t]{0.65\textwidth}\raggedright%
        \protect\sw@shorttitle\\%
        version \protect\printCurrentIssue{}, \protect\printAuthoredOnDate{}
       \end{minipage}
}

% center-top: empty
\chead{}

% top-right: document number//page number
\rhead{%
	\begin{minipage}[t]{0.35\textwidth}\raggedleft%
		% Document number
		\ifthenelse{\equal{\OMIdocumentNumber}{}}{%
			\textbf{AURA-OMI-KNMI-L01B-????-??}%
		}{%
			\OMIdocumentNumber%
		}\\ %
		% Page number and last page
		Page \thepage{} of \pageref{LastPage}
	\end{minipage}%
}

% nothing on the bottom of the page
\lfoot{}
\cfoot{}
\rfoot{}

% width of the lines between body & heading or footer.
\renewcommand{\footrulewidth}{0pt}
\renewcommand{\headrulewidth}{0.7pt}

% actually use the page-style.
\pagestyle{fancy}


% use this command to set the author in the pdf info 
% 
% Usage: \Prepared{Maarten Sneep}
\newcommand*{\aaca@Prepared}{n/a}
\newcommand*{\Prepared}[1]{\renewcommand{\aaca@Prepared}{#1}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright disclaimer and general disclaimer %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%% Copyright KNMI
\newcommand{\CopyRightKNMI}{\vspace*{0.0cm} 
\vfill
\begin{center}
\hrule \vspace{0.3cm}
{\large\textbf{Copyright statement}}
\end{center}
All rights reserved. Disclosure to third parties of this document or any part thereof, or 
the use of any information contained therein for purposes other than provided for by this document, 
is not permitted, except with the prior and express written permission of the Royal Netherlands Meteorological Institute KNMI.
\hspace{0pt} \vspace{8pt}
\hrule
}%
%%% Creative Commons license 
\newcommand{\CopyRightCreativeCommons}{
\vfill
\begin{center}
\hrule \vspace{0.3cm}
{\large\textbf{Copyright statement}}\\
This work is licensed under \\the Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License.\\ To view a copy of this license, visit \url{http://creativecommons.org/licenses/by-nc-nd/3.0/}\\
or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.\\%[0.06cm]
\includegraphics[height=0.6cm]{sw-latex-template/creative_commons_logo1.pdf} 
%\vspace{8pt}
%\hrule
\end{center}
\hspace{0pt} \vspace{-15pt}
\hrule
}%
%%%Disclaimer text, if there is more than one person to check the document, the disclaimer is put on a new page.
\newcommand{\Disclaimer}{%
%%%%
%\vfill 
 \begin{center}
\hrule \vspace{0.3cm}
{\large\textbf{Disclaimer}}
\end{center}
The Royal Netherlands Meteorological Institute KNMI does not represent or endorse the accuracy or  
reliability of any of the information or content (collectively the ``Information'') contained in this  
document. The reader hereby acknowledges that any reliance upon any Information shall be at the reader's sole risk. 
The Royal Netherlands Meteorological Institute KNMI reserves the right, in its sole discretion and without 
any obligation, to make improvements to the Information or correct any error or swssions in any portion of the Information.
\begin{center}
\vspace{-12pt}
{\noindent\rule{0.4\textwidth}{0.4pt}}
\vspace{-4pt}
\end{center}
THE INFORMATION IS PROVIDED BY THE ROYAL NETHERLANDS METEOROLOGICAL INSTITUTE KNMI ON AN ``AS IS'' BASIS, 
AND THE ROYAL NETHERLANDS METEOROLOGICAL INSTITUTE KNMI EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES, EXPRESS OR IMPLIED,
INCLUDING WITHOUT LIMITATION WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE, WITH RESPECT TO THE INFORMATION. 
IN NO EVENT SHALL THE ROYAL NETHERLANDS METEOROLOGICAL INSTITUTE KNMI BE LIABLE FOR ANY DIRECT, INDIRECT, 
INCIDENTAL, PUNITIVE, OR CONSEQUENTIAL DAMAGES OF ANY KIND WHATSOEVER WITH RESPECT TO THE INFORMATION.
\hspace{0pt} \vspace{8pt}
\hrule
}
%%Choosing the right copyright text depending on template option copyrightCC
\newcommand{\CopyRight}{\ifthenelse{\boolean{copyrightKNMI}}{\CopyRightKNMI}{\CopyRightCreativeCommons}\Disclaimer}

%: Moving labels
% the document number
% default to "AURA-OMI-KNMI-L01B-????-??"
\newcommand*{\OMIdocumentNumber}{AURA-OMI-KNMI-L01B-????-??}
\newcommand*{\documentnumber}[1]{\renewcommand*{\OMIdocumentNumber}{#1}}

%: Issue number
% Keep track of the current issue.
% issue number consists for OMI of the svn number that needs to be generated with the makefile 
%
\newcommand*{\printCurrentIssue}{\textit{SVN number}}
\newcommand*{\setCurrentIssue}[1]{\renewcommand*{\printCurrentIssue}{#1}}

%: Date printing. 
% A placeholder for the document date
\newcommand*{\@uthoredOnDate}{\today}
\newcommand*{\Date}[1]{\renewcommand{\@uthoredOnDate}{#1}}
\newcommand*{\printAuthoredOnDate}{%
		\@uthoredOnDate%
	}
\newcommand*{\printkeywords}{}
\newcommand*{\printsubject}{}
\newcommand*{\keywords}[1]{\renewcommand*{\printkeywords}{#1}}
\newcommand*{\subject}[1]{\renewcommand*{\printsubject}{#1}}



%: Table of contents
\tocloftpagestyle{fancy}
\cftsetpnumwidth{3em}
\cftsetrmarg{3em}

\setlength{\cftsecindent}{0pt}
\setlength{\cftsubsecindent}{0pt}
\setlength{\cftsubsubsecindent}{0pt}

\setlength{\cftsecnumwidth}{15mm}
\setlength{\cftsubsecnumwidth}{15mm}
\setlength{\cftsubsubsecnumwidth}{15mm}

\setlength{\cftbeforesecskip}{3pt}

\renewcommand{\cftsecfont}{\bfseries}
\renewcommand{\cftsubsecfont}{\normalfont}
\renewcommand{\cftsubsubsecfont}{\normalfont}

\renewcommand{\cftsecpagefont}{\bfseries}
\renewcommand{\cftsubsecpagefont}{\normalfont}
\renewcommand{\cftsubsubsecpagefont}{\normalfont}

\renewcommand{\cftdotsep}{1}
\renewcommand{\cftsecleader}{\cftdotfill{\cftdotsep}}


\setlength{\cfttabindent}{0pt}
\setlength{\cfttabnumwidth}{15mm}
\renewcommand{\cfttabfont}{\normalfont}
\renewcommand{\cfttabpagefont}{\normalfont}

\setlength{\cftfigindent}{0pt}
\setlength{\cftfignumwidth}{15mm}
\renewcommand{\cftfigfont}{\normalfont}
\renewcommand{\cftfigpagefont}{\normalfont}

\setlength{\cfttbdcntindent}{0pt}
\setlength{\cfttbdcntnumwidth}{15mm}
\renewcommand{\cfttbdcntfont}{\normalfont}
\renewcommand{\cfttbdcntpagefont}{\normalfont}


\let\oldlistoffigures\listoffigures
\renewcommand{\listoffigures}{\ifthenelse{\value{figureaux} > 0}{%
\addcontentsline{toc}{section}{\listfigurename}\oldlistoffigures}{}}

\let\oldlistoftables\listoftables
\renewcommand{\listoftables}{\ifthenelse{\value{tableaux} > 0}{%
\addcontentsline{toc}{section}{\listtablename}\oldlistoftables}{}}

\setcounter{secnumdepth}{5}
\setcounter{tocdepth}{3}


%: TBD
\newcommand{\tbd@label}{TBx}
\newcommand{\TBD}{% decided
  \renewcommand{\tbd@label}{TBD}\TBx}
\newcommand{\TBW}{% 
  \renewcommand{\tbd@label}{TBW}\TBx}
\newcommand{\TBC}{% checked
  \renewcommand{\tbd@label}{TBC}\TBx}
\newcommand{\TBA}{% added
  \renewcommand{\tbd@label}{TBA}\TBx}
\newcommand{\TBE}{% edited
  \renewcommand{\tbd@label}{TBE}\TBx}
\newcommand{\Editorial}[2][]{\TBE{\textcolor{blue}{#1: #2}}}  
%\newcommand{\Editorialresolved}[2][]{\TBE{\textcolor{green}{#1: #2}}}
\newcommand{\Editorialresolved}[2][]{\TBE{\textcolor[rgb]{0,0.5,0}{#1: #2}}}
\newcommand{\TBx}[2][]{%
  \refstepcounter{tbdcnt}
  \ifthenelse{\boolean{swNumberTBD}}{%
    \def\@currentlabel{<\tbd@label\ \#\thetbdcnt}%
    \par\noindent\textbf{<\tbd@label\ \#\thetbdcnt>} #2
    \ifthenelse{\equal{#1}{}}{%
      \addcontentsline{tbd}{tbdcnt}{\protect\numberline{\thetbdcnt}#2}}{%
      \addcontentsline{tbd}{tbdcnt}{\protect\numberline{\thetbdcnt}#1}}
    }{%
    \par\noindent\textbf{<\tbd@label>} #2
  \ifthenelse{\equal{#1}{}}{%
  \addcontentsline{tbd}{tbdcnt}{\protect\numberline{}#2}}{%
  \addcontentsline{tbd}{tbdcnt}{\protect\numberline{}#1}}}\par}

%: Title page
\renewcommand{\maketitle}{%
\fancypagestyle{titlepagestyle}{
  \renewcommand{\headrulewidth}{0pt}
  \fancyhf{}
  \fancyhead[C]{\begin{picture}(-165,0)%
	\put(0,29){%
		\makebox(0,34)[t]{%
			\includegraphics[height=8\baselineskip]{sw-latex-template/IW_KNMI_Logo_online_ex_pos_en}%
		}%
	}%
	\end{picture}}}
  \thispagestyle{titlepagestyle}%
  \vspace*{50mm}%
  \par\noindent%
  \begin{minipage}{0.85\textwidth}%
    {\raggedright\bfseries\Huge \sw@title\par}%
  \end{minipage}%
  \vspace{\fill}%
  %\par\begin{center}\includegraphics[width=100mm,trim={0 0 0.3cm 0}, clip]{sw-latex-template/logo.jpg}\end{center}\par% 
  \vspace{30mm}%
  \par\noindent%
  \begin{tabular}{l@{\ \ :\ \ }l}
		document number & \OMIdocumentNumber\\
    version & \printCurrentIssue\\
    date & \printAuthoredOnDate\\
   \end{tabular}
   \clearpage
  \CopyRight
   \clearpage
}

%: PDF setup
\hypersetup{%
	colorlinks=true,
	linkcolor=black,
	anchorcolor=black,
	citecolor=black,
	filecolor=black,
	menucolor=black,
	urlcolor=black,
	bookmarksopen=true,
	bookmarksnumbered=false,
	pdfpagemode=UseOutlines,
	pdfview={FitV},
	pdfstartview={FitV},
	pdfstartpage={1},
	plainpages=false}

% Add title, subject, keywords, author
\AtBeginDocument{%
\hypersetup{pdftitle = {\sw@title}, 
	pdfsubject= {\printsubject},
	pdfkeywords = {\printkeywords},
	pdfauthor={\aaca@Prepared}}
}

%: Float handling
\renewcommand{\topfraction}{.95}
\renewcommand{\bottomfraction}{.75}
\renewcommand{\textfraction}{.05}
\renewcommand{\floatpagefraction}{.7}
\renewcommand{\dbltopfraction}{.7}
\renewcommand{\dblfloatpagefraction}{.66}
\setcounter{topnumber}{9}
\setcounter{bottomnumber}{9}
\setcounter{totalnumber}{20}
\setcounter{dbltopnumber}{9}
\providecommand*{\textsubscript}[1]{\ensuremath{_{\text{#1}}}}



\newenvironment{Requirement}[1]{\noindent\begin{list}{}{%
  \setlength{\labelwidth}{35mm}%
  \setlength{\labelsep}{5mm}%
  \setlength{\itemindent}{0pt}%
  \setlength{\leftmargin}{40mm}%
  \setlength{\rightmargin}{20mm}%
  \let\makelabel\requirementlabel%
  \def\@currentlabel{#1}%
  \item[#1]\ignorespaces\raggedright\ttfamily}}{\end{list}\par\vspace{\baselineskip}%
	\aftergroup\@afterindentfalse%
	\aftergroup\@afterheading}

\newcommand{\requirementlabel}[1]{\ttfamily #1}

%: Math
\DeclareMathOperator{\median}{median}
\DeclareMathOperator{\minimize}{min}

% Macros for getting and setting a parameter / value pair; used in datadictionary related output

\newcommand{\getparametervalue}[2]{%
    \@ifundefined{parametername@#1@#2}{%  
        \errmessage{Unknown parameter #1 #2}
    }{%
        \@nameuse{parametername@#1@#2}%
    }%
}

\newcommand{\setparametervalue}[3]{%
    \global\@namedef{parametername@#1@#2}{#3}%
}

% Set PDF minor version to avoid warnings about PDF versions for included graphics.
\pdfminorversion=6

\endinput

