KNMI Space Weather Back-Office Data Store API-r
===============================================

Introduction
============

This document describes the KNMI Space Weather Back-Office Data Store
Application Programmer’s Interface with resample capabilities.

This API is an extension of the Heliophysics Application Programmer’s Interface ([HAPI](https://github.com/hapi-server/data-specification)).
While HAPI is a data access interface only, this API has full Create/Read/Update/Delete
functionallity based on a REST web interface.

The following table shows both the HAPI and API methods and endpoints:

| HAPI-r | API-r | Description |
| --- |---| ---|
| GET http://server/hapi-r/capabilities | GET http://server/api-r/capabilities | Get capabilities of server |
| GET http://server/hapi-r/catalog | GET http://server/api-r/datasets | Get list of datasets |
| - |  POST http://server/api-r/datasets | Add dataset |
| GET http://server/hapi-r/info | GET http://server/api-r/dataset | Get properties of dataset |
| - | POST http://server/api-r/dataset | Add data |
| - | PUT http://server/api-r/dataset | Update data |
| - | DELETE http://server/api-r/dataset | Delete dataset |
| GET http://server/hapi-r/data | GET http://server/api-r/data | Get data |
| - | DELETE http://server/api-r/data | Delete data |

Requests can be made using the HTTP protocol and the GET/POST/PUT/DELETE methods.

In case of GET and DELETE, parameters are given as a query string appended to the endpoint URL.

In case of POST and PUT, the body should be a JSON object ([RFC-7159](https://tools.ietf.org/html/rfc7159)).

The HTTP response has a status code and in most cases a JSON object in the body, except when requesting to get data in CSV format.

The HTTP response status codes are listed at the end of this document.

Resampling
==========

The resampling capability allows quick access data of large time periods while still keeping the possibility to access data at the original temporal resolution.
This is very useful for visualisation purposes.
Resampling is done at two moments: while ingesting the data into the database, and when retrieving the data from the database.

For example: suppose there is a dataset of 10 years with data points at every minute. This dataset has 5,256,000 data points.
If a user wants to visualize the whole 10 years, first the more than 5 million data points needs to be retrieved from the database, then send to the user and then put in a chart.

In most cases, a users display only allows to show a few thousand data points. Let's say the user can show 2000 points, then the temporal resolution would be 2628 minutes (5256000/2000).
If the data is stored not only in the original 1 minute resolution, but also in 30 minutes and 720 minutes, then in this case, the 720 minute resolution can be used and only 7300 data points need to be retrieved from the database.
This smaller data set can then be resampled during the retrieval and only 2000 data points will be send to the user.

This method of resampling saves a considerable time when getting the data from the database, and when sending the data to the user via the API.

Endpoints
=========

GET http://server/api/datasets
------------------------------

List the datasets available on this server.

**Request Parameters**

None

**Response**

Datasets response object, see example.

**Example**

```
$ curl http://pc190612.knmi.nl:9000/api/datasets
{
    "datasets": [
        {
            "id": "solar_wind_plasma",
            "title": "Real time solar wind plasma"
        }
    ],
    "status": {
        "code": 1200,
        "message": "OK"
    }
}
```

POST http://server/api/datasets
-------------------------------

Add a dataset to the server.

**Request Parameters**

Dataset object, see example.

**Response**

Status object, see example.

**Example**

```
$ cat solar_wind_plasma.json
{
    "id": "solar_wind_plasma",
    "startDate": "",
    "stopDate": "",
    "timeStampLocation": "begin",
    "cadence": "PT1M",
    "sampleStartDate": "",
    "sampleStopDate": "",
    "description": "Real time solar wind plasma",
    "resourceURL": "https://services.swpc.noaa.gov/products/solar-wind/",
    "resourceID": "NOAA Space Weather Prediction Center",
    "creationDate": "",
    "modificationDate": "",
    "contact": "",
    "contactID": "",
    "parameters": [
        {
            "name": "time",
            "type": "isotime",
            "units": "UTC",
            "fill": "",
            "description": "Time",
            "label": "",
            "key": true
        },
        {
            "name": "density",
            "type": "double",
            "units": "1/cm3",
            "fill": null,
            "description": "Density",
            "label": "",
            "key": false
        },
        {
            "name": "speed",
            "type": "double",
            "units": "km/s",
            "fill": null,
            "description": "Speed",
            "label": "",
            "key": false
        },
        {
            "name": "temperature",
            "type": "double",
            "units": "K",
            "fill": null,
            "description": "Temperature",
            "label": "",
            "key": false
        }
    ]
}

$ curl -X POST -H "Content-Type: application/json" -d @solar_wind_plasma.json http://pc190612.knmi.nl:9000/api/datasets
{
    "status": {
        "code": 1200,
        "message": "OK"
    }
}
```

GET http://server/api/dataset
-----------------------------

Get information about a dataset.

**Request Parameters**

| Name       | Description                                                       |
|------------|-------------------------------------------------------------------|
| id         | **Required** The identifier of the dataset.                       |

**Response**

Dataset object, see example.

**Example**

```
$ curl http://pc190612.knmi.nl:9000/api/dataset?id=solar_wind_plasma
{
    "id": "solar_wind_plasma",
    "startDate": "",
    "stopDate": "",
    "timeStampLocation": "begin",
    "cadence": "PT1M",
    "sampleStartDate": "",
    "sampleStopDate": "",
    "description": "Real time solar wind plasma",
    "resourceURL": "https://services.swpc.noaa.gov/products/solar-wind/",
    "resourceID": "NOAA Space Weather Prediction Center",
    "creationDate": "2021-05-19T20:50:11Z",
    "modificationDate": "",
    "contact": "",
    "contactID": "",
    "parameters": [
        {
            "name": "time",
            "type": "isotime",
            "length": "",
            "units": "UTC",
            "fill": "",
            "description": "Time",
            "label": ""
        },
        {
            "name": "density",
            "type": "double",
            "length": "",
            "units": "1/cm3",
            "fill": null,
            "description": "Density",
            "label": ""
        },
        {
            "name": "speed",
            "type": "double",
            "length": "",
            "units": "km/s",
            "fill": null,
            "description": "Speed",
            "label": ""
        },
        {
            "name": "temperature",
            "type": "double",
            "length": "",
            "units": "K",
            "fill": null,
            "description": "Temperature",
            "label": ""
        }
    ],
    "status": {
        "code": 1200,
        "message": "OK"
    }
}
```

POST http://server/api/dataset
------------------------------

Add data to a dataset. If data with the same key is already present, it will not be overwritten.

**Request Parameters**

Dataset object, see example.

**Response**

Status object, see example.

**Example**

```
$ cat data.json
{
    "id": "solar_wind_plasma",
    "parameters": ["time", "density", "speed", "temperature"],
    "data": [
        ["2021-05-12T21:09:00Z", 3.77, 514.7, 197529.0],
        ["2021-05-12T21:10:00Z", 4.23, 518.0, 217495.0],
        ["2021-05-12T21:11:00Z", 3.55, 512.2, 199368.0],
        ["2021-05-12T21:12:00Z", 3.63, 513.7, 209994.0],
        ["2021-05-12T21:13:00Z", 3.64, 511.6, 212365.0],
        ["2021-05-12T21:14:00Z", 3.35, 499.3, 167362.0],
        ["2021-05-12T21:15:00Z", 3.54, 519.1, 146608.0],
        ["2021-05-12T21:16:00Z", 3.53, 518.5, 155269.0]
    ]
}

$ curl -X POST -H "Content-Type: application/json" -d @data.json http://pc190612.knmi.nl:9000/api/dataset
{
    "status": {
        "code": 1200,
        "message": "OK"
    }
}
```

PUT http://server/api/dataset
-----------------------------

Add or update data in a dataset. If data with the same key is already present, it will be overwritten.

**Request Parameters**

Dataset object, see example.

**Response**

Status object, see example.

**Example**

```
$ cat data.json
{
    "id": "solar_wind_plasma",
    "parameters": ["time", "density", "speed", "temperature"],
    "data": [
        ["2021-05-12T21:09:00Z", 3.77, 514.7, 197529.0],
        ["2021-05-12T21:10:00Z", 4.23, 518.0, 217495.0],
        ["2021-05-12T21:11:00Z", 3.55, 512.2, 199368.0],
        ["2021-05-12T21:12:00Z", 3.63, 513.7, 209994.0],
        ["2021-05-12T21:13:00Z", 3.64, 511.6, 212365.0],
        ["2021-05-12T21:14:00Z", 3.35, 499.3, 167362.0],
        ["2021-05-12T21:15:00Z", 3.54, 519.1, 146608.0],
        ["2021-05-12T21:16:00Z", 3.53, 518.5, 155269.0]
    ]
}

$ curl -X PUT -H "Content-Type: application/json" -d @data.json http://pc190612.knmi.nl:9000/api/dataset
{
    "status": {
        "code": 1200,
        "message": "OK"
    }
}
```

DELETE http://server/api/dataset
--------------------------------

Delete a dataset from the server. This deletes all the data and the entries in the metadata.

**Request Parameters**

| Name       | Description                                                       |
|------------|-------------------------------------------------------------------|
| id         | **Required** The identifier of the dataset.                       |

**Response**

Status object, see example.

**Example**

```
$ curl -X DELETE http://pc190612.knmi.nl:9000/api/dataset?id=solar_wind_plasma
{
    "status": {
        "code": 1200,
        "message": "OK"
    }
}
```

GET http://server/api/data
--------------------------

Get data from a dataset.

**Request Parameters**

| Name       | Description                                                                    |
|------------|--------------------------------------------------------------------------------|
| id         | **Required** The identifier of the dataset.                                    |
| time.min   | **Required** The inclusive begin time for the data to include in the response. ISO8601 format: 'YYYY-MM-DDTHH:MM:SSZ' |
| time.max   | **Required** The exclusive end time for the data to include in the response. ISO8601 format: 'YYYY-MM-DDTHH:MM:SSZ'   |
| parameters | **Optional** List of comma separated parameter names. Default are all parameters. The time parameter is always included, also when not specified. |
| format     | **Optional** 'csv' or 'json'. Default is 'csv'.                                |
| include    | **Optional** 'brief_header' or 'header'. Default no header is included. Only for 'cvs' format. |
| resample   | **Optional** Resample the data. For example '300S' resamples the output data to 5 minute data points. |

**Response**

Data in CVS format ([RFC-4180](https://tools.ietf.org/html/rfc4180)) or data object in JSON format ([RFC-7159](https://tools.ietf.org/html/rfc7159)), see example.

**Example**

```
$ curl "http://pc190612.knmi.nl:9000/api/data?id=solar_wind_plasma&time.min=2021-05-12T21:10:00Z&time.max=2021-05-12T21:15:00Z"
2021-05-12T21:10:00Z,4.23,518,217495
2021-05-12T21:11:00Z,3.55,512.2,199368
2021-05-12T21:12:00Z,3.63,513.7,209994
2021-05-12T21:13:00Z,3.64,511.6,212365
2021-05-12T21:14:00Z,3.35,499.3,167362

$ curl "http://pc190612.knmi.nl:9000/api/data?id=solar_wind_plasma&time.min=2021-05-12T21:10:00Z&time.max=2021-05-12T21:15:00Z&parameters=time,density&include=brief_header"
time,density
2021-05-12T21:10:00Z,4.23
2021-05-12T21:11:00Z,3.55
2021-05-12T21:12:00Z,3.63
2021-05-12T21:13:00Z,3.64
2021-05-12T21:14:00Z,3.35

$ curl "http://pc190612.knmi.nl:9000/api/data?id=solar_wind_plasma&time.min=2021-05-12T21:10:00Z&time.max=2021-05-12T21:15:00Z&format=json"
{
    "parameters": [
        {
            "name": "time",
            "type": "isotime",
            "length": "",
            "units": "UTC",
            "fill": "",
            "description": "Time",
            "label": ""
        },
        {
            "name": "density",
            "type": "double",
            "length": "",
            "units": "1/cm3",
            "fill": null,
            "description": "Density",
            "label": ""
        },
        {
            "name": "speed",
            "type": "double",
            "length": "",
            "units": "km/s",
            "fill": null,
            "description": "Speed",
            "label": ""
        },
        {
            "name": "temperature",
            "type": "double",
            "length": "",
            "units": "K",
            "fill": null,
            "description": "Temperature",
            "label": ""
        }
    ],
    "data": [
        [
            "2021-05-12T21:10:00Z",
            4.23,
            518.0,
            217495.0
        ],
        [
            "2021-05-12T21:11:00Z",
            3.55,
            512.2,
            199368.0
        ],
        [
            "2021-05-12T21:12:00Z",
            3.63,
            513.7,
            209994.0
        ],
        [
            "2021-05-12T21:13:00Z",
            3.64,
            511.6,
            212365.0
        ],
        [
            "2021-05-12T21:14:00Z",
            3.35,
            499.3,
            167362.0
        ]
    ],
    "status": {
        "code": 1200,
        "message": "OK"
    }
}
```

DELETE http://server/api/data
-----------------------------

Delete data from a dataset.

**Request Parameters**

| Name       | Description                                                                    |
|------------|--------------------------------------------------------------------------------|
| id         | **Required** The identifier of the dataset.                                    |
| time.min   | **Required** The inclusive begin time for the data to include in the response. ISO8601 format: 'YYYY-MM-DDTHH:MM:SSZ' |
| time.max   | **Required** The exclusive end time for the data to include in the response. ISO8601 format: 'YYYY-MM-DDTHH:MM:SSZ'   |

**Response**

Status object, see example.

**Example**

```
$ curl -X DELETE "http://pc190612.knmi.nl:9000/api/data?id=solar_wind_plasma&time.min=2021-05-12T21:10:00Z&time.max=2021-05-12T21:13:00Z"
{
    "status": {
        "code": 1200,
        "message": "OK"
    }
}
```

Response codes
=============

| HTTP code | API code | Message                                       |
|-----------|----------|-----------------------------------------------|
| 200       | 1200     | OK                                            | 
| 200       | 1201     | OK - no data                                  | 
| 400       | 1400     | error: user input error                       | 
| 400       | 1401     | error: unknown API parameter name             | 
| 400       | 1402     | error: error in time.min                      |   
| 400       | 1403     | error: error in time.max                      | 
| 400       | 1404     | error: time.min equal to or after time.max    | 
| 400       | 1405     | error: time outside valid range               | 
| 400       | 1406     | error: unknown dataset id                     | 
| 400       | 1407     | error: unknown dataset parameter              | 
| 400       | 1408     | error: too much time or data requested        | 
| 400       | 1409     | error: unsupported output format              | 
| 400       | 1410     | error: unsupported include value              | 
| 400       | 1411     | error: out of order or duplicate parameters   | 
| 500       | 1500     | error: internal server error                  |  
| 500       | 1501     | error: upstream request error                 | 

