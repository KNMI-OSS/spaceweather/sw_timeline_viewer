# List of space weather data sources
This document was used to start further development of this tool in late 2020. A list of sources of space weather data that could/should be ingested by the back-office tool is listed here.

## Solar wind and interplanetary magnetic field measured at L1 (ACE/DSCOVR)

### Real-time data
https://services.swpc.noaa.gov/products/solar-wind/ (currently active satellite ACE or DSCOVR, ASCII table)
https://services.swpc.noaa.gov/text/ace-magnetometer.txt (ACE magnetometer, ASCII table)
https://services.swpc.noaa.gov/text/ace-swepam.txt (ACE SWEPAM, ASCII table)

### ACE science data
ftp://mussel.srl.caltech.edu/pub/ace/level2/ (HDF4 format)

### DSCOVR science data
https://www.ngdc.noaa.gov/dscovr/portal/index.html#/download/ (NetCDF format)

## Geomagnetic activity index time series based on magnetometers on ground
### Kp (planetary geomagnetic activity index)
Scale: 0-9 in steps of 1/3d
ftp://ftp.gfz-potsdam.de/pub/home/obs/kp-ap/

### Dst / Sym-H (ring current)

### AE (auroral electrojet)

## GOES time series data

### X-ray flux

### Proton flux

### Electron flux

## Solar imagery

# Python ecosystem for space weather and heliophysics
Overview paper describing the Python ecosystem for heliophysics research: ["Snakes on a Spaceship—An Overview of Python in Heliophysics"](https://agupubs.onlinelibrary.wiley.com/doi/full/10.1029/2018JA025877)
