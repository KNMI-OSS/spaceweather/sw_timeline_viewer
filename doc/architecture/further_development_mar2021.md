# Ideas for further work on space-weather-backoffice-viewer and -datastore

### Functionality to import/update data in the data store:
*	The user should be able to remove a data product over a specified time interval, or completely, from the data store, either if the data is no longer necessary (to save space), or if the data is to be replaced from a different source, or using a newer version of the ingest tool (to avoid mixing outdated and new data in the store).
*	A unified interface for data management from command-line (first), and from the front-end (secondary, nice to have)

Viewer / visualization maintenance:
*	Disconnect loading and parsing of data from drawing and redrawing the data in the Javascript code (cleaner, more maintainable code)
*	Store visualized data locally in a cache, and when panning/zooming, only request missing data from the server, like on google maps. 
-	Might require defining additional fixed zoom levels and storing these in the data store. 
-	With storage of sufficient pre-defined zoom levels, on-the-fly calculation of running-average data is no longer needed, and this would make the cache more efficient.
-	Requires cache management code in javascript (pasting together parts of the time series that were separately requested, while also retaining the option of having gaps in the cached time series)
-	The client could request and store a bit more data beyond what can currently be visualized, to further help with smooth panning and zooming.
-	Option to have the subplot’s y-axis scale automatically adjust based on the min/max of the currently displayed data, so that data during extreme events do not fall beyond the edge of the plot. 
-	Perhaps for each row, allow defining a limited number of default y-axis min/max values between which to switch based on the currently viewed min/max value in the data. 
-	For IMF data, min/max would always be symmetric around 0 (e.g. -20/20, -50/50, -100/100, …)
-	For other parameters, like X-ray flux, sw density/speed, the minimum can be fixed, but the maximum can be dynamic (e.g. for speed: 200/600, 200/1000, 200/1500, …)
*	If the user changes the size of the browser window or rotates their mobile device from portrait to landscape, the contents should automatically be rescaled accordingly (recompute the canvas size and redraw the entire canvas)
*	An option can be added so that after a predefined period without any user interaction (e.g. 5 minutes), the visualization returns to a predefined state 
-	e.g. zoomed and panned to the most recent real-time data – for operational use in the back-office
-	or zoomed and panned to a set event of interest that can be configured from the back-office – for a sort of kiosk mode.

## Interface to select data to plot:
*	An interface to configure which data is plotted, and how it is plotted, can first be implemented in the form of a .json configuration file. Later, a visual interface can be added that allows the editing of this configuration from the front-end client.
*	The data sources visual interface in the front-end client should normally be hidden, and only appear when the user presses a small button.
-	For good examples of fairly complex data visualization web interfaces, see e.g. https://earth.nullschool.net, https://helioviewer.org. Helioviewer works with the concept of layers that can be added and removed, which has similarities with the timeseries elements.
*	The interface should allow the user to add, remove and resize rows of subplots and •	allow the user to set properties of a row/subplot’s y-axis scale and annotation (y-min, y-max, linear/log, …). These are the properties that are currently set in the call to new_subplot_generator() in sw_viewer.html.
*	The interface should allow the user to add/remove one or more data plot elements to a row (e.g. solar wind velocity data from ACE and DSCOVR can be either added to a single row, or each to their own row).
*	The interface should allow the user to set properties of a plot element (style, line color, fill color, opacity, …). These are the properties that are currently set in the call to plot_element_timeseries() in sw_viewer.html.
*	The interface should allow the user to save/export and load/import the configuration of rows of subplots and plot elements. 
*	It should be possible to transfer the saved/exported configuration to a different machine or instance of the software.
*	Specific configurations that are suitable for mobile phone screens can be set up (Eelco can do this)

### Additional data types to store and visualize:
*	Image / image sequence / movie viewer (get rid of current SDO daily movie viewer and replace with more general viewer of images and image sequences (movies). 
-	Requires data store functionality for image sequences
-	Time indicator in time series plots should automatically progress while image sequences / movies are playing, so that timeseries and images can be studied together.
*	Simple timeseries (Eelco can do this):
-	Direct magnetometer measurements from Intermagnet
-	Dst, Sym-H indices (ring current)
*	Kp-like bars (Eelco can do this):
-	Hpo (open-ended high-cadence Kp)
*	Timeseries as filled area with lines (Eelco can do this): 
-	AE index (http://wdc.kugi.kyoto-u.ac.jp/ae_realtime/today/today.html)
*	Heat-map or coloured scatter plot (Eelco can do this):
-	In-situ satellite data with time on x-axis, latitude on y-axis and value as colour from colour map.

### Annotation in visualization (Eelco can do this):
*	Peak value annotation in text next to the peak on the timeseries graph, only when sufficiently zoomed in
*	Fix Kp annotation
-	currently annotation objects are not properly cleaned up, leading to visual glitches, and probably also performance issues
-	For high Kp values, the numbers could be drawn below the top of the bar, so that they do not fall beyond the edge of the plot.

