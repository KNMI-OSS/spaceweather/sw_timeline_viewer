This document was intended for collecting information for use as input for the further development of the KNMI space weather back-office data visualisation software, based on the [space-weather-viewer](https://gitlab.com/eelcodoornbos/space-weather-viewer/) software. This document contains information on the purpose, high level architecture and requirements of the software, that was used as a starting point for further discussion and further development that started in late 2020.

## Purpose of the software
The software will have a number of closely related purposes:
* Visualisation for real-time monitoring: To help back-office personnel to monitor the current space weather situation by presenting a visualisation of real-time observation and model data, as the data comes in. 
* Visualisation for space weather training and research: To allow space weather back-office personnel to browse through archived operational as well as scientific space weather data, using visualisations of such data that are the same as that of the real-time monitoring, in order to provide insight in data of past space weather events, as well as possible data quality issues.
* Library for space weather data research in Python: The software library that manages the storage and retrieval of data for the visualisation described above must also be usable without the visualisation part of the software, from any Python script or Jupyter notebook, for further manipulation of the space weather data for research purposes.
* External presentation: To allow for back-office personnel to make a selection of space weather visualisations, representative of current space weather or recent space weather events, to be displayed on one or more computer-connected screens in the corridors of the KNMI building. This will be done in order to increase the visibility and awareness of space weather and KNMI's role in the space weather domain. This functionality can also be used to prototype display of space weather status information for the client (government crisis coordination) on a future section of the KNMI web portal.
* Research to operations: Back-office personnel must be able to add new data parsing and visualisation elements to the software relatively quickly and easily, to test working with new data types and alternative data sources. Such new elements can be made based on the templates of already implemented elements.

## High-level architecture
The software currently consists of a back-end written in Python, and a front-end written in Javascript/HTML/CSS that runs in a user's web browser. 

### System requirements
In principle, the back-end could be installed on any machine that can run Python, including a cloud server (AWS), a KNMI Linux workstation or a personal laptop computer (running macOS, Linux or Windows). The back-end includes a data store and cache functionality, so the machine on which it is installed will require sufficient disk space for that. The client can run on any machine with a web browser, that is on a network from which an instance of the back-end is accessible via the https protocol. Front-end and back-end could (but do not have to) be run on the same machine, for instance on a personal laptop computer. In such a configuration, a fast internet connection between client and server will not be required to work with the software, although a connection between the back-end and the internet will still be needed to fetch new data into the data store from external sources.

### Back-end
The back-end consists of several components. Their functionality is briefly described here, and the current status of implementation will be further described below:
* A web server, providing communication with the front-end. The web server will perform the following functions: 
  - listen for and respond to http(s) requests from the front-end
  - serve the JavaScript/HTML/CSS code for the front-end user interface and data display,
  - serve the data that will be visualised in the front-end (for example in JSON or CSV format)
* A data store with associated data management and conversion functionality, which will perform the following functions:
  - retrieve both real-time and archived space weather data from various online data archives (e.g. NOAA, NASA, ESA web- and FTP-sites) and store it locally on the back-end machine. Within the KNMI network, the data store can also retrieve internal KNMI space weather data, for example data originating at the UK Met Office that is used in the geoweb application by forecasters in the KNMI weather room.
  - parse and translate the data from its various original formats (NetCDF, HDF, FITS, various text file formats), and store it into a cache format (e.g. a database or just Python/Pandas pickle files, that should be very quick to read and serve to the client) 
  - manipulate the cached data and store additional representations at various (time) resolutions for more efficient retrieval
  - on request, efficiently return the data for a given time interval and (time) resolution

The data store described above should be able to retrieve data in the background (regularly check if new data is available, for example via a crontab), as well as on request, for example if the front-end requests archived data that has not yet been retrieved before. In the latter case, of course the performance in terms of response time of the data store will be a lot slower than if already cached data is requested.

The data store should also be able to function as a stand-alone Python module. This allows it to be imported into a Python script or Jupyter Notebook environment, so that the data in the data store can also be used by the KNMI space weather back-office personnel for various purposes. Implementation of those scripts and Jupyter notebooks is outside the scope of the development for this project, except for one or more simple example script(s) to demonstrate the functionality. Space weather back-office personnel will in the future be able to work on new scripts or notebooks, for example to include statistical manipulation, for the making of quick plots for data exploration, as well as the making of fine-tuned presentation-ready or publication-ready plots of the data.

### Front-end
The front-end provides all the interactions with the user. Initially, it should be able to display time series data, although other types of data are also on the wish-list.

Since there are important cause- and effect relations between different space weather parameters, it is useful to have various time series visualised simultaneously in a grid with rows of panels, where each panel contains a separate time series graph, but where all graphs share a common X-axis for the time. The user should be able to freely pan and zoom in these time series, in order to focus on a time period of interest. It should be possible to have multiple of these grids available on screen at the same time (one for events at the Sun, and one for events at Earth, for example). The user should be provided with a way to organise the layout of the screen, with elements containing these time series grids, image / animation data, etc.

Initial development should be focused on time series data. Websites with core functionality already exist for browsing and manipulating solar imagery data (e.g. [Helioviewer](https://helioviewer.org/)). However, after time series are fully implemented. Solar imagery/animation data that can be linked to the time series data is high on the wish-list. It should for example be possible to view an image sequence animation of a solar flare, next to a time series graph of solar X-ray flux, with a vertical bar in the time series graph indicating the time of the currently shown image in the image sequence animation.

## Types of data
Ultimately, it would be very nice to be able to access data other than time series data from the space weather back-office software. The full list of high-level space weather data types is as follows:
* Time series data (both from observations and models). These could be either single time series, or series of vector components (e.g. for the Interplanetary Magnetic Field) or rows of fields with closely related time-series data (e.g. satellite position + satellite measurements from instruments on that satellite)
* Model data of the heliosphere, projected on slices through the (inner) solar system
* Model data (e.g. of the Earth's thermosphere-ionosphere) projected on a geographic map
* Solar imagery data (including also animations based on image sequences)

All data can have associated metadata, stored in a Python dictionary, which would include information on the source of the data, geodetic coordinates of an observatory, etc.

## Current status of implementation and need for improved architectural design
At the moment, part of the functionality of the software has been implemented as an early working proof of concept, with limitations imposed by the architecture. Further work on the project requires, as a first step, the evaluation of the current status, and a more solid definition of the architecture for going forward. 

The front-end is implemented in the form of a basic HTML page, from which Javascript functions are called that provide functionality for requesting data from the back-end and visualising it in the form of time series in the browser. The visualisation makes use of the [d3js library](https://d3js.org/), to render data in the form of Scalable Vector Graphics (SVG). The software attempts to offer a smooth zooming and panning experience on the time series data. One aspect of this is that the amount of data points being displayed is limited when zooming out on the timeline. If data is available at 1-minute cadence, but the user zooms out so that 1 pixel on the screen corresponds to about one day, the software requests daily summary statistics of the data (for instance, the daily mean), reducing the number of data points by a factor of 1440. This greatly increases the SVG (re)drawing performance in the browser and helps in keeping the application responsive to user input. When zooming back in, the interface will show progressively more detail in the data, until the 1 minute resolution is visible.
A timeline display of days for which daily solar imagery movies can be requested is currently included as well in the front-end, but this does not function well, and likely needs to be reimplemented. 

The back-end web server implemented using the Python [flask module](https://palletsprojects.com/p/flask/), that serves the HTML and javascript code, as well as the timeseries data in the form of .csv files (comma-separated value ascii files), and solar imagery movies in the form of .mp4 files.
The data store is also implemented in Python, and makes use of the [Pandas](http://pandas.pydata.org) library for data manipulation and storage, and the [requests](https://requests.readthedocs.io/en/master/) and [ftplib](https://docs.python.org/3/library/ftplib.html) libraries for accessing external data archives. However, the implementation of the data store is rather ad-hoc and does not follow a clearly defined architecture. It is in some places extremely inefficient, and therefore needs extensive work.

## Relation with the front-office geoweb software
The back-office software contains some overlapping functionality with the [geoweb software](https://gitlab.com/opengeoweb/geoweb-spaceweather), which is going to be used for space weather in the KNMI front office, in terms of visualisation of space weather time series data. In the KNMI space weather back-office, the geoweb software and associated space weather data and notifications from the Met Office can also be used for operational purposes. However, there are also important distinctions between geoweb and the development described here.

### Geoweb features not in the back-office software:
* The operational Geoweb space weather interface will only make use of managed/monitored data streams (mostly originating from the interface with the UK Met Office).
* Besides time series data, Geoweb will be able to show text of Met Office forecast bulletins, and Met Office notifications.
* Geoweb will also be the dedicated interface to work with KNMI space weather notifications and associated actions, which is not necessary in the back-office software.

### Common features, with highlighted differences in implementation:
* The back-office software will also be able to display the Met Office data used in Geoweb, as long as the back-end is run within the KNMI network, where it has access to that data store (this includes VPN access to the network). However, it will make a local copy and archive (cache) of this data for performance reasons.

### Back-office software features not in Geoweb:
* The back office software will be the dedicated place to work with long-term archived data, science data, additional/alternative data and new data sources. These data are stored on the same machine as the back-end installation of the application instance, and are managed/monitored by the administrator/user of that machine.
* Back office personnel should be able to make changes to (their instance of) the software, for example to add new types of data or change the screen layout. As much as possible, this can be done via configuration files. But back-office personnel should also be able to fork and later merge quick branches for adding new functionality for new data types.
* The data store module in the back-office software can be imported and called from any Python script or Jupyter notebook for read access and data maintenance.

# Work breakdown

## WP1: Architectural design and planning
Estimated effort: 40 hrs
### Inputs:
* The space-weather-viewer project and gitlab issues (this project, including this document)
* geoweb-spaceweather project
* data sources (described in separate gitlab issue document)

### Tasks
* Create architectural design 

### Outputs:
* Architectural design document

## WP2: Implementation of back-end data store and API
Estimated effort: 60 hrs
### Inputs:
* Architectural design document

### Tasks:
* Set up data store for timeseries data
* Set up data store for gridded model data (in heliospheric and geographic coordinates)
* Set up API for programmable access to data store, to be used in python scripts and notebooks as well as in rest of the back-end

### Outputs:
* Data store, with API and documentation

## WP3: Implementation of back-end 
Estimated effort: 60 hrs
### Inputs:
* Architectural design document
* Data store, with API and documentation

### Tasks:
* Set up web server
  - Listen for and respond to requests from the front-end
  - Serve the JavaScript/HTML/CSS code for the front-end user interface and data display
* Link web server to data store
  - Serve the data that will be visualised in the front-end (for example in JSON or CSV format) from the data store

### Output:
* Web server with documentation

## WP4: Implementation of front-end
Estimated effort: 80 hrs

### Inputs:
* Architectural design document
* Data store, with API and documentation
* Back end server

### Tasks
* Set up front-end framework
* Set up visualisation of timeseries data
* Set up visualisation of gridded data in geographic coordinates
* Set up visualisation of gridded data in heliospheric coordinates
* Set up an external presentation mode where an assigned portion of the data can be displayed on an external monitor
* Set up a way for the user to organise the layout of the front-end

### Outputs:
* Space weather back-office data visualisation software
* Software technical documentation and user manual
