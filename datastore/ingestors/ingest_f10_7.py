#!/usr/bin/env python3
from datastore.ingestors import ingest_tools
from datastore.datastore import resample_rdata
from datastore.accessors import penticton_f10

def ingest_f10_7():
    fileames = penticton_f10.download_data()
    df = penticton_f10.to_dataframe(noontime=True, drop_outliers=True,
                                    merge=True, timestamp='start')

    # Put in database
    parameters = ['time', 'f10_7']
    table_id = 'f10_7'
    data = df[parameters].values.tolist()
    ingest_tools.store(table_id, parameters, data, update=False)
    resample_rdata({'id': table_id})

if __name__ == "__main__":
    ingest_f10_7()