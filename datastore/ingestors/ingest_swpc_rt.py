#!/usr/bin/env python3
import os
import sys
import logging

import numpy as np
from datastore.ingestors import ingest_tools
from datastore.accessors import swpc_rt
from datastore.datastore import resample_rdata

allowed_datatypes = [
    'plasma',
    'mag',
    'xrays',
    'xray-flares',
    'integral-protons',
    'integral-electrons',
#    'ace_swepam',
#    'ace_mag'
]

db_ids = {
    'plasma': 'solar_wind_plasma_rt',
    'mag': 'solar_wind_mag_rt',
    'xrays': 'xray_flux_rt',
    'integral_protons': 'proton_flux_rt',
}

def store_swpc_rt_file(filename, combined=False):
    basename = os.path.basename(filename)
    logging.info(f"Reading file: {filename}")
    df = swpc_rt.json_to_dataframe(filename)
    if 'plasma' in basename:
        db_id = 'solar_wind_plasma_combined' if combined else 'solar_wind_plasma_rt'
        df['pressure'] = 1.6726e-6 * df['density'] * df['speed'] * df['speed']
        df['time'] = df.index.strftime("%Y-%m-%dT%H:%M:%SZ")
        parameters = ['time', 'density', 'speed', 'temperature', 'pressure']
        data = df[parameters].values.tolist()

    elif 'mag' in basename:
        db_id = 'solar_wind_mag_combined' if combined else 'solar_wind_mag_rt'
        df['time'] = df.index.strftime("%Y-%m-%dT%H:%M:%SZ")
        parameters = ['time', 'bt', 'bx_gsm', 'by_gsm', 'bz_gsm']
        data = df[parameters].values.tolist()

    elif 'xrays' in basename:
        db_id = 'xray_flux_combined' if combined else 'xray_flux_rt'
        # eliminate zero / invalidly low fluxes
        df['time'] = df.index.strftime("%Y-%m-%dT%H:%M:%SZ")
        df.rename({'0.05-0.4nm': 'xray_flux_short',
                   '0.1-0.8nm': 'xray_flux_long'}, axis=1, inplace=True)
        df.where(df['xray_flux_long'] > 1e-10, other=np.nan, inplace=True)
        parameters = ['time', 'xray_flux_short', 'xray_flux_long']
        data = df[parameters].values.tolist()

    elif 'xray-flares' in basename:
        db_id = 'xray_flares_rt'
        return

    elif 'integral-protons' in basename:
        db_id = 'proton_flux_rt' # no separate proton_flux_rt and proton_flux_combined tables
        times10 = df.query("(energy == '>=10 MeV')")['time_tag'].values
        flux10 = df.query("(energy == '>=10 MeV')")['flux'].values
        flux50 = df.query("(energy == '>=50 MeV')")['flux'].values
        flux100 = df.query("(energy == '>=100 MeV')")['flux'].values
        parameters = ['time', 'proton_flux_10MeV', 'proton_flux_50MeV', 'proton_flux_100MeV']
        data = np.column_stack((times10, flux10, flux50, flux100)).tolist()

    elif 'integral-electrons' in basename:
        db_id = 'electron_flux_rt'
        return
    else:
        logging.error(
            f"File type cannot be inferred from filename: {basename}."
        )

    logging.info(f"Ingesting file: {filename} into table with id: {db_id}")
    ingest_tools.store(db_id, parameters, data, update=True)


def store_swpc_rt_latest(datatypes=allowed_datatypes):
    for datatype in datatypes:
        files = swpc_rt.download_data(datatype=datatype)
        for file in files:  # Usually 1, sometimes 0 files
            store_swpc_rt_file(file, combined=False)
            #store_swpc_rt_file(file, combined=True)
    for key in db_ids:
        resample_rdata({'id': db_ids[key]})


if __name__ == "__main__":

    format = "%(asctime)s [%(levelname)s] %(message)s"
    logging.basicConfig(format=format, level=logging.INFO,
                        datefmt="%Y-%m-%d %H:%M:%S")
    arguments = sys.argv[1:]
    times = ingest_tools.parse_date_args(arguments)
    if len(arguments) == 0:
        logging.info("Ingesting all implemented real-time datatypes: "
                     f"{allowed_datatypes}")
        store_swpc_rt_latest(allowed_datatypes)
    elif times:
        logging.error(
            "Specifying a time span is not possible for SWPC real-time "
            "data streams. A datatype can be specified, to download the "
            "current real-time data, or a (list of) filename(s) can be "
            "provided to ingest those files."
        )
    else:
        for argument in arguments:
            if argument in allowed_datatypes:
                store_swpc_rt_latest([argument])
            else:
                store_swpc_rt_file(filename=argument)
        for key in db_ids:
            resample_rdata({'id': db_ids[key]})
