#!/usr/bin/env python3
import sys
import os
import requests
import logging
import datetime

# sys.path.append(os.environ['HOME'] + '/devel/space-weather-backoffice-data-store/')
from datastore.ingestors import ingest_tools

baseurl = 'http://sdo.oma.be/latest/images/'
# 2021/07/18/H0800/AIA.20210718_080300.0094.quicklook.png


def ingest_aia_image_0094_quicklook(t0=None, t1=None):

    if t0 is None:
        t0 = datetime.datetime.now()

    if t0 is not None:
        if t1 is None:
            t1 = t0

    t = t0
    while t <= t1:
        ingest_aia_image_0094_quicklook_day(t)
        t = t + datetime.timedelta(days=1)


def ingest_aia_image_0094_quicklook_day(t):

    data = []

    for h in range(24):
        url = baseurl + t.strftime('%Y/%m/%d/H') + str(h).zfill(2) + '00/'

        try:
            r = requests.get(url)
            lines = r.text.split('\n')
            lines = lines[13:-9]
            for line in lines:
                entry = []
                items = line.split('>')
                name = items[5].split('"')[1]
                type = name.split('.')[2]
                if type != '0094':
                    continue
                date = name.split('.')[1]
                entry = [datetime.datetime.strptime(date, '%Y%m%d_%H%M%S').
                         strftime('%Y-%m-%dT%H:%M:%SZ'), url, name, name]
                data.append(entry)

        except Exception as e:
            logging.error('failed to get ' + url + ': ' + str(e))

    dataset = {}
    dataset['id'] = 'aia_quicklook_image_0094'
    dataset['parameters'] = ['time', 'url_prefix',
                             'url_low_res', 'url_high_res']
    dataset['data'] = data

    ingest_tools.store(dataset['id'], dataset['parameters'], dataset['data'],
                       update=True, api='api_r_url')


if __name__ == "__main__":
    format = "%(asctime)s [%(levelname).1s] %(message)s"
    logging.basicConfig(format=format, level=logging.INFO,
                        datefmt="%Y-%m-%d %H:%M:%S")

    arguments = sys.argv[1:]
    times = ingest_tools.parse_date_args(arguments)
    if len(arguments) == 0:
        ingest_aia_image_0094_quicklook()
    elif times:
        ingest_aia_image_0094_quicklook(t0=times[0], t1=times[1])

# ------------------------------------------------------------------------------
