#!/usr/bin/env python3
import numpy as np
import pandas as pd
from datastore.accessors import goes_xrs, swpc_rt
from datastore.ingestors import ingest_tools
from datastore.ingestors.ingest_solar_wind_dscovr import store_dscovr_timespan
from datastore.ingestors.ingest_solar_wind_ace import store_ace_timespan
from datastore.ingestors.ingest_swpc_rt import store_swpc_rt_latest
from datastore.datastore import resample_rdata


def store_xray_flux_combined():
    id = 'xray_flux_combined'
    # Primary science / avg data
    df = goes_xrs.goes_xrs_primary_to_dataframe(only=[16])
    # Real-time data
    rt_filenames = swpc_rt.download_data(datatype='xrays')
    df2 = swpc_rt.json_to_dataframe(rt_filenames[0])
    df2.rename({'0.05-0.4nm': 'xray_flux_short',
                '0.1-0.8nm': 'xray_flux_long'}, axis=1, inplace=True)
    df2.where(df2['xray_flux_long'] > 1e-10, other=np.nan, inplace=True)
    # Join the science and real-time data
    fields = ['xray_flux_short', 'xray_flux_long']
    df_new = pd.concat(
        [df[fields], df2[fields].drop(df.index.intersection(df2.index))]
    )
    df_new['time'] = df_new.index.strftime("%Y-%m-%dT%H:%M:%SZ")
    parameters = ['time', 'xray_flux_short', 'xray_flux_long']
    data = df_new[parameters].values.tolist()
    ingest_tools.store(
        id,
        parameters,
        data,
        update=True
    )
    resample_rdata({'id': id})


def store_solar_wind_mag_combined(tstart=None):
    id = 'solar_wind_mag_combined'
    t1 = pd.Timestamp.utcnow()
    t0 = t1 - pd.to_timedelta(3, 'D') if tstart is None else tstart
    # ACE data
    store_ace_timespan(t0, t1, datatype='mag', table_id=id)
    # DSCOVR data
    store_dscovr_timespan(t0, t1, datatype='m1m', table_id=id)
    # Latest real-time data
    store_swpc_rt_latest(datatypes=['mag'])


def store_solar_wind_plasma_combined(tstart=None):
    id = 'solar_wind_mag_combined'
    t1 = pd.Timestamp.utcnow()
    t0 = t1 - pd.to_timedelta(3, 'D') if tstart is None else tstart
    # ACE data
    store_ace_timespan(t0, t1, datatype='mag', table_id=id)
    # DSCOVR data
    store_dscovr_timespan(t0, t1, datatype='m1m', table_id=id)
    # Latest real-time data
    store_swpc_rt_latest(datatypes=['plasma'])

def main():
    tstart = pd.to_datetime("1998-01-01")
    store_solar_wind_plasma_combined(tstart=tstart)
    store_solar_wind_mag_combined(tstart=tstart)
    store_xray_flux_combined()

if __name__ == "__main__":
    main()
