#!/usr/bin/env python3
import sys
import logging
import numpy as np
import pandas as pd
from datastore.accessors import swpc_rt
from datastore.accessors import goes_xrs
from datastore.ingestors import ingest_tools
from datastore.datastore import resample_rdata


def store_goes_xrs(goesnum):
    filenames = goes_xrs.download(goesnum)
    for filename in filenames:
        df = goes_xrs.nc_to_dataframe(
            filename,
            apply_flags=True,
            convert_column_names=True
        )
        df['time'] = df.index.strftime("%Y-%m-%dT%H:%M:%SZ")
        parameters = ['time', 'xray_flux_short', 'xray_flux_long']
        data = df[parameters].values.tolist()
        ingest_tools.store(
            f'xray_flux_goes{goesnum:02d}',
            parameters,
            data,
            update=True
        )


def store_xray_flux_combined():
    id = 'xray_flux_combined'
    # Primary science / avg data
    df = goes_xrs.goes_xrs_primary_to_dataframe()#only=[16])
    # Real-time data
    rt_filenames = swpc_rt.download_data(datatype='xrays')
    df2 = swpc_rt.json_to_dataframe(rt_filenames[0])
    df2.rename({'0.05-0.4nm': 'xray_flux_short',
                '0.1-0.8nm': 'xray_flux_long'}, axis=1, inplace=True)
    df2.where(df2['xray_flux_long'] > 1e-10, other=np.nan, inplace=True)
    # Join the science and real-time data
    fields = ['xray_flux_short', 'xray_flux_long']
    df_new = pd.concat(
        [df[fields], df2[fields].drop(df.index.intersection(df2.index))]
    )
    df_new['time'] = df_new.index.strftime("%Y-%m-%dT%H:%M:%SZ")
    parameters = ['time', 'xray_flux_short', 'xray_flux_long']
    data = df_new[parameters].values.tolist()
    ingest_tools.store(
        id,
        parameters,
        data,
        update=True
    )
    resample_rdata({'id': id})


def main():
    format = "%(asctime)s [%(levelname)s] %(message)s"
    logging.basicConfig(format=format,
                        level=logging.INFO,
                        datefmt="%Y-%m-%d %H:%M:%S")

    arguments = sys.argv[1:]
    if len(arguments) == 0:
        for goesnum in [13, 14, 15, 16, 17]:
            store_goes_xrs(goesnum)
            resample_rdata({'id': f'xray_flux_goes{goesnum:02d}'})
    elif len(arguments) == 1 and arguments[0] == 'download':
        for goesnum in range(3, 18):
            goes_xrs.download(goesnum)
    elif len(arguments) == 1 and arguments[0] == 'combined':
        store_xray_flux_combined()
    else:
        for argument in arguments:
            goesnum = int(argument)
            store_goes_xrs(goesnum=goesnum)
            resample_rdata({'id': f'xray_flux_goes{goesnum:02d}'})


if __name__ == "__main__":
    main()
