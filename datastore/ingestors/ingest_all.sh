#!/bin/bash

./ingest_kp_index.py t=1930,2021
 
./ingest_solar_wind_dscovr.py t=2015-01-01,2022-01-01

./ingest_solar_wind_ace.py t=1998,2020

./ingest_solar_wind_mag_rt.py
./ingest_solar_wind_plasma_rt.py
./ingest_xray_flux_rt.py

./ingest_aia_image_0193.py -t=2021-06-01,2021-07-01
