#!/usr/bin/env python3
import os
import glob
import json
import requests
import numpy as np
import pandas as pd
import re
import xarray as xr
import configparser
from datastore.ingestors import get_hapi_session_cookies
from datastore.config import config
from datastore.ingestors import ingest_tools


def download_enlil_file(username, password, filename, local_filename):
    session_established, auth_cookie = get_hapi_session_cookies.get_auth_cookie(username, password)
    if session_established:
        url = 'https://esa.spaceweather.api.metoffice.gov.uk/models/enlil/files/' + filename
        print(f"Session established, accessing {url}")
        response = requests.get(url, cookies = {'iPlanetDirectoryPro': auth_cookie})
        if response.ok:
            data = response.content
            with open(local_filename, 'wb') as fh:
                fh.write(data)
            print(f"{pd.Timestamp.utcnow()}: {filename} was written")
            return local_filename
        else:
            print("Request failed...")
            print(response.status_code, response.reason)
    else:
        print("Unable to establish session...")
    return None


def download_enlil_files_timerange(username, password, local_path, t0, t1):
    files_downloaded = []
    date_range = pd.date_range(t0, t1, freq='2H')

    for date in date_range:
        date_str = date.strftime("%Y-%m-%dT%HZ")
        filename = f"{date_str}.evo.Earth.nc"
        local_filename = f'{local_path}{filename}'
        if os.path.isfile(local_filename):
            print(f"file {filename} already exists locally")
        else:
            new_file = download_enlil_file(username, password, filename, local_filename)
            if new_file:
                files_downloaded.append(new_file)
    return files_downloaded


def parse_enlilfile(enlilfile, enlil_times):
    timefmt = "%Y-%m-%dT%H:%M:%SZ"
    reftime = enlil_times[enlilfile]
    ds = xr.open_dataset(enlilfile)
    df = ds.to_dataframe()
    df.index = reftime + pd.to_timedelta(df['TIME'], 's')
    df = df.resample('30min').mean()
    df.index = df.index + pd.to_timedelta('15min')
    df['density'] = 1e-6 * df['D'] / (1.67262e-27) # kg/m3 to protons/cm3
    df['speed'] = 1e-3 * np.sqrt(df['V1']**2 + df['V2']**2 + df['V3']**2)
    df['pressure'] = 1.6726e-6 * df['density'] * df['speed']**2
    df['bt'] = 1e9 * np.sqrt(df['B1']**2 + df['B2']**2 + df['B3']**2)
    df.rename({'T': 'temperature'}, axis=1, inplace=True)
    df['time'] = df.index.strftime(timefmt)
    df['timetag_issue'] = reftime.strftime(timefmt)
    return df

def get_esa_portal_credentials():
    # Username and password for ESA portal
    config = configparser.ConfigParser()
    configfile = f'{os.path.dirname(os.path.realpath(__file__))}/swe_auth.txt'
    print(configfile)
    config.read(configfile)
    username = config['DEFAULT']['username']
    password = config['DEFAULT']['password']
    return username, password

# Local path to store the NetCDF files
LOCAL_PATH = f"{config['local_source_data_path']}/metoffice/enlil/"

# ESA portal credentials
username, password = get_esa_portal_credentials()

def ingest_enlil_metoffice():
    # Start and end time for downloading new files
    all_enlil_files = glob.iglob(f"{LOCAL_PATH}/*")
    enlil_times = {file: pd.to_datetime(file.split('/')[-1].split('.')[0]) for file in all_enlil_files}
    if len(enlil_times.values()) > 0:
        timestamp_latest_file = max(enlil_times.values())
    else:
        timestamp_latest_file = pd.Timestamp.utcnow().floor('1D') - pd.to_timedelta('10D')
    t1 = pd.Timestamp.utcnow().ceil('2H')
    t0 = (timestamp_latest_file - pd.to_timedelta('6H')).floor('2H')
#    t0 = pd.to_datetime("2022-03-01T00:00:00", utc=True)

    # Download files
    new_files = download_enlil_files_timerange(username, password, LOCAL_PATH, t0, t1)
    files_to_ingest = list(filter(lambda file: "T00Z" in file or "T12Z" in file or "T06Z" in file or "T18Z" in file, new_files))
    files_to_ingest.sort()
    enlil_times = {file: pd.to_datetime(file.split('/')[-1].split('.')[0]) for file in files_to_ingest}

    # Ingest files
    for enlilfile in files_to_ingest:
        print(f"Ingesting {enlilfile} in HAPI database")
        df = parse_enlilfile(enlilfile, enlil_times)
        # Put in database
        parameters = ['time', 'timetag_issue', 'density', 'speed', 'temperature', 'pressure', 'bt']
        table_id = 'solar_wind_plasma_forecast_metoffice'
        df_cropped = df[enlil_times[enlilfile]-pd.to_timedelta(5, 'D'):]
        data = df_cropped[parameters].values.tolist()
        ingest_tools.store(table_id, parameters, data, update=False)

if __name__ == '__main__':
    ingest_enlil_metoffice()
