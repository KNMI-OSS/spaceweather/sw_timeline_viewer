#!/usr/bin/env python3
from datastore.accessors import swarm_diss
from datastore.ingestors import ingest_tools
import numpy as np
import pandas as pd

# Read the data
t0 = pd.to_datetime("2023-04-01T", utc=True)
t1 = pd.to_datetime("2023-07-01T", utc=True)
sat = "Swarm C"
data_type = 'DNSxPOD'
swarm_obj = swarm_diss.SwarmFiles(data_type=data_type, sat=sat)
swarm_obj.set_time_interval(t0, t1)
df = swarm_obj.to_dataframe()

# Select the required fields
if data_type.endswith("POD"):
    parameters = ['density', 'density_orbitmean']
else:
    parameters = ['density']

allparams = ['time'] + parameters

# Set invalid data to NaN
if data_type.endswith("POD"):
    for par in parameters:
        df[par] = df[par].where(df['validity_flag'] == 0, np.nan)

if True:
    # Set the time as a properly formatted string
    df['time'] = df.index.strftime("%Y-%m-%dT%H:%M:%SZ")

    # Convert to list and store in datastore
    data = df[allparams].values.tolist()
    if data_type.endswith("ACC"):
        id = f'thermosphere_{sat.replace(" ","").lower()}_acc_PT10S'
    else:
        id = f'thermosphere_{sat.replace(" ","").lower()}_PT30S'
    ingest_tools.store(
        id,
        allparams,
        data,
        update=True,
        api='api_url'
    )

    # Now process the lower cadences of high-rate (< 1 orbital period) data
    if data_type.endswith("ACC"):
        cadences = ['PT30S', 'PT3M', 'PT6M', 'PT15M']
    else:
        cadences = ['PT3M', 'PT6M', 'PT15M']
    for cadence in cadences:
        df_resampled = df[parameters].resample(pd.to_timedelta(cadence)).mean()
        df_resampled['time'] = df_resampled.index.strftime("%Y-%m-%dT%H:%M:%SZ")
        data = df_resampled[allparams].values.tolist()
        if data_type.endswith("ACC"):
            id = f'thermosphere_{sat.replace(" ","").lower()}_acc_' + cadence
        else:
            id = f'thermosphere_{sat.replace(" ","").lower()}_' + cadence
        ingest_tools.store(
            id,
            allparams,
            data,
            update=True,
            api='api_url'
        )

# Now process the lower cadences of aggregated (min, mean, max) data for lower cadences (> 1 orbital period)
cadences = ['PT1H40M', 'PT3H', 'P1D', 'P3D', 'P15D', 'P27D']
aggstats = ['min', 'max', 'mean', 'count']
for cadence in cadences:
    # Compute statistics
    df_agg = df[parameters].resample(pd.to_timedelta(cadence), label='left').agg(aggstats)

    # Adjust index to middle of cadence
    df_agg.index = df_agg.index + 0.5 * pd.to_timedelta(cadence)

    # Set data to NaN if statistics are based on limited observations in the interval, then get rid of the count
    for par in ['density']:
        maxcount = 0.9*df_agg[par]['count'].max()
        for aggstat in ['min', 'max', 'mean']:
            df_agg.loc[:,(par, aggstat)].where(df_agg.loc[:,(par, 'count')] > maxcount, inplace=True)
        df_agg.drop((par, "count"), axis=1, inplace=True)

    # Change the multi-index to single index ("density_min", "density_max", etc.)
    df_agg.columns = ["_".join(col_name) for col_name in df_agg.columns.to_flat_index()]
    agg_params = df_agg.columns
    df_agg['time'] = df_agg.index.strftime("%Y-%m-%dT%H:%M:%SZ")
    data = df_agg[['time', 'density_min', 'density_max', 'density_mean']].values.tolist()
    id = f'thermosphere_{sat.replace(" ","").lower()}_minmaxmean_' + cadence
    ingest_tools.store(
        id,
        ['time', 'density_min', 'density_max', 'density_mean'],
        data,
        update=True,
        api='api_url'
    )
