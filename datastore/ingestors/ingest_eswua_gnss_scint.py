#!/usr/bin/env python3
import os
import pandas as pd
import xarray as xr
from datastore.ingestors import ingest_tools
from datastore.datastore import resample_rdata

local_path = f'{os.environ["HOME"]}/SynologyDrive/datasets/eswua/'
cutoff = 30
stations = stations = ['lyb0p',
                       'dmc0p',
                       'san0p',
                       'nya0p',
                       'sao0p',
                       'lam0p',
                       'mzs0p',
                       'nya1p',
                       'nic0p',
                       'han0p',
                       'thu0p',
                       'ush0p',
                       'tuc0p',
                       'hel0p',
                       'klu0p']

t0 = '2021-09-01'
t1 = '2022-04-01'
parameters = ["time",
              "phi60l1slant_max",
              "totals4l1_max",
              "phi60l1slant_sats_mod",
              "phi60l1slant_sats_sev",
              "totals4l1_sats_mod",
              "totals4l1_sats_sev"]

for station in stations:
    full_path = f"{local_path}{station}/"
    for day in pd.date_range(t0, t1, freq='1d'):
        filename = f"{full_path}gnss_scint_{station}_{day.strftime('%Y%m%d')}.nc"
        if not os.path.isfile(filename):
            print(f"File not found {filename}")
            continue
        print(f"Ingesting {filename}")
        xrdata = xr.load_dataset(filename)
        #xrdata_filtered = xrdata.where( (xrdata['elevation'] > cutoff) & ((xrdata['svid']<=37) | ( (xrdata['svid']>=71) & (xrdata['svid'] <= 106) ) ))
        xrdata_filtered = xrdata.where( (xrdata['elevation'] > cutoff) & (xrdata['svid']<=106) )
        xrdata_filtered['time'] = (['dt'], pd.to_datetime(xrdata_filtered['dt'].data).strftime("%Y-%m-%dT%H:%M:%SZ"))
        xrdata_filtered['phi60l1slant_max'] = xrdata_filtered['phi60l1slant'].max(dim='svid')
        xrdata_filtered['totals4l1_max'] = xrdata_filtered['totals4l1'].max(dim='svid')
        xrdata_filtered['phi60l1slant_sats_mod'] = xrdata_filtered['phi60l1slant'].where(xrdata_filtered['phi60l1slant']>=0.4).count(dim='svid')
        xrdata_filtered['phi60l1slant_sats_sev'] = xrdata_filtered['phi60l1slant'].where(xrdata_filtered['phi60l1slant']>=0.7).count(dim='svid')
        xrdata_filtered['totals4l1_sats_mod'] = xrdata_filtered['totals4l1'].where(xrdata_filtered['totals4l1']>=0.5).count(dim='svid')
        xrdata_filtered['totals4l1_sats_sev'] = xrdata_filtered['totals4l1'].where(xrdata_filtered['totals4l1']>=0.8).count(dim='svid')

        data = xrdata_filtered[parameters].squeeze().to_dataframe().drop("station", axis=1).values.tolist()
        # print(parameters)
        # print(data[0])
        ingest_tools.store(f'gnss_scint_{station}', parameters, data, update=True)

    resample_rdata({'id': f'gnss_scint_{station}'})
