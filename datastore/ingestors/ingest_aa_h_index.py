#!/usr/bin/env python3
import pandas as pd
import requests
from datastore.ingestors import ingest_tools
from datastore.accessors import gfz_kp_hp_indices
from datastore.datastore import resample_rdata


url = 'https://www.swsc-journal.org/articles/swsc/olm/2018/01/swsc180022/swsc180022-2-olm.txt'
filename = 'swsc180022-2-olm.txt'

r = requests.get(url)
if not r.ok:
    print("Error accessing the data")
else:
    data_raw = r.text
    with open(filename, 'w') as fh:
        fh.write(r.text)

    columns = ["year", "month", "day", "hour", "fraction of year",
               "aa_h_index", "aa_HN", "aa_SN", "aa/s", "aa_N/s", "aa_S/s"]
    df = pd.read_table(filename, delim_whitespace=True, skiprows=35, names=columns)
    df.index = pd.to_datetime(df[['year', 'month', 'day', 'hour']], utc=True)
    df['time'] = df.index.strftime("%Y-%m-%dT%H:%M:%SZ")

    parameters = ['time', 'aa_h_index']
    
    table_id = 'aa_h_index'
    data = df[parameters].values.tolist()
    ingest_tools.store(table_id, parameters, data, update=True)
    resample_rdata({'id': table_id})
