#!/usr/bin/env python3

import os, sys, re

import pandas as pd
import requests
import logging
import datetime

# sys.path.append(os.environ['HOME'] + '/devel/space-weather-backoffice-data-store/')
from datastore.ingestors import ingest_tools

baseurl = 'https://sdo.gsfc.nasa.gov/assets/img/browse/'

# ------------------------------------------------------------------------------

def ingest_aia_image_1600(t0 = None, t1 = None):

    if t0 == None:
        t0 = datetime.datetime.now()

    if not t0 == None:
        if t1 == None:
            t1 = t0;

    t = t0
    while t <= t1:
        ingest_aia_image_1600_day(t)
        t = t + datetime.timedelta(days=1)

# ------------------------------------------------------------------------------

def ingest_aia_image_1600_day(t):

    entries = []

    url = baseurl + t.strftime('%Y/%m/%d')
    try:
        r = requests.get(url)
        lines = r.text.split('\n')
        lines = lines[8:-3]
        for line in lines:
            entry = {}
            items = line.split('>')
            entry['url_prefix'] = url
            entry['href'] = items[0].split('"')[1]
            entry['modification_date'] = items[2].strip()[0:16]
            entry['size'] = items[2].strip()[17:].strip()

            name = items[1].split('<')[0]
            basename = name.split('.')[0]
            basename_items = basename.split('_')
            if len(basename_items) == 4:
                date = basename_items[0]
                time = basename_items[1]
                res  = basename_items[2]
                type = basename_items[3]

                entry['name'] = name
                entry['res' ] = res
                entry['type'] = type

                entry['date'] = datetime.datetime.strptime(date + ' ' + time, '%Y%m%d %H%M%S').strftime('%Y-%m-%dT%H:%M:%SZ')

                entries.append(entry)

    except Exception as e:
        logging.error('failed to get ' + url + ': ' + str(e))

    data = []

    date_list = {}
    for entry in entries:
        if entry['date'] not in date_list:
            date_list[entry['date']] = []
        date_list[entry['date']].append(entry)

    for date in date_list:
        href_512 = ''
        href_1024 = ''
        for entry in date_list[date]:
            if entry['type'] == '1600' and  entry['res'] == '512':
                href_512 = entry['href']
            if entry['type'] == '1600' and  entry['res'] == '1024':
                href_1024 = entry['href']
        if href_512 != '' or href_1024 != '':
            data.append([date, entry['url_prefix'], href_512, href_1024])

    dataset = {}
    dataset['id'] = 'aia_image_1600'
    dataset['parameters'] = ['time', 'url_prefix', 'url_low_res', 'url_high_res']
    dataset['data'] = data

    ingest_tools.store(dataset['id'], dataset['parameters'], dataset['data'], update=True, api='api_r_url')

# ------------------------------------------------------------------------------

if __name__ == "__main__":

    format = "%(asctime)s [%(levelname).1s] %(message)s"
    logging.basicConfig(format=format, level=logging.INFO, datefmt="%Y-%m-%d %H:%M:%S")

    arguments = sys.argv[1:]
    times = ingest_tools.parse_date_args(arguments) # Returns tuple of times from t=2001,2003-like command line arg, or None for invalid input
    if len(arguments) == 0:
        ingest_aia_image_1600()
    elif times:
        ingest_aia_image_1600(t0=times[0], t1=times[1])

# ------------------------------------------------------------------------------
