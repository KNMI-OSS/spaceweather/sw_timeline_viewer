#!/usr/bin/env python3
import sys
import logging
from datastore.ingestors import ingest_tools
from datastore.accessors import ace
from datastore.datastore import resample_rdata


def store_ace_plasma_file(filename, table_id):
    df = ace.hdf_to_dataframe(filename)
    df['time'] = df.index.strftime("%Y-%m-%dT%H:%M:%SZ")
    df['pressure'] = 1.6726e-6 * df['proton_density'] * df['proton_speed']**2
    parameters = ['time', 'density', 'speed', 'temperature', 'pressure']
    data = df[['time', 'proton_density', 'proton_speed',
               'proton_temp', 'pressure']].values.tolist()
    ingest_tools.store(table_id, parameters, data, update=True)


def store_ace_mag_file(filename, table_id):
    logging.info(f"Storing {filename} in data store.")
    df = ace.hdf_to_dataframe(filename)
    df['time'] = df.index.strftime("%Y-%m-%dT%H:%M:%SZ")
    parameters = ['time', 'bt', 'bx_gsm', 'by_gsm', 'bz_gsm']
    data = df[['time', 'Bmag', 'Bgsm_x', 'Bgsm_y', 'Bgsm_z']].values.tolist()
    ingest_tools.store(table_id, parameters, data, update=True)


def store_ace_timespan(t0, t1, datatype, table_id=None):
    filenames = ace.download_data(t0, t1, datatype)
    for filename in filenames:
        if datatype == 'swepam':
            store_ace_plasma_file(filename, "solar_wind_plasma_ace"
                                            if table_id is None else table_id)
        elif datatype == 'mag':
            store_ace_mag_file(filename, "solar_wind_mag_ace"
                                         if table_id is None else table_id)


if __name__ == "__main__":

    format = "%(asctime)s [%(levelname)s] %(message)s"
    logging.basicConfig(format=format, level=logging.INFO,
                        datefmt="%Y-%m-%d %H:%M:%S")

    arguments = sys.argv[1:]
    times = ingest_tools.parse_date_args(arguments)
    if len(arguments) == 0:
        pass
    elif times:
        store_ace_timespan(t0=times[0], t1=times[1], datatype='swepam')
        store_ace_timespan(t0=times[0], t1=times[1], datatype='mag')
    else:
        for argument in arguments:
            if 'swepam_data' in argument:
                store_ace_plasma_file(filename=argument)
            elif 'mag_data' in argument:
                store_ace_mag_file(filename=argument)

    resample_rdata({'id': 'solar_wind_mag_ace'})
    resample_rdata({'id': 'solar_wind_plasma_ace'})
