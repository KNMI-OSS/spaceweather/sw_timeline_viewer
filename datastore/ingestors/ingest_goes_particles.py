#!/usr/bin/env python3
import sys
import logging
import numpy as np
import pandas as pd
from datastore.accessors import swpc_rt
from datastore.accessors import goes_particles
from datastore.ingestors import ingest_tools
from datastore.datastore import resample_rdata


def rt_proton_flux(filename=None):
    # Real-time data
    if filename is None:
        rt_filename = swpc_rt.download_data(datatype='integral-protons')[0]
    else:
        rt_filename = filename
        
    df2 = swpc_rt.json_to_dataframe(rt_filename)
    
    flux10 = df2.query("(energy == '>=10 MeV')")['flux'].values
    flux50 = df2.query("(energy == '>=50 MeV')")['flux'].values
    flux100 = df2.query("(energy == '>=100 MeV')")['flux'].values
    
    times10 = df2.query("(energy == '>=10 MeV')")['time_tag'].values
    times50 = df2.query("(energy == '>=50 MeV')")['time_tag'].values
    times100 = df2.query("(energy == '>=100 MeV')")['time_tag'].values
    if not (times10==times100).all():
        logging.error("Problem with SWPC realtime data file")
    rtdata = np.column_stack((times10, flux10, flux50, flux100))
    return rtdata

def science_proton_flux():
    # Primary avg data
    df_old_format, df_new_format = goes_particles.goes_eps_primary_to_dataframe()
    
    df_old_format['time'] = df_old_format.index.strftime("%Y-%m-%dT%H:%M:%SZ")
    df_old_format['proton_flux_10MeV'] = df_old_format['p3_flux_ic']
    df_old_format['proton_flux_50MeV'] = df_old_format['p5_flux_ic']
    df_old_format['proton_flux_100MeV'] = df_old_format['p7_flux_ic']
    
    df_new_format['time'] = df_new_format.index.strftime("%Y-%m-%dT%H:%M:%SZ")
    # ZPGT10E = Flux of >10 MeV protons from the B detector that faces
    # either East or West depending on the yaw flip of the satellite
    # with a correction applied to remove contaminating particles
    # ZPGT10W = Flux of >10 MeV protons from the A detector that faces
    # either East or West depending on the yaw flip of the satellite
    # with a correction applied to remove contaminating particles
    df_new_format['proton_flux_10MeV_A'] = df_new_format['ZPGT10W']
    df_new_format['proton_flux_10MeV'] = df_new_format['ZPGT10E']
    # ZPGT50E = Flux of >50 MeV protons from the B detector that faces
    # either East or West depending on the yaw flip of the satellite
    # with a correction applied to remove contaminating particles
    # etc. etc.
    df_new_format['proton_flux_50MeV_A'] = df_new_format['ZPGT50W']
    df_new_format['proton_flux_50MeV'] = df_new_format['ZPGT50E']
    df_new_format['proton_flux_100MeV_A'] = df_new_format['ZPGT100W']
    df_new_format['proton_flux_100MeV'] = df_new_format['ZPGT100E']

    return df_old_format, df_new_format

def store_proton_flux(rt_filename=None, sciencedata=True, resample=True):
    id = 'proton_flux'
    parameters = ['time', 'proton_flux_10MeV', 'proton_flux_50MeV', 'proton_flux_100MeV']

    # Real-time data
    rtdata = rt_proton_flux(filename=rt_filename)

    if sciencedata:
        # Science data
        df_old_format, df_new_format = science_proton_flux()
       
    
        # Combine EPS old_format, EPS new_format and real time data
        data_old = df_old_format[parameters].to_numpy()
        data_new = df_new_format[parameters].to_numpy()
        data = np.concatenate((data_old, data_new))
        data = np.concatenate((data, rtdata))
        data = data.tolist()
    else:
        data = rtdata.tolist()      
    
    # Ingest in sqlite databse
    ingest_tools.store(
        id,
        parameters,
        data,
        update=True
    )
    if resample:
        resample_rdata({'id': id})


def main():
    format = "%(asctime)s [%(levelname)s] %(message)s"
    logging.basicConfig(format=format,
                        level=logging.INFO,
                        datefmt="%Y-%m-%d %H:%M:%S")

    arguments = sys.argv[1:]
    if len(arguments) == 0:
        store_proton_flux()
    elif len(arguments) == 0: # still need to implement possibility to select time span
        start = arguments[0]
        end = arguments[1]
        # call function
    else:
        # Ingest rt json files one-by-one
        for filename in sys.argv[1:]:
            print(filename)
            store_proton_flux(rt_filename=filename, sciencedata=False, resample=False)
        resample_rdata({'id': 'proton_flux'})


if __name__ == "__main__":
    main()
