#!/usr/bin/env python3
import numpy as np
import pandas as pd
import os
import re
import json
import requests
import chaosmagpy
from swxtools.config import config
from swxtools.access import swarm_diss
from swxtools.orbit import transforms
from swxtools import dataframe_tools
from datastore.ingestors import ingest_tools
from datastore.datastore import add_rdataset, retrieve_dataframe

def ingest_swarm_magx_lr_fast():
    chaosmodel = chaosmagpy.load_CHAOS_matfile(config['local_source_data_path'] + '/chaosmodel/CHAOS-7.14.mat')
    for satletter in ['a', 'b', 'c']:
        for data_type in ['MODx_SC', 'MAGx_LR']:
            files = swarm_diss.download(sat=f'Swarm {satletter.upper()}', data_type=data_type, fast=True)

        # Define the dataset
        metadata_json = '''{
            "id": "swarma_mag_lr_fast",
            "description": "Magnetic field data from the Swarm A satellite",
            "timeStampLocation": "begin",
            "resourceURL": "https://swarm-diss.eo.esa.int/",
            "resourceID": "ESA",
            "contact": "ESA EO Help",
            "contactID": "ESA EO Help",
            "resampleMethod": "mean",
            "subsets": [
                {
                    "cadence": "PT1S",
                    "additionalDescription": ""
                },
                {
                    "cadence": "PT3S",
                    "additionalDescription": "downsampled to 2 seconds"
                },
                {
                    "cadence": "PT8S",
                    "additionalDescription": "downsampled to 8 seconds"
                },
                {
                    "cadence": "PT30S",
                    "additionalDescription": "downsampled to 30 seconds"
                },
                {
                    "cadence": "PT2M",
                    "additionalDescription": "downsampled to 2 minutes"
                },
                {
                    "cadence": "PT6M",
                    "additionalDescription": "downsampled to 6 minutes"
                },
                {
                    "cadence": "PT15M",
                    "additionalDescription": "downsampled to 15 minutes"
                }
            ],
            "parameters": [
                {
                    "name": "time",
                    "type": "isotime",
                    "units": "UTC",
                    "fill": "",
                    "description": "Timestamp of the LP measurement",
                    "label": "",
                    "key": true
                },
                {
                    "name": "Latitude",
                    "type": "double",
                    "units": "degrees",
                    "fill": "",
                    "description": "Geocentric latitude",
                    "label": "",
                    "key": false
                },
                {
                    "name": "Longitude",
                    "type": "double",
                    "units": "degrees",
                    "fill": "",
                    "description": "Geocentric longitude",
                    "label": "",
                    "key": false
                },
                {
                    "name": "Radius",
                    "type": "double",
                    "units": "m",
                    "fill": "",
                    "description": "Radius",
                    "label": "",
                    "key": false
                },
                {
                    "name": "B_NEC_1",
                    "type": "double",
                    "units": "nT",
                    "fill": null,
                    "description": "Northward component of the magnetic field",
                    "label": "",
                    "key": false
                },
                {
                    "name": "B_NEC_2",
                    "type": "double",
                    "units": "nT",
                    "fill": null,
                    "description": "Eastward component of the magnetic field",
                    "label": "",
                    "key": false
                },
                {
                    "name": "B_NEC_3",
                    "type": "double",
                    "units": "nT",
                    "fill": null,
                    "description": "Nadir component of the magnetic field",
                    "label": "",
                    "key": false
                },
                {
                    "name": "F",
                    "type": "double",
                    "units": "nT",
                    "fill": null,
                    "description": "Magnitude of the magnetic field",
                    "label": "",
                    "key": false
                },
                {
                    "name": "Delta_B_NEC_1",
                    "type": "double",
                    "units": "nT",
                    "fill": null,
                    "description": "Northward component of the magnetic field, with core and crustal fields removed using the CHAOS model",
                    "label": "",
                    "key": false
                },
                {
                    "name": "Delta_B_NEC_2",
                    "type": "double",
                    "units": "nT",
                    "fill": null,
                    "description": "Eastward component of the magnetic field, with core and crustal fields removed using the CHAOS model",
                    "label": "",
                    "key": false
                },
                {
                    "name": "Delta_B_NEC_3",
                    "type": "double",
                    "units": "nT",
                    "fill": null,
                    "description": "Nadir component of the magnetic field, with core and crustal fields removed using the CHAOS model",
                    "label": "",
                    "key": false
                },
                {
                    "name": "Delta_F",
                    "type": "double",
                    "units": "nT",
                    "fill": null,
                    "description": "Magnitude of the magnetic field, with core and crustal fields removed using the CHAOS model",
                    "label": "",
                    "key": false
                },
                {
                    "name": "F_error",
                    "type": "double",
                    "units": "nT",
                    "fill": null,
                    "description": "Error estimate of the magnetic field intensity",
                    "label": "",
                    "key": false
                },
                {
                    "name": "Flags_F",
                    "type": "int",
                    "units": "",
                    "fill": null,
                    "description": "Flags related to the magnetic field intensity measurements, see Table 6-2 of SW-RS-DSC_SY-0007",
                    "label": "",
                    "key": false
                },
                {
                    "name": "Flags_B",
                    "type": "int",
                    "units": "",
                    "fill": null,
                    "description": "Flags characterizing the magnetic field vector measurements, see Table 6-2 of SW-RS-DSC-SY-0007",
                    "label": "",
                    "key": false
                },
                {
                    "name": "lon",
                    "type": "double",
                    "units": "deg",
                    "fill": null,
                    "description": "Geographic longitude",
                    "label": "",
                    "key": false
                },
                {
                    "name": "lat",
                    "type": "double",
                    "units": "deg",
                    "fill": null,
                    "description": "Geodetic latitude",
                    "label": "",
                    "key": false
                },
                {
                    "name": "height",
                    "type": "double",
                    "units": "m",
                    "fill": null,
                    "description": "Geodetic altitude",
                    "label": "",
                    "key": false
                },
                {
                    "name": "lon_qd",
                    "type": "double",
                    "units": "deg",
                    "fill": null,
                    "description": "Quasi-dipole longitude",
                    "label": "",
                    "key": false
                },
                {
                    "name": "lat_qd",
                    "type": "double",
                    "units": "deg",
                    "fill": null,
                    "description": "Quasi-dipole latitude",
                    "label": "",
                    "key": false
                },
                {
                    "name": "mlt",
                    "type": "double",
                    "units": "hours",
                    "fill": null,
                    "description": "Quasi-dipole magnetic local time",
                    "label": "",
                    "key": false
                }
            ]
        }'''
        # Set the satellite
        metadata_json = metadata_json.replace('swarma', f'swarm{satletter.lower()}')
        metadata_json = metadata_json.replace('Swarm A', f'Swarm {satletter.upper()}')

        # Get the id, parameters and cadences from the definition
        metadata = json.loads(metadata_json)
        db_id = metadata['id']
        parameters = list(map(lambda x: x['name'], metadata['parameters']))
        cadences = list(map(lambda x: x['cadence'], metadata['subsets']))

        # Helper functions for parsing the HAPI catalog
        def is_iso_duration(duration_string):
            regex = '^[-+]?P(?!$)(([-+]?\d+Y)|([-+]?\d+\.\d+Y$))?(([-+]?\d+M)|([-+]?\d+\.\d+M$))?(([-+]?\d+W)|([-+]?\d+\.\d+W$))?(([-+]?\d+D)|([-+]?\d+\.\d+D$))?(T(?=[\d+-])(([-+]?\d+H)|([-+]?\d+\.\d+H$))?(([-+]?\d+M)|([-+]?\d+\.\d+M$))?([-+]?\d+(\.\d+)?S)?)??$'
            return re.fullmatch(regex, duration_string) is not None

        def dataset_split_root_and_cadence(dataset_id):
            parts = dataset_id.split('_')
            if is_iso_duration(parts[-1]):
                cadence_str = parts[-1]
                cadence_sec = pd.to_timedelta(cadence_str)/pd.to_timedelta(1, 'sec')
                return {'dataset': dataset_id, 'root': "_".join(parts[0:-1]), 'cadence_iso': parts[-1], 'cadence_sec': cadence_sec}
            elif parts[-1].endswith('Hz'):
                freq_str = parts[-1]
                cadence_sec = 1/float(freq_str[0:-2])
                return {'dataset': dataset_id, 'root': "_".join(parts[0:-1]), 'cadence_iso': freq_str, 'cadence_sec': cadence_sec}
            else:
                return {'dataset': dataset_id, 'root': dataset_id}

        # Info
        info_request_json = requests.get(f'http://localhost:9000/hapi/info?id={db_id}_{cadences[0]}')
        info_request = json.loads(info_request_json.text)
        print(info_request['status']['message'])

        # Create if the dataset does not exist
        if info_request['status']['code'] == 1406:
            print("Dataset does not exist, adding")
            add_rdataset(metadata)

        # Read the list of already processed data files
        filelist_file = f'swarm{satletter.lower()}_MAGx_LR_FAST_processed_files.txt'
        if os.path.isfile(filelist_file):
            with open(filelist_file, 'r') as fh:
                processed_filenames = fh.read().splitlines()
        else:
            processed_filenames = []

        # Get the magnetic field data
        swarm_obj_mag = swarm_diss.SwarmFiles(data_type='MAGx_LR', sat=f'Swarm {satletter.upper()}', fast=True)

        # Initial set up of the boundaries for processing lower cadence data
        t_first_data = pd.to_datetime("2100-01-01T00:00:00", utc=True)
        t_last_data = pd.to_datetime("1900-01-01T00:00:00", utc=True)

        for i_file, file_info in enumerate(swarm_obj_mag.filelist.to_dict(orient='records')):
            # Skip if file has already been processed
            if file_info['filename'] in processed_filenames:
                continue

            # Load the LP data
            print("Processing: ", i_file, file_info['filename'])
            swarm_data = swarm_obj_mag.to_dataframe_for_file_index(i_file).replace(0.000, np.nan)

            # Compute the model values
            modeltime = (pd.to_datetime(swarm_data.index.values) - pd.to_datetime("2000-01-01T"))/pd.to_timedelta(1, 'D')
            B_radius, B_theta, B_phi = chaosmodel(modeltime,
                                                  swarm_data['Radius'].values / 1000,
                                                  90.0 - swarm_data['Latitude'].values,
                                                  swarm_data['Longitude'].values,
                                                  source_list='internal')

            # print(b_chaos_1[0:10], b_chaos_2[0:10], b_chaos_3[0:10])

            swarm_data['Delta_B_NEC_1'] = swarm_data['B_NEC_1'].values + B_theta
            swarm_data['Delta_B_NEC_2'] = swarm_data['B_NEC_2'].values - B_phi
            swarm_data['Delta_B_NEC_3'] = swarm_data['B_NEC_3'].values + B_radius
            swarm_data['Delta_F'] = swarm_data['F'].values - np.sqrt(B_radius**2 + B_theta**2 + B_phi**2)

            # print(swarm_data[['B_NEC_1', 'Delta_B_NEC_1']].head())

            # Update the boundaries for processing lower cadence data
            if swarm_data.index[0] < t_first_data:
                t_first_data = swarm_data.index[0]
            if swarm_data.index[-1] > t_last_data:
                t_last_data = swarm_data.index[-1]

            # Get and clean up the orbit data
            swarm_obj_orbit = swarm_diss.SwarmFiles(data_type='MODx_SC', sat=f'Swarm {satletter.upper()}', fast=True)
            swarm_obj_orbit.set_time_interval(swarm_data.iloc[0].name - pd.to_timedelta(2, 'min'), swarm_data.iloc[-1].name + pd.to_timedelta(2, 'min'))
            swarm_orbit = swarm_obj_orbit.to_dataframe().drop('time_gps', axis=1)
            swarm_orbit = swarm_orbit[~swarm_orbit.index.duplicated()]

            # Interpolate the orbit
            swarm_orbit_interpolated = transforms.interpolate_orbit_to_datetimeindex(swarm_orbit, swarm_data.index)

            # Convert to geodetic and quasi-dipole coordinates
            swarm_orbit_geo = transforms.itrf_to_geodetic(swarm_orbit_interpolated)
            swarm_orbit_qd = transforms.geodetic_to_qd(swarm_orbit_geo)

            # Append the orbit data to the Magnetic field data
            swarm_data_complete = pd.concat([swarm_data,
                                             swarm_orbit_qd[['lon', 'lat', 'height', 'lon_qd', 'lat_qd', 'mlt']]],
                                             axis=1)

            # Append the filename to the list of processed files
            processed_filenames.append(file_info['filename'])
            with open(filelist_file, 'a+') as fh:
                fh.write(file_info['filename'] + '\n')

            # Upload to the data store
            swarm_data_complete['time'] = swarm_data_complete.index.strftime("%Y-%m-%dT%H:%M:%S.%fZ")
            data = swarm_data_complete[parameters].values.tolist()
            ingest_tools.store(
                f'{db_id}_{cadences[0]}',
                parameters,
                data,
                update=True,
                api='api_url'
            )

        # Time interval for processing lower cadences
        highest_cadence = pd.to_timedelta(cadences[-1])
        t_start = (t_first_data - 2 * highest_cadence).floor(freq='1D')
        t_stop = (t_last_data + 2 * highest_cadence).ceil(freq='1D')

        print("Resampling for the time interval: ", t_start, t_stop)

        for t in pd.date_range(t_start, t_stop, freq='1D'):
            t0 = t - 2 * highest_cadence
            t1 = t + pd.to_timedelta(1, 'D') + 2 * highest_cadence
            url = f'http://localhost:9000/hapi/data?id={db_id}_{cadences[0]}&time.min={t0.strftime("%Y-%m-%dT%H:%M:%SZ")}&time.max={t1.strftime("%Y-%m-%dT%H:%M:%SZ")}'
            print("Reading data from HAPI for: ", t0, t1)
            df = pd.read_csv(url, header=None, names=parameters)
            if len(df) == 0:
                continue
            df.index = pd.to_datetime(df['time'])
            parameters_no_time = parameters[1:]
            for cadence in cadences[1:]:
                df_resampled = df[parameters_no_time].resample(pd.to_timedelta(cadence), origin='epoch').mean()
                df_resampled = dataframe_tools.mark_gaps_in_dataframe(df_resampled, nominal_timedelta=pd.to_timedelta(cadence), nominal_start_time=t0, nominal_end_time=t1)
                df_resampled['time'] = df_resampled.index.strftime("%Y-%m-%dT%H:%M:%SZ")
                data = df_resampled[parameters].values.tolist()
                id = f"{db_id}_{cadence}"
                print("--Writing data to HAPI at cadence: ", cadence)
                ingest_tools.store(
                    id,
                    parameters,
                    data,
                    update=True,
                    api='api_url'
                )

if __name__ == "__main__":
    ingest_swarm_magx_lr_fast()
