import json
import pandas
import logging
import requests
from datastore.config import config

def store(dataset, parameters, data, update=False, api='api_r_url'):

    post = {}
    post['id'] = dataset
    post['parameters'] = parameters
    post['data'] = data

    headers = {"Content-Type": "application/json"}

    endpoint = config[api] + '/dataset'
    if update:
        response = requests.put(endpoint, data=json.dumps(post),
                                headers=headers)
    else:
        response = requests.post(endpoint, data=json.dumps(post),
                                 headers=headers)
    json_response = response.json()

    if response.status_code == 200:
        logging.info('success: {}'.format(json_response['status']))
    else:
        logging.error('failed: {}'.format(json_response['status']))

# def resample(data, parameters, rule):
#     df = pd.DataFrame.from_records(data=data, columns=parameters)
#     df['time'] = pd.to_datetime(df['time'], utc=True)
#     df.index = df['time']
#     df = df.drop(['time'], axis=1)
#
#     if 'kp_index' in parameters:
#         df_resampled = df.resample(rule).max()
#     else:
#         df_resampled = df.resample(rule).mean()
#
#     time = df_resampled.index.strftime('%Y-%m-%dT%H:%M:%SZ')
#     df_resampled.insert(0, 'time', time)
#
#     tuples = df_resampled.to_records(index=False)
#     return [list(tuple) for tuple in tuples]


def parse_date_args(arguments):
    import re
    import pandas as pd
    p = re.compile("t=(?P<t0>.*),(?P<t1>.*)")
    for arg in arguments:
        r = p.search(arg)
        if r:
            try:
                t0 = pd.to_datetime(r.groups()[0])
                t1 = pd.to_datetime(r.groups()[1])
                return(t0, t1)
            except (TypeError, ValueError):
                return None
