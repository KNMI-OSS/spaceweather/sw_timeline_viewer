#!/usr/bin/env python3

import os
import sys
import re

import json
import logging
import requests
import datetime

# import ingest_kp_index_forecast_metoffice
# sys.path.append(os.environ['HOME'] + '/devel/space-weather-backoffice-data-store/')
import datastore
from datastore.config import config

from datastore.ingestors import ingest_kp_index_forecast_noaa
from datastore.ingestors import ingest_swpc_geomag_forecast
from datastore.ingestors import ingest_kp_index
from datastore.ingestors import ingest_hp30
from datastore.ingestors import ingest_swpc_rt
from datastore.ingestors import ingest_solar_wind_dscovr
from datastore.ingestors import ingest_goes_xrs
from datastore.ingestors import ingest_aia_image_0094
from datastore.ingestors import ingest_aia_image_0193
from datastore.ingestors import ingest_aia_image_0304
from datastore.ingestors import ingest_aia_image_1600
from datastore.ingestors import ingest_aia_image_0094_quicklook
from datastore.ingestors import ingest_enlil_metoffice
from datastore.ingestors import ingest_f10_7
from datastore.ingestors import ingest_silso_sunspots
from datastore.ingestors import ingest_swarm_efix_lp_fast
from datastore.ingestors import ingest_swarm_magx_lr_fast

# ------------------------------------------------------------------------------

def ingest_update():

    now = datetime.datetime.now()

    url = config['api_r_url'] + '/dataset?id=solar_wind_plasma_rt'
    reply = requests.get(url)
    message = json.loads(reply.content)
    if message['status']['code'] != 1200:
        logging.warning('unable to update database: ' + message['status']['message'])
        return

    diff = 1000000
    if message['stopDate'] != '':
        stopDate = datetime.datetime.strptime(message['stopDate'], '%Y-%m-%dT%H:%M:%SZ')
        diff = (now - stopDate).total_seconds()

    if diff < 2*3600:
        ingest_hp30.main()
        ingest_swpc_rt.store_swpc_rt_latest()
        #ingest_aia_image_0094.ingest_aia_image_0094()
        #ingest_aia_image_0193.ingest_aia_image_0193()
        #ingest_aia_image_0304.ingest_aia_image_0304()
        #ingest_aia_image_1600.ingest_aia_image_1600()
        #ingest_aia_image_0094_quicklook.ingest_aia_image_0094_quicklook()
        #ingest_goes_xrs.store_xray_flux_combined()

    else:
        ingest_hp30.main()
        ingest_swpc_rt.store_swpc_rt_latest()
        #ingest_aia_image_0094.ingest_aia_image_0094()
        #ingest_aia_image_0193.ingest_aia_image_0193()
        #ingest_aia_image_0304.ingest_aia_image_0304()
        #ingest_aia_image_1600.ingest_aia_image_1600()
        #ingest_aia_image_0094_quicklook.ingest_aia_image_0094_quicklook()
        #ingest_goes_xrs.store_xray_flux_combined()

    url = config['api_r_url'] + '/dataset?id=kp_index'
    reply = requests.get(url)
    message = json.loads(reply.content)
    if message['status']['code'] != 1200:
        logging.warning('unable to update database: ' + message['status']['message'])
        return

    diff = 1000000
    if message['stopDate'] != '':
        stopDate = datetime.datetime.strptime(message['stopDate'], '%Y-%m-%dT%H:%M:%SZ')
        diff = (now - stopDate).total_seconds()

    if diff > 3*3600:
        ingest_swpc_geomag_forecast.store_swpc_geomagnetic_forecast_current()
        ingest_kp_index.store_kp()
        # ingest_kp_index_forecast_metoffice.ingest_kp_index_forecast_metoffice()
        ingest_kp_index_forecast_noaa.ingest_kp_index_forecast_noaa()
        ingest_enlil_metoffice.ingest_enlil_metoffice() 
        ingest_silso_sunspots.ingest_silso_sunspots()
        ingest_f10_7.ingest_f10_7()
        ingest_swarm_efix_lp_fast.ingest_swarm_efix_lp_fast()
        ingest_swarm_magx_lr_fast.ingest_swarm_efix_lp_fast()


    url = config['api_r_url'] + '/dataset?id=solar_wind_plasma_dscovr'
    reply = requests.get(url)
    message = json.loads(reply.content)
    if message['status']['code'] != 1200:
        logging.warning('unable to update database: ' + message['status']['message'])
        return

    if message['stopDate'] != '':
        stopDate = datetime.datetime.strptime(message['stopDate'], '%Y-%m-%dT%H:%M:%SZ')
        diff = (now - stopDate).total_seconds()
        if diff > 24*3600:
            ingest_solar_wind_dscovr.store_dscovr_timespan(t0=stopDate, t1=now, datatype='f1m')
            ingest_solar_wind_dscovr.store_dscovr_timespan(t0=stopDate, t1=now, datatype='m1m')

# ------------------------------------------------------------------------------

if __name__ == "__main__":

    format = "%(asctime)s [%(levelname).1s] %(message)s"
    logging.basicConfig(format=format, level=logging.INFO, datefmt="%Y-%m-%d %H:%M:%S")

    ingest_update()

# ------------------------------------------------------------------------------
