#!/usr/bin/env python3

import os
import sys
import re

import pandas as pd
import datetime
import requests
import logging

# sys.path.append(os.environ['HOME'] + '/devel/space-weather-backoffice-data-store/')
from datastore.ingestors import ingest_tools

url = 'https://services.swpc.noaa.gov/products/noaa-planetary-k-index-forecast.json'

# ------------------------------------------------------------------------------

def ingest_kp_index_forecast_noaa(file = None):

    if file == None:
        logging.info('getting {}'.format(url))
        r = requests.get(url)
        if r.status_code != 200:
            logging.error('failed to get {}'.format(url))
            return
        df = pd.read_json(r.text)
    else:
        if not os.path.isfile(file):
            logging.error('failed to read {}'.format(file))
            return
        df = pd.read_json(file)

    header = df.iloc[0]
    df = df[1:]
    df.columns = header

    observed = ['observed', 'estimated', 'predicted']

    data = [(
        datetime.datetime.strptime(df['time_tag'][i+1], '%Y-%m-%d %H:%M:%S').strftime('%Y-%m-%dT%H:%M:%SZ'),
        float(df['kp'][i+1]),
        float(observed.index(df['observed'][i+1])),
        ) for i in range(df['time_tag'].size)]

    parameters = [ "time", "kp_index", "observed" ]

    ingest_tools.store('kp_index_forecast_noaa', parameters, data, update=True)

    base = df['time_tag'][1]

    for i in range(df['time_tag'].size):
        if df['observed'][i+1] == 'observed':
            base = df['time_tag'][i+1]

    base = datetime.datetime.strptime(base, '%Y-%m-%d %H:%M:%S')

    observed = ['observed', 'estimated', 'predicted']

    data = []
    for i in range(df['time_tag'].size):
        dtime = (datetime.datetime.strptime(df['time_tag'][i+1], '%Y-%m-%d %H:%M:%S') - base).total_seconds() / 3600
        if dtime >= 0:
            data.append((
                datetime.datetime.strptime(df['time_tag'][i+1], '%Y-%m-%d %H:%M:%S').strftime('%Y-%m-%dT%H:%M:%SZ'),
                dtime,
                float(df['kp'][i+1]),
                float(observed.index(df['observed'][i+1]))))

    parameters = [ "time", "delta_time", "kp_index", "observed" ]

    ingest_tools.store('kp_index_forecast_all_noaa', parameters, data, update=True)

# ------------------------------------------------------------------------------

if __name__ == "__main__":

    format = "%(asctime)s [%(levelname)s] %(message)s"
    logging.basicConfig(format=format, level=logging.INFO, datefmt="%Y-%m-%d %H:%M:%S")

    arguments = sys.argv[1:]
    if len(arguments) == 0:
        ingest_kp_index_forecast_noaa()
    else:
        for argument in arguments:
            print(argument)
            ingest_kp_index_forecast_noaa(file = argument)

# ------------------------------------------------------------------------------
