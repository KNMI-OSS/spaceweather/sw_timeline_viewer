#!/usr/bin/env python3
import logging
from datastore.ingestors import ingest_tools
from datastore.accessors import gfz_kp_hp_indices
from datastore.datastore import resample_rdata


def store_kp_file(filename):
    logging.info(f"Storing {filename} in data store.")
    df = gfz_kp_hp_indices.txt_to_dataframe(filename)
    df['time'] = df.index.strftime("%Y-%m-%dT%H:%M:%SZ")
    df.rename({'Kp': 'kp_index'}, axis=1, inplace=True)
    parameters = ['time', 'kp_index']
    data = df[parameters].dropna().values.tolist()
    ingest_tools.store('kp_index', parameters, data, update=True)


def store_kp():
    filenames = gfz_kp_hp_indices.download_data(index_type='Kp')
    for filename in filenames:
        store_kp_file(filename)


def main():
    format = "%(asctime)s [%(levelname)s] %(message)s"
    logging.basicConfig(format=format, level=logging.INFO,
                        datefmt="%Y-%m-%d %H:%M:%S")

    store_kp()
    resample_rdata({'id': 'kp_index'})


if __name__ == "__main__":
    main()