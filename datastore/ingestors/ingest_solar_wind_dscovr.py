#!/usr/bin/env python3
import sys
import logging
import pandas as pd
from datastore.ingestors import ingest_tools
from datastore.accessors import dscovr
from datastore import datastore


def store_dscovr_plasma_file(filename, table_id='solar_wind_plasma_dscovr'):
    logging.info(f"Storing {filename} in data store.")
    df = dscovr.nc_to_dataframe(filename)
    df['time'] = df.index.strftime("%Y-%m-%dT%H:%M:%SZ")
    df['pressure'] = 1.6726e-6 * df['proton_density'] * df['proton_speed']**2
    data = df[['time', 'proton_density', 'proton_speed',
               'proton_temperature', 'pressure']].values.tolist()
    parameters = ['time', 'density', 'speed', 'temperature', 'pressure']
    ingest_tools.store(table_id, parameters, data,
                       update=True)


def store_dscovr_mag_file(filename, table_id='solar_wind_mag_dscovr'):
    logging.info(f"Storing {filename} in data store.")
    df = dscovr.nc_to_dataframe(filename)
    df['time'] = df.index.strftime("%Y-%m-%dT%H:%M:%SZ")
    data = df[['time', 'bt', 'bx_gsm', 'by_gsm', 'bz_gsm']].values.tolist()
    parameters = ['time', 'bt', 'bx_gsm', 'by_gsm', 'bz_gsm']
    ingest_tools.store(table_id, parameters, data,
                       update=True)


def store_dscovr_timespan(t0, t1, datatype='f1m', table_id=None):
    filenames = dscovr.download_data(t0, t1, datatype)
    for filename in filenames:
        if datatype == 'f1m':
            store_dscovr_plasma_file(filename,
                                     'solar_wind_plasma_dscovr'
                                     if table_id is None else table_id)
        elif datatype == 'm1m':
            store_dscovr_mag_file(filename,
                                  'solar_wind_mag_dscovr'
                                  if table_id is None else table_id)


if __name__ == "__main__":
    format = "%(asctime)s [%(levelname)s] %(message)s"
    logging.basicConfig(format=format, level=logging.INFO,
                        datefmt="%Y-%m-%d %H:%M:%S")

    arguments = sys.argv[1:]
    times = ingest_tools.parse_date_args(arguments)
    # Returns tuple of times from t=2001,2003-like command line arg,
    #  or None for invalid input
    if len(arguments) == 0:
        times = [pd.to_datetime("19960101"), pd.Timestamp.utcnow()]
        store_dscovr_timespan(t0=times[0], t1=times[1], datatype='f1m')
        store_dscovr_timespan(t0=times[0], t1=times[1], datatype='m1m')
    elif times:
        store_dscovr_timespan(t0=times[0], t1=times[1], datatype='f1m')
        store_dscovr_timespan(t0=times[0], t1=times[1], datatype='m1m')
    else:
        for argument in arguments:
            if 'f1m' in argument:
                store_dscovr_plasma_file(filename=argument)
            elif 'm1m' in argument:
                store_dscovr_mag_file(filename=argument)
    datastore.resample_rdata({'id': 'solar_wind_mag_dscovr'})
    datastore.resample_rdata({'id': 'solar_wind_plasma_dscovr'})
