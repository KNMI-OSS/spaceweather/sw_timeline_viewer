#!/usr/bin/env python3
import sys
import logging
import os
# sys.path.append(os.environ['HOME'] + '/devel/space-weather-backoffice-data-store/')
from datastore.ingestors import ingest_tools
from datastore.accessors import swpc_geomagnetic_forecast

def store_swpc_geomagnetic_forecast_file(filename):
    timefmt = "%Y-%m-%dT%H:%M:%SZ"
    df = swpc_geomagnetic_forecast.to_dataframe(filename)
    df['timestr_issue'] = df['timetag_issue'].dt.strftime(timefmt)
    df['timestr_forecast'] = df['timetag_forecast'].dt.strftime(timefmt)
    data = df[['timestr_forecast', 'timestr_issue',
               'kp_index']].values.tolist()
    parameters = ['time', 'timetag_issue', 'kp_index']
    ingest_tools.store('kp_index_forecast_noaa_new', parameters, data,
                       update=True)


def store_swpc_geomagnetic_forecast_current():
    filenames = swpc_geomagnetic_forecast.download_current()
    for filename in filenames:
        store_swpc_geomagnetic_forecast_file(filename)


if __name__ == "__main__":
    format = "%(asctime)s [%(levelname)s] %(message)s"
    logging.basicConfig(format=format, level=logging.INFO,
                        datefmt="%Y-%m-%d %H:%M:%S")

    arguments = sys.argv[1:]
    if len(arguments) == 0:
        store_swpc_geomagnetic_forecast_current()
    else:
        for argument in arguments:
            print(argument)
            store_swpc_geomagnetic_forecast_file(filename=argument)

# ------------------------------------------------------------------------------
