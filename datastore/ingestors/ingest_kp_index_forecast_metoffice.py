#!/usr/bin/env python3

import os
import sys
import re

import logging
import datetime
import requests
import json

# sys.path.append(os.environ['HOME'] + '/devel/space-weather-backoffice-data-store/')
from datastore.ingestors import ingest_tools

base_url = 'https://fk9io4mwb9.execute-api.eu-west-1.amazonaws.com/spaceweather-api/timeseries/data'

# ------------------------------------------------------------------------------

def ingest_kp_index_forecast_metoffice(t0 = None, t1 = None):

    if t0 == None:
        t0 = datetime.datetime.now() - datetime.timedelta(days=14)
    if t1 == None:
        t1 = datetime.datetime.now()

    if isinstance(t0, str):
        t0 = datetime.datetime.strptime(t0, '%Y-%m-%d')
    if isinstance(t1, str):
        t1 = datetime.datetime.strptime(t1, '%Y-%m-%d')

    stream = 'kpforecast'
    datatype = 'kpforecast'

    t0str = t0.strftime('%Y-%m-%dT%H:%M:%SZ')
    t1str = t1.strftime('%Y-%m-%dT%H:%M:%SZ')

    url = base_url + '?stream={}&parameter={}&time_start={}&time_stop={}'.format(stream, datatype, t0str, t1str)
    logging.info('getting {}'.format(url))
    r = requests.get(url)
    if r.status_code != 200:
        logging.error('failed to get {}'.format(url))
        return

    values = json.loads(r.content)["data"]

    data = [(
        v['timestamp'],
        v['value'] * 3
        ) for v in values]

    parameters = [ "time", "kp_index" ]

    ingest_tools.store('kp_index_forecast_metoffice', parameters, data, update=True)

    base = datetime.datetime.strptime(values[0]['timestamp'], '%Y-%m-%dT%H:%M:%SZ')

    data = [(
        v['timestamp'],
        (datetime.datetime.strptime(v['timestamp'], '%Y-%m-%dT%H:%M:%SZ') - base).total_seconds() / 3600,
        v['value'] * 3
        ) for v in values]

    parameters = [ "time", "delta_time", "kp_index" ]

    ingest_tools.store('kp_index_forecast_all_metoffice', parameters, data, update=True)

# ------------------------------------------------------------------------------

if __name__ == "__main__":

    format = "%(asctime)s [%(levelname)s] %(message)s"
    logging.basicConfig(format=format, level=logging.INFO, datefmt="%Y-%m-%d %H:%M:%S")

    ingest_kp_index_forecast_metoffice()

# ------------------------------------------------------------------------------

