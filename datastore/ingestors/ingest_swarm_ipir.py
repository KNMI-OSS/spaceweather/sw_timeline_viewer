#!/usr/bin/env python3
import sys
import logging
import pandas as pd
from datastore.accessors import swarm_diss
from datastore.ingestors import ingest_tools

def ingest_swarm_ipir(satletter, t0, t1):
    swarm_obj = swarm_diss.SwarmFiles('IPDxIRR', f'Swarm {satletter.upper()}')

    parameters = ['Latitude', 'Ne', 'Background_Ne', 'Foreground_Ne', 'Te', 'Grad_Ne_at_100km', 'Grad_Ne_at_50km',
                  'Grad_Ne_at_20km', 'ROD', 'RODI10s', 'RODI20s',
                  'delta_Ne10s', 'delta_Ne20s', 'delta_Ne40s', 'mVTEC', 'mROT',
                  'mROTI10s', 'mROTI20s', 'TEC_STD']
    allparameters = ['time', *parameters]

    for date in pd.date_range(t0, t1, freq='1MS'):
        logging.info(f"{date}, {date + pd.tseries.offsets.MonthBegin(1)}")
        swarm_obj.set_time_interval(date, date + pd.tseries.offsets.MonthBegin(1))
        df_swarm = swarm_obj.to_dataframe()
        if len(df_swarm) == 0:
            print("No Swarm data found")
            continue
        df_swarm['time'] = df_swarm.index.strftime("%Y-%m-%dT%H:%M:%SZ")
        data = df_swarm[allparameters].values.tolist()
        id = f'ipir_swarm{satletter.lower()}_PT1S'
        logging.info("PT1S")
        ingest_tools.store(
            id,
            allparameters,
            data,
            update=True,
            api='api_url'
        )
        # Now process the lower cadences of high-rate (< 1 orbital period) data
        cadences = ['PT5S', 'PT30S', 'PT3M', 'PT6M', 'PT15M']
        for cadence in cadences:
            logging.info(cadence)
            df_resampled = df_swarm[parameters].resample(pd.to_timedelta(cadence)).mean()
            df_resampled['time'] = df_resampled.index.strftime("%Y-%m-%dT%H:%M:%SZ")
            data = df_resampled[allparameters].values.tolist()
            id = f'ipir_swarm{satletter.lower()}_' + cadence
            ingest_tools.store(
                id,
                allparameters,
                data,
                update=True,
                api='api_url'
            )

        # Now process the lower cadences of aggregated (min, mean, max) data for lower cadences (> 1 orbital period)
        cadences = ['PT1H40M', 'PT3H', 'P1D', 'P3D', 'P15D', 'P27D']
        aggstats = ['min', 'max', 'mean', 'count']
        for cadence in cadences:
            logging.info(cadence)
            # Compute statistics
            df_agg = df_swarm[['Ne']].resample(pd.to_timedelta(cadence), label='left').agg(aggstats)

            # Adjust index to middle of cadence
            df_agg.index = df_agg.index + 0.5 * pd.to_timedelta(cadence)

            # Set data to NaN if statistics are based on limited observations in the interval, then get rid of the count
            for par in ['Ne']:
                maxcount = 0.9*df_agg[par]['count'].max()
                for aggstat in ['min', 'max', 'mean']:
                    df_agg.loc[:,(par, aggstat)].where(df_agg.loc[:,(par, 'count')] > maxcount, inplace=True)
                df_agg.drop((par, "count"), axis=1, inplace=True)

            # Change the multi-index to single index ("density_min", "density_max", etc.)
            df_agg.columns = ["_".join(col_name) for col_name in df_agg.columns.to_flat_index()]
            agg_params = df_agg.columns
            df_agg['time'] = df_agg.index.strftime("%Y-%m-%dT%H:%M:%SZ")
            data = df_agg[['time', 'Ne_min', 'Ne_max', 'Ne_mean']].values.tolist()
            id = f'ipir_ne_swarm{satletter.lower()}_minmaxmean_' + cadence
            ingest_tools.store(
                id,
                ['time', 'Ne_min', 'Ne_max', 'Ne_mean'],
                data,
                update=True,
                api='api_url'
            )

if __name__ == "__main__":
    format = "%(asctime)s [%(levelname)s] %(message)s"
    logging.basicConfig(format=format, level=logging.INFO,
                        datefmt="%Y-%m-%d %H:%M:%S")

    arguments = sys.argv[1:]
    times = ingest_tools.parse_date_args(arguments)
    if len(arguments) == 0:
        logging.info("Ingesting IPIR data for the past weeks")
        t1 = pd.Timestamp.utcnow()
        t0 = t1 - pd.to_timedelta(50, 'D')
        for sat in ['a', 'b', 'c']:
            logging.info(f"Swarm {sat.upper()}")
            ingest_swarm_ipir(sat, t0, t1)

    elif times:
        for sat in ['a', 'b', 'c']:
            logging.info(f"Swarm {satletter.upper()}")
            ingest_swarm_ipir(sat, times[0], times[1])
    else:
        logging.info("unknown arguments")
