#!/usr/bin/env python3

# ------------------------------------------------------------------------------
#
# KNMI Space Weather Back-Office
#
# Copyright 2021 KNMI
# This software is the proprietary information of KNMI
# All rights Reserved
#
# ------------------------------------------------------------------------------

import os
import sys
import json
import logging
import requests
import ingest_kp_index_forecast_metoffice_geoweb
import ingest_kp_index_forecast_noaa
import ingest_kp_index
import ingest_swpc_rt
# import ingest_solar_wind_mag_rt
# import ingest_solar_wind_plasma_rt
# import ingest_xray_flux_rt

sys.path.append('../datastore')
import datastore

# ------------------------------------------------------------------------------

def ingest_update():

    pass

"""
    config, config_file = datastore.loadConfig()

    now = datetime.datetime.now()

    url = config['api_url'] + '/rdataset?id=solar_wind_plasma_rt'
    r = requests.get(url)

    d = json.loads(r.content)
    stopDate = datetime.datetime.strptime(d['stopDate'], '%Y-%m-%dT%H:%M:%SZ')
    diff = (now - stopDate).total_seconds()

    if diff < 2*3600:
        ingest_solar_wind_mag_rt.ingest_solar_wind_mag_rt(remote_file = 'mag-2-hour.json')
        ingest_solar_wind_plasma_rt.ingest_solar_wind_plasma_rt(remote_file = 'plasma-2-hour.json')
        ingest_xray_flux_rt.ingest_xray_flux_rt(remote_file = 'xrays-6-hour.json')
    else:
        ingest_solar_wind_mag_rt.ingest_solar_wind_mag_rt(remote_file = 'mag-7-day.json')
        ingest_solar_wind_plasma_rt.ingest_solar_wind_plasma_rt(remote_file = 'plasma-7-day.json')
        ingest_xray_flux_rt.ingest_xray_flux_rt(remote_file = 'xrays-7-day.json')

    url = 'http://pc190612.knmi.nl:8888/hapi/info?id=timeseries_kp_index_3H'
    r = requests.get(url)

    d = json.loads(r.content)
    stopDate = datetime.datetime.strptime(d['stopDate'], '%Y-%m-%dT%H:%M:%SZ')
    diff = (now - stopDate).total_seconds()

    if diff > 3*3600:
        ingest_kp_index.ingest_kp_index()
        ingest_kp_index_forecast_metoffice_geoweb.ingest_kp_index_forecast_metoffice_geoweb()
        ingest_kp_index_forecast_noaa.ingest_kp_index_forecast_noaa()
"""

# ------------------------------------------------------------------------------

if __name__ == "__main__":

    #format = "%(asctime)s [%(levelname).1s] %(message)s"
    format = "[%(levelname).1s] %(message)s"
    logging.basicConfig(format=format, level=logging.INFO, datefmt="%Y-%m-%d %H:%M:%S")

    logging.info('KNMI Space Weather Back-Office')
    logging.info('Datastore Initialisation')
    logging.info('')

    try:
        config, config_file = datastore.loadConfig()
        logging.info("Loading configuration from " + config_file);
    except Exception as error:
        logging.error(error)
        sys.exit()

    logging.info('')
    if 'database_path' not in config:
        logging.error('Missing \'database_path\' entry in configuration file')
        sys.exit()

    logging.info('Database: ' + config['database_path'])
    if os.path.isfile(config['database_path']):
        logging.warning('Database already exists and will be deleted')

    logging.info('')
    if 'api_url' not in config:
        logging.error('Missing \'api_url\' entry in configuration file')
        sys.exit()

    logging.info('API url: ' + config['api_url'])
    url = config['api_url'] + '/rdatasets'
    try:
        reply = requests.get(url)
        message = json.loads(reply.content)
        if message['status']['code'] == 1200:
            logging.info('API server up and running')
        else:
            logging.error('Something wrong with API server, reply status: ' + str(message['status']))
            sys.exit()
    except Exception as error:
        logging.error('Something wrong with API server, error: ' + str(error))
        sys.exit()

    logging.info('')
    reply = input('Continue [y/N]: ')
    if reply.lower() != 'y':
        logging.info('User abort')
        sys.exit()

    logging.info('TBD')

# ------------------------------------------------------------------------------
