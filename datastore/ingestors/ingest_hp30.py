#!/usr/bin/env python3
import logging
from datastore.ingestors import ingest_tools
from datastore.accessors import gfz_kp_hp_indices
from datastore.datastore import resample_rdata


def store_hp30_file(filename):
    logging.info(f"Storing {filename} in data store.")
    df = gfz_kp_hp_indices.txt_to_dataframe(filename)
    df['time'] = df.index.strftime("%Y-%m-%dT%H:%M:%SZ")
    parameters = ['time', 'Hp30']
    data = df[parameters].dropna().values.tolist()
    ingest_tools.store('hp30_index', parameters, data, update=True)


def store_hp30():
    filenames = gfz_kp_hp_indices.download_data(index_type='Hp30')
    for filename in filenames:
        store_hp30_file(filename)


def main():
    format = "%(asctime)s [%(levelname)s] %(message)s"
    logging.basicConfig(format=format, level=logging.INFO,
                        datefmt="%Y-%m-%d %H:%M:%S")

    store_hp30()
    resample_rdata({'id': 'hp30_index'})


if __name__ == "__main__":
    main()
