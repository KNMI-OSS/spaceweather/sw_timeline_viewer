#!/usr/bin/env python3
from timeit import default_timer as timer
from datetime import datetime, timedelta
import pandas as pd
import logging
import sqlite3
from datastore.config import config

md_dataset = [
    {'name': 'id', 'type': 'string', 'required': True, 'key': True},
    {'name': 'startDate', 'type': 'string', 'required': False, 'key': False},
    {'name': 'stopDate', 'type': 'string', 'required': False, 'key': False},
    {'name': 'timeStampLocation', 'type': 'string', 'required': False,
     'key': False},
    {'name': 'cadence', 'type': 'string', 'required': False, 'key': False},
    {'name': 'sampleStartDate', 'type': 'string', 'required': False,
     'key': False},
    {'name': 'sampleStopDate', 'type': 'string', 'required': False,
     'key': False},
    {'name': 'description', 'type': 'string', 'required': False, 'key': False},
    {'name': 'resourceURL', 'type': 'string', 'required': False, 'key': False},
    {'name': 'resourceID', 'type': 'string', 'required': False, 'key': False},
    {'name': 'creationDate', 'type': 'string', 'required': False,
     'key': False},
    {'name': 'modificationDate', 'type': 'string', 'required': False,
     'key': False},
    {'name': 'contact', 'type': 'string', 'required': False, 'key': False},
    {'name': 'contactID', 'type': 'string', 'required': False, 'key': False},
]

md_parameter = [
    {'name': 'name', 'type': 'string', 'required': True, 'key': True},
    {'name': 'type', 'type': 'string', 'required': True, 'key': False},
    {'name': 'length', 'type': 'string', 'required': False, 'key': False},
    {'name': 'units', 'type': 'string', 'required': True, 'key': False},
    {'name': 'fill', 'type': 'string', 'required': True, 'key': False},
    {'name': 'description', 'type': 'string', 'required': False, 'key': False},
    {'name': 'label', 'type': 'string', 'required': False, 'key': False},
]

md_rdataset = [
    {'name': 'id', 'type': 'string', 'required': True, 'key': True},
    {'name': 'startDate', 'type': 'string', 'required': False,
     'key': False},
    {'name': 'stopDate', 'type': 'string', 'required': False,
     'key': False},
    {'name': 'timeStampLocation', 'type': 'string', 'required': False,
     'key': False},
    {'name': 'sampleStartDate', 'type': 'string', 'required': False,
     'key': False},
    {'name': 'sampleStopDate', 'type': 'string', 'required': False,
     'key': False},
    {'name': 'description', 'type': 'string', 'required': False, 'key': False},
    {'name': 'resourceURL', 'type': 'string', 'required': False, 'key': False},
    {'name': 'resourceID', 'type': 'string', 'required': False, 'key': False},
    {'name': 'creationDate', 'type': 'string', 'required': False,
     'key': False},
    {'name': 'modificationDate', 'type': 'string', 'required': False,
     'key': False},
    {'name': 'contact', 'type': 'string', 'required': False, 'key': False},
    {'name': 'contactID', 'type': 'string', 'required': False, 'key': False},
    {'name': 'resampleMethod', 'type': 'string', 'required': True,
     'key': False},
]

md_rsubset = [
    {'name': 'cadence', 'type': 'string', 'required': True, 'key': True},
    {'name': 'additionalDescription', 'type': 'string', 'required': True,
     'key': False},
]

md_rparameter = [
    {'name': 'name', 'type': 'string', 'required': True, 'key': True},
    {'name': 'type', 'type': 'string', 'required': True, 'key': False},
    {'name': 'length', 'type': 'string', 'required': False, 'key': False},
    {'name': 'units', 'type': 'string', 'required': True, 'key': False},
    {'name': 'fill', 'type': 'string', 'required': True, 'key': False},
    {'name': 'description', 'type': 'string', 'required': False, 'key': False},
    {'name': 'label', 'type': 'string', 'required': False, 'key': False},
]


def parse_isoduration(s):

    def get_isosplit(s, split):
        if split in s:
            n, s = s.split(split)
        else:
            n = 0
        return n, s

    # Remove prefix
    s = s.split('P')[-1]

    # Step through letter dividers
    days, s = get_isosplit(s, 'D')
    _, s = get_isosplit(s, 'T')
    hours, s = get_isosplit(s, 'H')
    minutes, s = get_isosplit(s, 'M')
    seconds, s = get_isosplit(s, 'S')

    # Convert all to seconds
    dt = timedelta(days=int(days), hours=int(hours), minutes=int(minutes),
                   seconds=float(seconds))
    return int(dt.total_seconds())


def rows_to_dataframe(rows, parameter_names, keep_time_col=False):
    df = pd.DataFrame.from_records(data=rows, columns=parameter_names)
    df.index = pd.to_datetime(df['time'], utc=True)
    if not keep_time_col:
        df = df.drop(['time'], axis=1)
    return df


def type_db(type):
    if type == 'string':
        return 'TEXT'
    if type == 'isotime':
        return 'TEXT'
    if type == 'double':
        return 'REAL'
    if type == 'integer':
        return 'INTEGER'
    return ''


def init_db():
    md_parameter_db = md_parameter.copy()
    md_parameter_db.append(
        {'name': 'datasetID', 'type': 'string', 'required': False,
         'key': True})

    md_rsubset_db = md_rsubset.copy()
    md_rsubset_db.append(
        {'name': 'rdatasetID', 'type': 'string', 'required': False,
         'key': True})

    md_rparameter_db = md_rparameter.copy()
    md_rparameter_db.append(
        {'name': 'rdatasetID', 'type': 'string', 'required': False,
         'key': True})

    con = sqlite3.connect(config['database_path'])
    cur = con.cursor()
    statement = create_create_statement('md_dataset', md_dataset)
    cur.execute(statement)
    statement = create_create_statement('md_parameter', md_parameter_db)
    cur.execute(statement)
    statement = create_create_statement('md_rdataset', md_rdataset)
    cur.execute(statement)
    statement = create_create_statement('md_rsubset', md_rsubset_db)
    cur.execute(statement)
    statement = create_create_statement('md_rparameter', md_rparameter_db)
    cur.execute(statement)
    con.commit()
    con.close()


def create_create_statement(dataset, columns):

    keys = []
    for column in columns:
        if 'key' in column and column['key']:
            keys.append(column['name'])
    statement = 'CREATE TABLE IF NOT EXISTS ' + dataset + '('
    statement += ', '.join([(column['name'] + ' ' + type_db(column['type']))
                            for column in columns])
    if keys != []:
        statement += ', PRIMARY KEY (' + ', '.join(keys) + ')'
    statement += ');'
    return statement


def create_insert_statement(dataset, columns, replace=False):
    statement = 'REPLACE' if replace else 'INSERT OR IGNORE'
    statement += ' INTO ' + dataset + ' ( '
    statement += ' , '.join(columns)
    statement += ' ) VALUES ( '
    statement += ' , '.join(['?' for column in columns])
    statement += ' );'
    return statement


def get_capabilities(args):
    allowed_args = []
    for arg in args:
        if arg not in allowed_args:
            logging.error('invalid parameter: ' + arg)
            return '1401', {}

    reply = {}
    reply['outputFormats'] = ['csv', 'json']

    return '1200', reply


def get_datasets(args, target):
    allowed_args = []
    for arg in args:
        if arg not in allowed_args:
            logging.error('invalid parameter: ' + arg)
            return '1401', {}

    init_db()
    con = sqlite3.connect(config['database_path'])
    cur = con.cursor()
    statement = 'SELECT id, description FROM md_dataset ORDER BY id ASC'
    cur.execute(statement)
    rows = cur.fetchall()
    con.commit()
    con.close()

    reply = []
    for row in rows:
        reply.append({'id': row[0], 'title': row[1]})

    return '1200', {target: reply}


def get_dataset(args):
    allowed_args = ['id', 'parameters']
    for arg in args:
        if arg not in allowed_args:
            logging.error('invalid parameter: ' + arg)
            return '1401', {}

    if 'id' in args:
        id = args['id']
    else:
        logging.error('required parameter missing: id')
        return '1400', {}

    parameters_request = []
    if 'parameters' in args:
        parameters_request = args['parameters'].split(',')

    if parameters_request != []:
        parameters_request.append('time')

    names = []
    for item in md_dataset:
        names.append(item['name'])

    statement = 'SELECT '
    statement += ' , '.join(names)
    statement += ' FROM md_dataset WHERE id = ?'

    init_db()

    con = sqlite3.connect(config['database_path'])
    cur = con.cursor()
    cur.execute(statement, (id,))
    rows = cur.fetchall()
    con.commit()
    con.close()

    if rows == []:
        return '1406', {}

    reply = {}

    for i in range(len(names)):
        reply[names[i]] = list(rows[0])[i]

    names = []
    for item in md_parameter:
        names.append(item['name'])

    statement = 'SELECT '
    statement += ' , '.join(names)
    statement += ' FROM md_parameter WHERE datasetID = ?'

    con = sqlite3.connect(config['database_path'])
    cur = con.cursor()
    cur.execute(statement, (id,))
    rows = cur.fetchall()
    con.commit()
    con.close()

    parameters = []
    for row in rows:
        parameter = {}
        for i in range(len(names)):
            parameter[names[i]] = list(row)[i]
        if parameters_request == [] or parameter['name'] in parameters_request:
            parameters.append(parameter)

    reply['parameters'] = parameters

    return '1200', reply


def remove_dataset(args):
    allowed_args = ['id']
    for arg in args:
        if arg not in allowed_args:
            logging.error('invalid parameter: ' + arg)
            return '1401', {}

    if 'id' in args:
        id = args['id']
    else:
        logging.error('required parameter missing: id')
        return '1400', {}

    init_db()
    con = sqlite3.connect(config['database_path'])
    cur = con.cursor()
    statement = 'DROP TABLE IF EXISTS ' + id
    cur.execute(statement)
    statement = 'DELETE FROM md_dataset WHERE id = ?'
    cur.execute(statement, (id,))
    statement = 'DELETE FROM md_parameter WHERE datasetID = ?'
    cur.execute(statement, (id,))
    con.commit()
    con.close()

    return '1200', {}


def add_dataset(dataset):
    dataset_names = []
    dataset_values = []

    names = []
    values = []
    for item in md_dataset:
        name = item["name"]
        value = ""
        if name in dataset:
            value = dataset[name]
        elif item['required']:
            logging.error('required parameter missing: {}'.format(name))
            return '1400', {}
        if name == 'creationDate':
            value = datetime.today().strftime('%Y-%m-%dT%H:%M:%SZ')
        names.append(name)
        values.append(value)

    dataset_names = names
    dataset_values.append(tuple(values))

    parameters = dataset["parameters"]

    parameter_names = []
    parameter_values = []

    for parameter in parameters:
        names = []
        values = []
        for item in md_parameter:
            name = item["name"]
            value = ""
            if name in parameter:
                value = parameter[name]
            elif item['required']:
                logging.error('required parameter missing: {}'.format(name))
                return '1400', {}
            names.append(name)
            values.append(value)
        names.append('datasetID')
        values.append(dataset['id'])

        parameter_names = names
        parameter_values.append(tuple(values))

    init_db()

    con = sqlite3.connect(config['database_path'])
    cur = con.cursor()

    statement = create_insert_statement('md_dataset', dataset_names)
    cur.executemany(statement, dataset_values)

    statement = create_insert_statement('md_parameter', parameter_names)
    cur.executemany(statement, parameter_values)

    statement = create_create_statement(dataset['id'], parameters)
    cur.execute(statement)

    con.commit()
    con.close()

    # if table already has data, check time format

    con = sqlite3.connect(config['database_path'])
    cur = con.cursor()

    statement = 'SELECT time FROM ' + dataset['id'] + ' LIMIT 1'
    cur.execute(statement)
    rows = cur.fetchall()

    con.commit()
    con.close()

    separator = 'T'
    if len(rows) > 0 and rows[0][0] is not None:
        separator = rows[0][0][10:11]

    if separator == ' ':

        logging.info('converting time format')

        time_index = []
        for i in range(len(parameters)):
            if parameters[i]['type'] == 'isotime':
                time_index.append(i)

        con = sqlite3.connect(config['database_path'])
        cur = con.cursor()

        tmp_dataset = 'tmp_dataset'
        statement = create_create_statement(tmp_dataset, parameters)
        cur.execute(statement)

        statement = 'SELECT * FROM ' + dataset['id']
        cur.execute(statement)
        rows = cur.fetchall()
        converted_rows = []
        for row in rows:
            row_list = list(row)
            for i in time_index:
                time = datetime.strptime(row_list[i], '%Y-%m-%d %H:%M:%S')
                row_list[i] = time.strftime('%Y-%m-%dT%H:%M:%SZ')
            converted_rows.append(tuple(row_list))

        statement = create_insert_statement(tmp_dataset,
                                            [parameter['name'] for parameter
                                             in parameters])
        cur.executemany(statement, converted_rows)

        statement = 'DROP TABLE ' + dataset['id']
        cur.execute(statement)
        statement = ('ALTER TABLE ' + tmp_dataset + ' RENAME TO ' +
                     dataset['id'])
        cur.execute(statement)

        con.commit()
        con.close()

    return '1200', {}


def add_data(data, replace=False):

    if 'id' in data:
        dataset = data['id']
    else:
        logging.error('required parameter missing: {}'.format('dataset'))
        return '1400', {}

    if 'parameters' in data:
        parameters = data['parameters']
    else:
        logging.error('required parameter missing: {}'.format('parameters'))
        return '1400', {}

    if 'data' in data:
        data = data['data']
    else:
        logging.error('required parameter missing: {}'.format('data'))
        return '1400', {}

    tuples = []
    for item in data:
        tuples.append(tuple(item))

    modificationDate = datetime.today().strftime('%Y-%m-%dT%H:%M:%SZ')

    init_db()

    con = sqlite3.connect(config['database_path'])
    cur = con.cursor()

    statement = 'SELECT id FROM md_dataset WHERE id = ?'
    cur.execute(statement, (dataset,))
    rows = cur.fetchall()
    if rows == []:
        return '1406', {}

    statement = create_insert_statement(dataset, parameters, replace=replace)
    cur.executemany(statement, tuples)

    statement = 'SELECT MIN(time), MAX(time) FROM ' + dataset
    cur.execute(statement)
    rows = cur.fetchall()

    startDate = rows[0][0] if rows[0][0] is not None else ''
    stopDate = rows[0][1] if rows[0][1] is not None else ''
    statement = 'UPDATE md_dataset SET '
    statement += 'modificationDate = ?, startDate = ?, stopDate = ? '
    statement += 'WHERE id = ?'
    cur.execute(statement, (modificationDate, startDate, stopDate, dataset))

    con.commit()
    con.close()

    return '1200', {}


def get_data(args):
    allowed_args = ['id', 'time.min', 'time.max', 'parameters', 'include',
                    'format', 'resample']
    for arg in args:
        if arg not in allowed_args:
            logging.error('invalid parameter: ' + arg)
            return '1401', 'json', {}, {}

    if 'id' in args:
        id = args['id']
    else:
        logging.error('required parameter missing: id')
        return '1400', 'json', {}, {}

    if 'time.min' in args:
        time_min = args['time.min']
    else:
        logging.error('required parameter missing: time.min')
        return '1400', 'json', {}, {}

    if 'time.max' in args:
        time_max = args['time.max']
    else:
        logging.error('required parameter missing: time.max')
        return '1400', 'json', {}, {}

    parameters_request = []
    if 'parameters' in args:
        parameters_request = args['parameters'].split(',')

    if parameters_request != []:
        if 'time' not in parameters_request:
            parameters_request.append('time')

    include = ''
    if 'include' in args:
        include = args['include']

    format = 'csv'
    if 'format' in args:
        format = args['format']
    if format not in ['json', 'csv']:
        logging.error('invalid parameter: format: ' + format)
        return '1400', 'json', {}, {}

    resample = ''
    if 'resample' in args:
        resample = args['resample']

    try:
        time_min = datetime.strptime(time_min, '%Y-%m-%dT%H:%M:%SZ')
    except:
        logging.error('invalid parameter: time.min: ' + time_min)
        return '1402', 'json', {}, {}

    try:
        time_max = datetime.strptime(time_max, '%Y-%m-%dT%H:%M:%SZ')
    except:
        logging.error('invalid parameter: time.max: ' + time_max)
        return '1403', 'json', {}, {}

    time_min = time_min.strftime('%Y-%m-%dT%H:%M:%SZ')
    time_max = time_max.strftime('%Y-%m-%dT%H:%M:%SZ')

    names = []
    for item in md_parameter:
        names.append(item['name'])

    statement = 'SELECT '
    statement += ', '.join(names)
    statement += ' FROM md_parameter WHERE datasetID = ?'

    init_db()
    con = sqlite3.connect(config['database_path'])
    cur = con.cursor()
    cur.execute(statement, (id,))
    rows = cur.fetchall()
    con.commit()
    con.close()

    if len(rows) == 0:
        return '1406', 'json', {}, {}

    parameters = []
    parameter_names = []
    for row in rows:
        parameter = {}
        for i in range(len(names)):
            parameter[names[i]] = list(row)[i]
        if parameters_request == [] or parameter['name'] in parameters_request:
            parameter_names.append(parameter['name'])
            parameters.append(parameter)

    statement = 'SELECT '
    statement += ', '.join(parameter_names)
    statement += ' FROM ' + id + ' WHERE time >= ? AND time < ? ORDER BY time'

    con = sqlite3.connect(config['database_path'])
    cur = con.cursor()
    cur.execute(statement, (time_min, time_max))
    rows = cur.fetchall()
    con.commit()
    con.close()

    df = rows_to_dataframe(rows, parameter_names)

    data = []
    for row in rows:
        data.append(list(row))

    if format == 'json':
        reply_header = {}
        reply = {}
        reply['parameters'] = parameters
        reply['data'] = data
    else:
        reply_header = {}
        if include == 'header':
            reply_header['parameters'] = parameters
        elif include == 'brief_header':
            reply_header = ','.join(parameter_names)
        reply = df.to_csv(index=True, date_format='%Y-%m-%dT%H:%M:%S.%fZ',
                          float_format='%g', header=False)

    return '1200' if len(rows) > 0 else '1201', format, reply, reply_header


def remove_data(args):
    allowed_args = ['id', 'time.min', 'time.max']
    for arg in args:
        if arg not in allowed_args:
            logging.error('invalid parameter: ' + arg)
            return '1401', {}

    if 'id' in args:
        dataset = args['id']
    else:
        logging.error('required parameter missing: id')
        return '1400', {}

    if 'time.min' in args:
        time_min = args['time.min']
    else:
        logging.error('required parameter missing: time.min')
        return '1400', {}

    if 'time.max' in args:
        time_max = args['time.max']
    else:
        logging.error('required parameter missing: time.max')
        return '1400', {}

    try:
        time_min = datetime.strptime(time_min, '%Y-%m-%dT%H:%M:%SZ')
    except:
        logging.error('invalid parameter: time.min: ' + time_min)
        return '1402', 'json', {}, {}

    try:
        time_max = datetime.strptime(time_max, '%Y-%m-%dT%H:%M:%SZ')
    except:
        logging.error('invalid parameter: time.max: ' + time_max)
        return '1403', 'json', {}, {}

    time_min = time_min.strftime('%Y-%m-%dT%H:%M:%SZ')
    time_max = time_max.strftime('%Y-%m-%dT%H:%M:%SZ')

    statement = 'SELECT id FROM md_dataset WHERE id = ?'

    init_db()
    con = sqlite3.connect(config['database_path'])
    cur = con.cursor()
    cur.execute(statement, (id,))
    rows = cur.fetchall()
    con.commit()
    con.close()

    if len(rows) == 0:
        return '1406', 'json', {}, {}

    modificationDate = datetime.today().strftime('%Y-%m-%dT%H:%M:%S')

    con = sqlite3.connect(config['database_path'])
    cur = con.cursor()
    statement = 'DELETE FROM ' + dataset + ' WHERE time >= ? AND time < ?'
    cur.execute(statement, (time_min, time_max))
    statement = 'SELECT MIN(time), MAX(time) FROM ' + dataset
    cur.execute(statement)
    rows = cur.fetchall()
    startDate = rows[0][0] if rows[0][0] is not None else ''
    stopDate = rows[0][1] if rows[0][1] is not None else ''
    statement = 'UPDATE md_dataset SET '
    statement += 'modificationDate = ?, startDate = ?, stopDate = ? '
    statement += 'WHERE id = ?'
    cur.execute(statement, (modificationDate, startDate, stopDate, dataset))
    con.commit()
    con.close()

    return '1200', {}


def get_rdatasets(args, target):
    allowed_args = []
    for arg in args:
        if arg not in allowed_args:
            logging.error('invalid parameter: ' + arg)
            return '1401', {}

    reply = []
    init_db()
    with sqlite3.connect(config['database_path']) as con:
        cur = con.cursor()
        statement = 'SELECT id, description FROM md_rdataset'
        cur.execute(statement)
        rows = cur.fetchall()
        for row in rows:
            reply.append({'id': row[0], 'title': row[1]})
    return '1200', {target: reply}


def add_rdataset(rdataset):
    rdataset_names = []
    rdataset_values = []

    rdataset['creationDate'] = datetime.today().strftime('%Y-%m-%dT%H:%M:%SZ')

    names = []
    values = []
    for item in md_rdataset:
        name = item["name"]
        value = ""
        if name in rdataset:
            value = rdataset[name]
        elif item['required']:
            logging.error('required parameter missing: {}'.format(name))
            return '1400', {}
        names.append(name)
        values.append(value)

    rdataset_names = names
    rdataset_values.append(tuple(values))

    subsets = rdataset["subsets"]

    subset_names = []
    subset_values = []

    for subset in subsets:
        names = []
        values = []
        for item in md_rsubset:
            name = item["name"]
            value = ""
            if name in subset:
                value = subset[name]
            elif item['required']:
                logging.error('required parameter missing: {}'.format(name))
                return '1400', {}
            names.append(name)
            values.append(value)
        names.append('rdatasetID')
        values.append(rdataset['id'])

        subset_names = names
        subset_values.append(tuple(values))

    parameters = rdataset["parameters"]

    parameter_names = []
    parameter_values = []

    for parameter in parameters:
        names = []
        values = []
        for item in md_rparameter:
            name = item["name"]
            value = ""
            if name in parameter:
                value = parameter[name]
            elif item['required']:
                logging.error('required parameter missing: {}'.format(name))
                return '1400', {}
            names.append(name)
            values.append(value)
        names.append('rdatasetID')
        values.append(rdataset['id'])

        parameter_names = names
        parameter_values.append(tuple(values))

    init_db()

    with sqlite3.connect(config['database_path']) as con:
        statement = create_insert_statement('md_rdataset', rdataset_names)
        con.executemany(statement, rdataset_values)

        statement = create_insert_statement('md_rsubset', subset_names)
        con.executemany(statement, subset_values)

        statement = create_insert_statement('md_rparameter', parameter_names)
        con.executemany(statement, parameter_values)

    subsets = rdataset["subsets"]
    for subset in subsets:
        dataset = rdataset.copy()
        del dataset['resampleMethod']
        del dataset['subsets']
        dataset['id'] = dataset['id'] + '_' + subset['cadence']
        if subset['cadence'].startswith('P'):
            dataset['cadence'] = subset['cadence']
        elif subset['cadence'].endswith('Hz'):
            dataset['cadence'] = f"PT{1/float(subset['cadence'][0:-2])}S"
        dataset['description'] += ' ' + subset['additionalDescription']
        status, reply = add_dataset(dataset)

    return '1200', {}


def remove_rdataset(args):
    allowed_args = ['id']
    for arg in args:
        if arg not in allowed_args:
            logging.error('invalid parameter: ' + arg)
            return '1401', {}

    if 'id' in args:
        id = args['id']
    else:
        logging.error('required parameter missing: id')
        return '1400', {}

    init_db()

    with sqlite3.connect(config['database_path']) as con:

        statement = 'SELECT cadence FROM md_rsubset WHERE rdatasetID = ?'

        cur = con.cursor()
        cur.execute(statement, (id,))
        rows = cur.fetchall()

        if len(rows) == 0:
            return '1406', 'json', {}, {}

        for row in rows:
            statement = 'DROP TABLE IF EXISTS ' + id + '_' + row[0]
            cur.execute(statement)

        statement = 'DELETE FROM md_rdataset WHERE id = ?'
        cur.execute(statement, (id,))
        statement = 'DELETE FROM md_rparameter WHERE rdatasetID = ?'
        cur.execute(statement, (id,))
        statement = 'DELETE FROM md_rsubset WHERE rdatasetID = ?'
        cur.execute(statement, (id,))
        con.commit()

    return '1200', {}


def get_rdataset(args):
    allowed_args = ['id', 'parameters']
    for arg in args:
        if arg not in allowed_args:
            logging.error('invalid parameter: ' + arg)
            return '1401', {}

    if 'id' in args:
        id = args['id']
    else:
        logging.error('required parameter missing: id')
        return '1400', {}

    parameters_request = []
    if 'parameters' in args:
        parameters_request = args['parameters'].split(',')

    if parameters_request != []:
        parameters_request.append('time')

    names = []
    for item in md_rdataset:
        names.append(item['name'])

    statement = 'SELECT '
    statement += ' , '.join(names)
    statement += ' FROM md_rdataset WHERE id = ?'

    init_db()

    con = sqlite3.connect(config['database_path'])
    cur = con.cursor()
    cur.execute(statement, (id,))
    rows = cur.fetchall()
    con.commit()
    con.close()

    if rows == []:
        return '1406', {}

    reply = {}

    for i in range(len(names)):
        reply[names[i]] = list(rows[0])[i]

    names = []
    for item in md_rsubset:
        names.append(item['name'])

    statement = 'SELECT '
    statement += ' , '.join(names)
    statement += ' FROM md_rsubset WHERE rdatasetID = ?'

    con = sqlite3.connect(config['database_path'])
    cur = con.cursor()
    cur.execute(statement, (id,))
    rows = cur.fetchall()
    con.commit()
    con.close()

    subsets = []
    for row in rows:
        subset = {}
        for i in range(len(names)):
            subset[names[i]] = list(row)[i]
        subsets.append(subset)

    reply['subsets'] = subsets

    names = []
    for item in md_rparameter:
        names.append(item['name'])

    statement = 'SELECT '
    statement += ' , '.join(names)
    statement += ' FROM md_rparameter WHERE rdatasetID = ?'

    con = sqlite3.connect(config['database_path'])
    cur = con.cursor()
    cur.execute(statement, (id,))
    rows = cur.fetchall()
    con.commit()
    con.close()

    parameters = []
    for row in rows:
        parameter = {}
        for i in range(len(names)):
            parameter[names[i]] = list(row)[i]
        if parameters_request == [] or parameter['name'] in parameters_request:
            parameters.append(parameter)

    reply['parameters'] = parameters

    return '1200', reply


def add_rdata(dataContainer, replace=False):

    if 'id' in dataContainer:
        id = dataContainer['id']
    else:
        logging.error('required parameter missing: {}'.format('id'))
        return '1400', {}

    if 'parameters' in dataContainer:
        parameters = dataContainer['parameters']
    else:
        logging.error('required parameter missing: {}'.format('parameters'))
        return '1400', {}

    if 'data' in dataContainer:
        data = dataContainer['data']
    else:
        logging.error('required parameter missing: {}'.format('data'))
        return '1400', {}

    init_db()

    with sqlite3.connect(config['database_path']) as con:

        statement = 'SELECT resampleMethod FROM md_rdataset WHERE id = ?'

        cur = con.cursor()
        cur.execute(statement, (id,))
        rows = cur.fetchall()

        if len(rows) == 0:
            return '1406', {}

        resampleMethod = rows[0][0]

        statement = 'SELECT cadence FROM md_rsubset WHERE rdatasetID = ?'

        cur = con.cursor()
        cur.execute(statement, (id,))
        rows = cur.fetchall()

        if len(rows) == 0:
            return '1406', {}

        cadences = []
        logging.info(rows)  # DEBUG
        for row in rows:
            cadences.append([row[0], pd.to_timedelta(row[0])])
        cadences.sort(key=lambda x: x[1])

        df = rows_to_dataframe(data, parameters, keep_time_col=True)

        dataset_to_store = id + '_' + cadences[0][0]

        dataContainer = {}
        dataContainer['id'] = dataset_to_store
        dataContainer['parameters'] = parameters
        dataContainer['data'] = df.values.tolist()

        status, reply = add_data(dataContainer, replace=replace)
        if status != '1200':
            return status, reply

        # Perform resampling to other cadences
        # resample_rdata({'id': id})

        # Update the modification date and start/end time of the dataset
        statement = (f'SELECT MIN(time), MAX(time) FROM {id}_'
                     f'{cadences[0][0]}')
        cur.execute(statement)
        rows = cur.fetchall()

        modificationDate = datetime.today().strftime('%Y-%m-%dT%H:%M:%SZ')
        startDate = rows[0][0] if rows[0][0] is not None else ''
        stopDate = rows[0][1] if rows[0][1] is not None else ''
        statement = 'UPDATE md_rdataset SET '
        statement += 'modificationDate = ?, startDate = ?, stopDate = ? '
        statement += 'WHERE id = ?'
        cur.execute(statement, (modificationDate, startDate, stopDate, id))

    return '1200', {}


def get_rdata(args):
    allowed_args = ['id', 'time.min', 'time.max', 'parameters', 'include',
                    'format', 'resample']
    for arg in args:
        if arg not in allowed_args:
            logging.error('invalid parameter: ' + arg)
            return '1401', 'json', {}, {}

    if 'id' in args:
        id = args['id']
    else:
        logging.error('required parameter missing: id')
        return '1400', 'json', {}, {}

    if 'time.min' in args:
        time_min = args['time.min']
    else:
        logging.error('required parameter missing: time.min')
        return '1400', 'json', {}, {}

    if 'time.max' in args:
        time_max = args['time.max']
    else:
        logging.error('required parameter missing: time.max')
        return '1400', 'json', {}, {}

    parameters_request = []
    if 'parameters' in args:
        parameters_request = args['parameters'].split(',')

    if parameters_request != []:
        if 'time' not in parameters_request:
            parameters_request.append('time')

    include = ''
    if 'include' in args:
        include = args['include']

    format = 'csv'
    if 'format' in args:
        format = args['format']
    if format not in ['json', 'csv']:
        logging.error('invalid or unsupported parameter: format: ' + format)
        return '1400', 'json', {}, {}

    resample = ''
    if 'resample' in args:
        resample = args['resample']

    try:
        time_min = datetime.strptime(time_min, '%Y-%m-%dT%H:%M:%SZ')
    except:
        logging.error('invalid parameter: time.min: ' + time_min)
        return '1402', 'json', {}, {}

    try:
        time_max = datetime.strptime(time_max, '%Y-%m-%dT%H:%M:%SZ')
    except:
        logging.error('invalid parameter: time.max: ' + time_max)
        return '1403', 'json', {}, {}

    time_min = time_min.strftime('%Y-%m-%dT%H:%M:%SZ')
    time_max = time_max.strftime('%Y-%m-%dT%H:%M:%SZ')

    # all inputs checked now, get data from database

    init_db()

    with sqlite3.connect(config['database_path']) as con:

        statement = 'SELECT resampleMethod FROM md_rdataset WHERE id = ?'

        cur = con.cursor()
        cur.execute(statement, (id,))
        rows = cur.fetchall()

        if len(rows) == 0:
            return '1406', 'json', {}, {}

        resampleMethod = rows[0][0]

        statement = 'SELECT cadence FROM md_rsubset WHERE rdatasetID = ?'

        cur = con.cursor()
        cur.execute(statement, (id,))
        rows = cur.fetchall()

        if len(rows) == 0:
            return '1406', 'json', {}, {}

        resolutions = []
        for row in rows:
            resolutions.append([row[0], parse_isoduration(row[0])])
        resolutions.sort(key=lambda x: x[1])

        # default use the highest resolution
        resolution_use = resolutions[0][0]

        if resample != '':
            for resolution in resolutions:
                if parse_isoduration(resample) >= parse_isoduration(resolution[0]):
                    resolution_use = resolution[0]
            frequency_use = parse_isoduration(resample)
            if frequency_use < parse_isoduration(resolutions[0][0]):
                frequency_use = parse_isoduration(resolutions[0][0])
            frequency = str(frequency_use) + 'S'

        dataset = id + '_' + resolution_use

        names = []
        for item in md_parameter:
            names.append(item['name'])

        statement = 'SELECT '
        statement += ', '.join(names)
        statement += ' FROM md_parameter WHERE datasetID = ?'

        cur = con.cursor()
        cur.execute(statement, (dataset,))
        rows = cur.fetchall()

        if len(rows) == 0:
            return '1406', 'json', {}, {}

        parameters = []
        parameter_names = []
        for row in rows:
            parameter = {}
            for i in range(len(names)):
                parameter[names[i]] = list(row)[i]
            if parameters_request == [] or parameter['name'] in parameters_request:
                parameter_names.append(parameter['name'])
                parameters.append(parameter)

        start = timer()

        statement = 'SELECT '
        statement += ', '.join(parameter_names)
        statement += ' FROM ' + dataset + ' WHERE time >= ? AND time < ? ORDER BY time'

        cur = con.cursor()
        cur.execute(statement, (time_min, time_max))
        rows = cur.fetchall()

        end = timer()
        logging.info('TIMING: ' + id + ': database access: ' + str(end - start) + ' rows: ' + str(len(rows)))
        start = timer()

        df = rows_to_dataframe(rows, parameter_names, keep_time_col=False)

        # if resampleMethod == 'max':
        #     df = df.apply(pd.to_numeric)
        #     df = df.resample(frequency).max()
        # elif resampleMethod == 'mean':
        #     df = df.apply(pd.to_numeric)
        #     df = df.resample(frequency).mean()
        # elif resampleMethod == 'first':
        #     df = df.resample(frequency).first()
        # elif resampleMethod == 'last':
        #     df = df.resample(frequency).last()
        # else:
        #     pass # TODO

        end = timer()
        logging.info('TIMING: ' + id + ': resample: ' + str(end - start))

    if format == 'json':
        time = df.index.strftime('%Y-%m-%dT%H:%M:%SZ')
        df.insert(0, 'time', time)
        reply_header = {}
        reply = {}
        reply['parameters'] = parameters
        reply['data'] = df.values.tolist()
    else:
        reply_header = {}
        if include == 'header':
            reply_header['parameters'] = parameters
        elif include == 'brief_header':
            reply_header = ','.join(parameter_names)
        reply = df.to_csv(index=True, date_format='%Y-%m-%dT%H:%M:%S.%fZ', float_format='%g', header=False)

    return '1200' if len(rows) > 0 else '1201', format, reply, reply_header


def resample_rdata(args):
    allowed_args = ['id']
    for arg in args:
        if arg not in allowed_args:
            logging.error('invalid parameter: ' + arg)
            return '1401', 'json', {}, {}

    if 'id' in args:
        id = args['id']
    else:
        logging.error('required parameter missing: id')
        return '1400', 'json', {}, {}

    # Get basic information on the dataset
    response, dataset = get_rdataset({'id': id})
    iso_cadences = [subset['cadence'] for subset in dataset['subsets'][:]]
    parameter_names = [par['name'] for par in dataset['parameters']]
    resample_method = dataset['resampleMethod']

    # Connect to the database
    init_db()
    with sqlite3.connect(config['database_path']) as con:
        # Fetch the entire dataset for the highest cadence
        logging.info("Reading full-rate data before interpolation")
        dataset = id + '_' + iso_cadences[0]
        statement = 'SELECT '
        statement += ', '.join(parameter_names)
        statement += ' FROM ' + dataset + ' ORDER BY time'
        cur = con.cursor()
        cur.execute(statement)
        rows = cur.fetchall()

    # Convert to pandas dataframe
    df = rows_to_dataframe(rows, parameter_names)

    # Interpolate the dataset at lower cadences and store in db
    for iso_cadence in iso_cadences[1:]:
        dataset_to_store = id + '_' + iso_cadence
        logging.info(f"Interpolating data to lower cadence: {dataset_to_store}")
        new_cadence = pd.to_timedelta(iso_cadence)
        # Select the resampling method specified in the metadata
        if resample_method == 'max':
            df = df.apply(pd.to_numeric)
            df_resampled = df.resample(new_cadence).max()
        elif resample_method == 'min':
            df = df.apply(pd.to_numeric)
            df_resampled = df.resample(new_cadence).min()
        elif resample_method == 'mean':
            df = df.apply(pd.to_numeric)
            df_resampled = df.resample(new_cadence).mean()
        elif resample_method == 'first':
            df_resampled = df.resample(new_cadence).first()
        elif resample_method == 'last':
            df_resampled = df.resample(new_cadence).last()
        else:
            logging.error("Unknown resampling method specification: "
                          f"{resample_method}")
            df_resampled = df.copy()

        # Pandas assigns the leftmost time-tag of the resampling interval
        #   to the new index. We should add half the interval, to put the
        #   new time tags at the middle of the resampling interval.
        new_timeindex = df_resampled.index  # + 0.5 * new_cadence
        time = new_timeindex.strftime('%Y-%m-%dT%H:%M:%SZ')
        df_resampled.insert(0, 'time', time)

        dataContainerResampled = {}
        dataContainerResampled['id'] = dataset_to_store
        dataContainerResampled['parameters'] = parameter_names
        dataContainerResampled['data'] = df_resampled.values.tolist()

        with sqlite3.connect(config['database_path']) as con:
            status, reply = add_data(dataContainerResampled, replace=True)
            if status != '1200':
                return status, reply


def retrieve_dataframe(data_id, t0, t1):
    import numpy as np
    import pandas as pd

    # Retrieve parameters
    returncode, reply = get_rdataset({'id': data_id})
    if returncode == '1200':
        columns = [parameter['name'] for parameter in reply['parameters']]
    else:
        print("Error on info request", returncode, reply)
        return None

    tmin = pd.to_datetime(t0).strftime('%Y-%m-%dT%H:%M:%SZ')
    tmax = pd.to_datetime(t1).strftime("%Y-%m-%dT%H:%M:%SZ")

    # Retrieve the data
    returncode, returnformat, reply, reply_header = get_rdata(
        {'id': data_id, 'parameters': ','.join(columns),
         'time.min': tmin, 'time.max': tmax, 'format': 'json'}
    )

    if returncode == '1200':
        timeindex = pd.to_datetime(np.array(reply['data'])[:, 0])
        df = pd.DataFrame(data=reply['data'], columns=columns, index=timeindex)
        df.drop('time', axis=1, inplace=True)
        return df
    else:
        print("Error on data request", returncode, returnformat, reply, reply_header)
        return None
