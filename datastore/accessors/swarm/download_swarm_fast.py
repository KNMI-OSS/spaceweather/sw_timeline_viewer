#!/usr/bin/env python3
from swxtools.access import swarm_diss

for data_type in ['MODx_SC', 'EFIx_LP', 'MAGx_LR']:
    for sat in ['Swarm A', 'Swarm B', 'Swarm C' ]:
        files = swarm_diss.download(sat=sat, data_type=data_type, fast=True)
