import os
import io
import re
import requests
import time
import numpy as np
import pandas as pd
import netCDF4
import logging
import datetime
from dateutil.relativedelta import relativedelta
import calendar
from datastore.accessors import download_tools
from datastore.config import config


def filename_from_url(url):
    return url[url.rfind("/")+1:len(url)]


def download(download_information):
    year = download_information['firstdayofmonth'][0:4]
    month = download_information['firstdayofmonth'][4:6]
    goessats = download_information['goes_id']
    firstdayofmonth = download_information['firstdayofmonth']
    lastdayofmonth = download_information['lastdayofmonth']

    local_data_dir = (f"{config['local_source_data_path']}/"
                      f"goes_eps/{year}")
    download_tools.ensure_data_dir(local_data_dir)

    filenames = []
    for satno in goessats:
        # for GOES satellites older than 2010 the detectors and thus naming convention
        # is different. Still need to fix this

        switchtime = datetime.datetime.strptime(year + month, "%Y%m")
        if switchtime < datetime.datetime(2010,1,1):
            url = (f"https://www.ncei.noaa.gov/data/goes-space-environment-monitor/access/avg/{year}/" +
               f"{month}/goes{satno:02d}/netcdf/g{satno:02d}_eps_5m_" +
               f"{firstdayofmonth}_{lastdayofmonth}.nc")
        elif switchtime >= datetime.datetime(2010,1,1) and switchtime <= datetime.datetime(2019,12,1):
            url = (f"https://www.ncei.noaa.gov/data/goes-space-environment-monitor/access/{year}/" +
               f"{month}/goes{satno}/netcdf/g{satno}_epead_cpflux_5m_" +
               f"{firstdayofmonth}_{lastdayofmonth}.nc")
        # satellite changes happens on 2019-12-09T00:00:00 so download the datasets
        # of both GOES 15 and 16 that month
        elif switchtime >= datetime.datetime(2019,12,1):
            # The data for GOES-R are not available ...
            # use the old link to prevent the update from crashing
            # URL for new data is like this: https://data.ngdc.noaa.gov/platforms/solar-space-observing-satellites/goes/goes16/l2/data/sgps-l2-avg5m/
            url = (f"https://www.ncei.noaa.gov/data/goes-space-environment-monitor/access/{year}/" +
               f"{month}/goes{satno}/netcdf/g{satno}_epead_cpflux_5m_" +
               f"{firstdayofmonth}_{lastdayofmonth}.nc")

        filename = f"{local_data_dir}/{filename_from_url(url)}"

        # Check if file exists
        if not os.path.isfile(filename):
            downloadsucces = download_tools.download_file_http(url, filename)
        else:
            downloadsucces = True
            logging.info(f"File already exists at {filename}")

        if downloadsucces:
            filenames.append([filename, satno])

    return filenames


def nc_to_dataframe(file, convert_column_names=False):
    ncdata = netCDF4.Dataset(file)
    ncvariables = ncdata.variables.keys()

    time_var = ncdata.variables['time_tag']
    time_units = ncdata.variables['time_tag'].units
    # milliseconds since 1970-01-01 00:00:00.0 UTC
    (units, time_base_str) = time_units.split(" since ")
    time_index = (pd.to_datetime(time_base_str, utc=True) +
                  pd.to_timedelta(time_var[:], units))

    # Collect the data
    framedata = {}
    for key in ncdata.variables.keys():
        var = ncdata.variables[key]

        #  Collect from columns that match the time-tag's dimension
        if var.dimensions == time_var.dimensions:
            values = var[:].astype(float)

            # Set missing values to NaN (old format data only)
            if 'missing_value' in var.ncattrs():
                missing_value = float(var.getncattr('missing_value'))
                mask = values == missing_value
                framedata[key] = np.where(mask, np.nan, values)
            else:
                framedata[key] = values

    ncdata.close()
    df = pd.DataFrame(data=framedata, index=time_index)

    return df


def goes_eps_primary_to_dataframe():
    t_now = pd.Timestamp.utcnow().strftime("%Y-%m-%dT%H:%M:%S")
    date_now = datetime.datetime.strptime(t_now, '%Y-%m-%dT%H:%M:%S').date()
    # Source doc for definitions of primary vs secondary:
    #   https://www.ngdc.noaa.gov/stp/satellite/goes/doc/GOES_XRS_readme.pdf
    #
    goes_primaries = f'''
    starttime             endtime                primary  secondary
    1986-01-01T00:00:00   1988-01-26T00:00:00          6          5
    1988-01-26T00:00:00   1994-12-11T00:00:00          7          6
    1994-12-11T00:00:00   1995-03-01T00:00:00          7          8
    1995-03-01T00:00:00   1998-07-27T00:00:00          8          7
    1998-07-27T00:00:00   2003-04-08T15:00:00          8         10
    2003-04-08T15:00:00   2003-05-15T15:00:00         10         12
    2003-05-15T15:00:00   2006-06-28T14:00:00         12         10
    2006-06-28T14:00:00   2007-01-01T00:00:00         12         11
    2007-01-01T00:00:00   2007-04-12T00:00:00         10         11
    2007-04-12T00:00:00   2007-11-21T00:00:00         11         10
    2007-11-21T00:00:00   2007-12-05T00:00:00         11       None
    2007-12-05T00:00:00   2007-12-18T00:00:00         11         10
    2007-12-18T00:00:00   2008-02-10T16:30:00         11       None
    2008-02-10T16:30:00   2009-12-01T00:00:00         10       None
    2009-12-01T00:00:00   2010-09-01T00:00:00         14       None
    2010-09-01T00:00:00   2010-10-28T00:00:00         14         15
    2010-10-28T00:00:00   2011-09-01T00:00:00         15       None
    2011-09-01T00:00:00   2012-10-23T16:00:00         15         14
    2012-10-23T16:00:00   2012-11-19T16:31:00         14         15
    2012-11-19T16:31:00   2015-01-26T16:01:00         15       None
    2015-01-26T16:01:00   2015-05-21T18:00:00         15         13
    2015-05-21T18:00:00   2015-06-09T16:25:00         14         13
    2015-06-09T16:25:00   2016-05-03T13:00:00         15         13
    2016-05-03T13:00:00   2016-05-12T17:30:00         13         14
    2016-05-12T17:30:00   2016-05-16T17:00:00         14         13
    2016-05-16T17:00:00   2016-06-09T17:30:00         14         15
    2016-06-09T17:30:00   2017-12-12T16:30:00         15         13
    2017-12-12T16:30:00   2019-12-09T00:00:00         15         14
    2019-12-09T00:00:00   {t_now}                     16         17
    '''

    with io.StringIO(goes_primaries) as f:
        goes_primary_df = pd.read_table(f, delim_whitespace=True,
                                        parse_dates=True, na_values='None')
    goes_primary_dict = goes_primary_df.to_dict(orient='records')

    # The data is organised per month, so make a list of months starting at the 1st
    startdate = datetime.datetime.strptime(goes_primary_dict[0]['starttime'], '%Y-%m-%dT%H:%M:%S').date()
    enddate = datetime.datetime.strptime(goes_primary_dict[-1]['endtime'], '%Y-%m-%dT%H:%M:%S').date()
    monthlist = []
    currentstep = startdate
    while currentstep <= enddate:
        monthlist.append(currentstep)
        currentstep += relativedelta(months=1)

    # Depending on the year and month, the data come from a different satellite.
    # We need to know during which month we need to download data from which satellite.
    # Sometimes, a 'switch' to a newer satellite occurs, in those months we need
    # to download both data sets.
    filelist_new_format = np.array([]).reshape(0,2)
    filelist_old_format = np.array([]).reshape(0,2)
    download_information = {}
    for monthdate in monthlist:
        firstdayofmonth = monthdate.strftime('%Y%m%d')
        daysinmonth = calendar.monthrange(monthdate.year, monthdate.month)[1]
        lastdayofmonth = datetime.date(monthdate.year, monthdate.month, daysinmonth)
        lastdayofmonth_str = lastdayofmonth.strftime('%Y%m%d')

        # Check which GOES satellite(s) we need
        for i, goesstep in enumerate(goes_primary_dict):
            stepstartdate = datetime.datetime.strptime(goesstep['starttime'], '%Y-%m-%dT%H:%M:%S').date()
            stependdate = datetime.datetime.strptime(goesstep['endtime'], '%Y-%m-%dT%H:%M:%S').date()

            # Find the GOES period in which the current month falls
            if monthdate >= stepstartdate and monthdate < stependdate:
                download_information['goes_id'] = [goesstep['primary']]
                download_information['firstdayofmonth'] = firstdayofmonth
                download_information['lastdayofmonth'] = lastdayofmonth_str

                # Sometimes, the 'switch' to another satellite falls in the current month.
                # In those cases, two files need to be downloaded
                if lastdayofmonth > stependdate and stependdate < date_now:
                    if goesstep['primary'] != goes_primary_dict[i+1]['primary']: # to avoid the entries where only secondary changed
                        download_information['goes_id'] = [goesstep['primary'], goes_primary_dict[i+1]['primary']]

        goesid = download_information['goes_id']

        # Download the data
        logging.info(f"Starting to download data for month: {firstdayofmonth}-{lastdayofmonth_str}, from GOES id(s): {goesid}.")
        filenames = download(download_information)
        if filenames:
            if monthdate.year < 2010:
                filelist_old_format = np.concatenate([filelist_old_format,filenames])
            elif monthdate.year >= 2010:
                filelist_new_format = np.concatenate([filelist_new_format,filenames])

    ## Now that everything is downloaded, load it into a dataframe
    # Old format
    dfs_multifile = []
    for file, goesnum in filelist_old_format:
        logging.info(f"Starting to merge {file}")
        df = nc_to_dataframe(file, convert_column_names=True)
        df['satellite'] = f'goes{goesnum}'
        dfs_multifile.append(df)
    df_total_old_format = pd.concat(dfs_multifile).sort_index()
    df_total_old_format.index.name = 'timeindex'
    df_total_old_format = df_total_old_format.reset_index() # Needed because with the dates as index, we have non unique indices

    # New format
    dfs_multifile = []
    for file, goesnum in filelist_new_format:
        logging.info(f"Starting to merge {file}")
        df = nc_to_dataframe(file, convert_column_names=True)
        df['satellite'] = f'goes{goesnum}'
        dfs_multifile.append(df)
    df_total_new_format = pd.concat(dfs_multifile).sort_index()
    df_total_new_format.index.name = 'timeindex'
    df_total_new_format = df_total_new_format.reset_index() # Needed because with the dates as index, we have non unique indices

    ## Select only the data from the primary satellite, i.e. remove parts of the
    ## double months we downloaded when a satellite 'switch' happened
    # Old format
    indicestodrop = np.array([])
    for timespan in goes_primary_dict:
        begin = datetime.datetime.strptime(timespan['starttime'], '%Y-%m-%dT%H:%M:%S')
        beginms = begin.replace(tzinfo=datetime.timezone.utc).timestamp() * 1000 # in miliseconds
        end = datetime.datetime.strptime(timespan['endtime'], '%Y-%m-%dT%H:%M:%S')
        endms = end.replace(tzinfo=datetime.timezone.utc).timestamp() * 1000 # in miliseconds
        goesnum = timespan['primary']

        # Select in dataframe the periods where these three things do not match
        # and delete those rows
        looseindices = df_total_old_format.query(f"(time_tag >= {beginms}) & (time_tag < {endms}) & (satellite != 'goes{goesnum}')").index.to_numpy()
        indicestodrop = np.concatenate((indicestodrop, looseindices))

    print(len(indicestodrop))
    logging.info(f"Clean up dataframe (because of satellite changes some files contain overlapping dates)")
    df_total_old_format.drop(indicestodrop, inplace=True)

    # Change index back to previous one
    df_total_old_format.set_index('timeindex', inplace=True)

    # New format
    indicestodrop = np.array([])
    for timespan in goes_primary_dict:
        begin = datetime.datetime.strptime(timespan['starttime'], '%Y-%m-%dT%H:%M:%S')
        beginms = begin.replace(tzinfo=datetime.timezone.utc).timestamp() * 1000 # in miliseconds
        end = datetime.datetime.strptime(timespan['endtime'], '%Y-%m-%dT%H:%M:%S')
        endms = end.replace(tzinfo=datetime.timezone.utc).timestamp() * 1000 # in miliseconds
        goesnum = timespan['primary']

        # Select in dataframe the periods where these three things do not match
        # and delete those rows
        looseindices = df_total_new_format.query(f"(time_tag >= {beginms}) & (time_tag < {endms}) & (satellite != 'goes{goesnum}')").index.to_numpy()
        indicestodrop = np.concatenate((indicestodrop, looseindices))

    logging.info(f"Clean up dataframe (because of satellite changes some files contain overlapping dates)")
    df_total_new_format.drop(indicestodrop, inplace=True)

    # Change index back to previous one
    df_total_new_format.set_index('timeindex', inplace=True)

    ## Return both dataframes
    return df_total_old_format, df_total_new_format
