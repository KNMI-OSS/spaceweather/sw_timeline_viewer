import os
import get_hapi_session_cookies
import json 
import requests 
import pandas as pd
import re
from time import sleep
import configparser

# Local path to store the NetCDF files
LOCAL_PATH = f'{os.environ["HOME"]}/researchdrive/Space Weather Services/enlil/metoffice/'

# Configuration for accessing Met Office data via ESA's portal
config = configparser.ConfigParser()
config.read('swe_auth.txt')
username = config['DEFAULT']['username']
password = config['DEFAULT']['password']

def download_enlil_file(username, password, filename, local_filename):
    session_established, auth_cookie = get_hapi_session_cookies.get_auth_cookie(username, password) 
    if session_established:
        url = 'https://esa.spaceweather.api.metoffice.gov.uk/models/enlil/files/' + filename
        print(f"Session established, accessing {url}")
        response = requests.get(url, cookies = {'iPlanetDirectoryPro': auth_cookie})
        if response.ok:
            data = response.content
            with open(local_filename, 'wb') as fh:
                fh.write(data)
            print(f"{pd.Timestamp.utcnow()}: {filename} was written")
        else:
            print("Request failed...")
    else:
        print("Unable to establish session...")

def download_enlil_files_timerange(username, password, local_path, t0, t1):
    date_range = pd.date_range(t0, t1, freq='2H')

    for date in date_range:
        date_str = date.strftime("%Y-%m-%dT%HZ")
        filename = f"{date_str}.evo.Earth.nc"
        local_filename = f'{local_path}{filename}'
        if os.path.isfile(local_filename):
            print(f"file {filename} already exists locally")
        else:
            download_enlil_file(username, password, filename, local_filename)

t1 = pd.Timestamp.utcnow().floor('2H')
t0 = t1 - pd.to_timedelta('3D')

download_enlil_files_timerange(username, password, LOCAL_PATH, t0, t1)



import os
import glob
import xarray as xr
import pygmt
import numpy as np
import pandas as pd
from datastore import datastore
from datastore.ingestors import ingest_tools




# Local path to store the NetCDF files
LOCAL_PATH = f'{os.environ["HOME"]}/researchdrive/Space Weather Services/enlil/metoffice/'

enlilfiles = glob.glob(LOCAL_PATH + "/*.nc")
enlilfiles = list(filter(lambda file: "T00Z" in file or "T12Z" in file or "T06Z" in file or "T18Z" in file, enlilfiles))
enlilfiles.sort()
enlil_times = {file: pd.to_datetime(file.split('/')[-1].split('.')[0]) for file in enlilfiles}

timefmt = "%Y-%m-%dT%H:%M:%SZ"

def parse_enlilfile(enlilfile, enlil_times):
    reftime = enlil_times[enlilfile]
    ds = xr.open_dataset(enlilfile)
    df = ds.to_dataframe()
    df.index = reftime + pd.to_timedelta(df['TIME'], 's')
    df = df.resample('30min').mean()
    df.index = df.index + pd.to_timedelta('15min')
    df['density'] = 1e-6 * df['D'] / (1.67262e-27) # kg/m3 to protons/cm3
    df['speed'] = 1e-3 * np.sqrt(df['V1']**2 + df['V2']**2 + df['V3']**2)
    df['pressure'] = 1.6726e-6 * df['density'] * df['speed']**2
    df['bt'] = 1e9 * np.sqrt(df['B1']**2 + df['B2']**2 + df['B3']**2)
    df.rename({'T': 'temperature'}, axis=1, inplace=True)
    df['time'] = df.index.strftime(timefmt)
    df['timetag_issue'] = reftime.strftime(timefmt)
    return df

for enlilfile in enlilfiles:
    print(enlilfile)
    df = parse_enlilfile(enlilfile, enlil_times)
    # Put in database
    parameters = ['time', 'timetag_issue', 'density', 'speed', 'temperature', 'pressure', 'bt']
    table_id = 'solar_wind_plasma_forecast_metoffice'
    data = df[parameters].values.tolist()
    ingest_tools.store(table_id, parameters, data, update=False)
