#!/usr/bin/env python3

import os, sys, re
sys.path.append('..')

import datastore as ds
import pandas as pd
import numpy as np
import logging
import datetime
import matplotlib
import matplotlib.pyplot as plt
import ingest_kp_index_forecast_metoffice_geoweb as ig

# ------------------------------------------------------------------------------

if __name__ == "__main__":

    format = "%(asctime)s [%(levelname)s] %(message)s"
    logging.basicConfig(format=format, level=logging.INFO, datefmt="%Y-%m-%d %H:%M:%S")

    #ig.ingest_kp_index_forecast_metoffice_geoweb()

    df = ds.get_timeseries('kp_index_forecast_metoffice_geoweb', '1900-01-01 00:00:00', '2100-01-01 00:00:00')
    print(df)

    #time = df.index.strftime('%H')
    #df.index = time
    #df["kp_index"].plot(kind="bar");
    #plt.show()

# ------------------------------------------------------------------------------
