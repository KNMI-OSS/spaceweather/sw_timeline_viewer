#!/usr/bin/env python3

import os, sys, re
sys.path.append('..')

import logging
import datetime
import datastore as ds

# ------------------------------------------------------------------------------

if __name__ == "__main__":

    format = "%(asctime)s [%(levelname)s] %(message)s"
    logging.basicConfig(format=format, level=logging.INFO, datefmt="%Y-%m-%d %H:%M:%S")

    df = ds.get_timeseries('solar_wind_mag_ace', '2000-01-01 00:00:00', '2000-01-05 00:00:00')
    print(df)
    
    df = ds.get_timeseries('solar_wind_mag_ace', '2000-01-01 00:00:00', '2000-01-05 00:00:00', resolution = '3H')
    print(df)
    
# ------------------------------------------------------------------------------
