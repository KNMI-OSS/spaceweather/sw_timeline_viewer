#!/usr/bin/env python3

import os, sys, re
sys.path.append('..')

import logging
import datastore as ds

# ------------------------------------------------------------------------------

if __name__ == "__main__":

    format = "%(asctime)s [%(levelname)s] %(message)s"
    logging.basicConfig(format=format, level=logging.INFO, datefmt="%Y-%m-%d %H:%M:%S")

    status = ds.get_status()
    print(status)

# ------------------------------------------------------------------------------

