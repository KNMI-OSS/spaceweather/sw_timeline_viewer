#!/usr/bin/env python3

import os, sys, re
sys.path.append('..')

import logging
import datetime
import datastore as ds
import matplotlib
import matplotlib.pyplot as plt
import ingest_solar_wind_mag_dscovr as ig

# ------------------------------------------------------------------------------

if __name__ == "__main__":

    format = "%(asctime)s [%(levelname)s] %(message)s"
    logging.basicConfig(format=format, level=logging.INFO, datefmt="%Y-%m-%d %H:%M:%S")

    # use string time stamps
    ig.ingest_solar_wind_mag_dscovr(t0 = '2021-01-01', t1 = '2021-01-02')

    # use string time stamps, keep downloaded files after ingestion
    #ig.ingest_solar_wind_mag_dscovr(t0 = '2021-01-01', t1 = '2021-01-02', keep = True)

    # use datetime objects
    #ig.ingest_solar_wind_mag_dscovr(t0 = datetime.datetime.strptime('2021-01-01', '%Y-%m-%d'), t1 = datetime.datetime.strptime('2021-01-10', '%Y-%m-%d'))

    # use file
    #ig.ingest_solar_wind_mag_dscovr(file = 'oe_m1m_dscovr_s20200101000000_e20200101235959_p20200102022225_pub.nc.gz')

    df = ds.get_timeseries('solar_wind_mag_dscovr', '1900-01-01 00:00:00', '2100-01-01 00:00:00')
    print(df)

    #df.plot();
    #plt.show()

    
# ------------------------------------------------------------------------------
