#!/usr/bin/env python3

import os, sys, re
sys.path.append('..')

import logging
import datetime
import datastore as ds
import ingest_kp_index as ig

# ------------------------------------------------------------------------------

if __name__ == "__main__":

    format = "%(asctime)s [%(levelname)s] %(message)s"
    logging.basicConfig(format=format, level=logging.INFO, datefmt="%Y-%m-%d %H:%M:%S")

    ig.ingest_kp_index()

    # load the full kp index set
    #ig.ingest_kp_index(t0 = datetime.datetime.strptime('1932', '%Y'), t1 = datetime.datetime.strptime('2021', '%Y'))

    df = ds.get_timeseries('kp_index', '1900-01-01 00:00:00', '2100-01-01 00:00:00')
    print(df)

    #df = ds.get_timeseries('kp_index',  datetime.datetime.strptime('1900-01-01 00:00:00', '%Y-%m-%d %H:%M:%S'), datetime.datetime.now())
    #print(df)

# ------------------------------------------------------------------------------
