#!/usr/bin/env python3

import os, sys, re
sys.path.append('..')

import logging
import datetime
import datastore as ds
import ingest_solar_wind_plasma_rt

# ------------------------------------------------------------------------------

if __name__ == "__main__":

    format = "%(asctime)s [%(levelname)s] %(message)s"
    logging.basicConfig(format=format, level=logging.INFO, datefmt="%Y-%m-%d %H:%M:%S")

    ingest_solar_wind_plasma_rt.ingest_solar_wind_plasma_rt()

    df = ds.get_timeseries('solar_wind_plasma_rt', '1900-01-01 00:00:00', '2100-01-01 00:00:00')
    print(df)
    
# ------------------------------------------------------------------------------
