#!/usr/bin/env python3

import os, sys, re
sys.path.append('..')

import logging
import datetime
import datastore as ds
import ingest_solar_wind_plasma_ace as ig

# ------------------------------------------------------------------------------

if __name__ == "__main__":

    format = "%(asctime)s [%(levelname)s] %(message)s"
    logging.basicConfig(format=format, level=logging.INFO, datefmt="%Y-%m-%d %H:%M:%S")

    # range of years
    # ig.ingest_solar_wind_plasma_ace(t0 = datetime.datetime.strptime('1997', '%Y'), t1 = datetime.datetime.strptime('2020', '%Y'))

    # single year
    ig.ingest_solar_wind_plasma_ace(t0 = datetime.datetime.strptime('2016', '%Y'))

    # file
    # ig.ingest_solar_wind_plasma_ace(file = 'mag_data_16sec_year2015.hdf'))

    # current year
    # ig.ingest_solar_wind_plasma_ace()

    df = ds.get_timeseries('solar_wind_plasma_ace', '1900-01-01 00:00:00', '2100-01-01 00:00:00')
    print(df)
    
# ------------------------------------------------------------------------------
