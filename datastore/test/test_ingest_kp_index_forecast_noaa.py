#!/usr/bin/env python3

import os, sys, re
sys.path.append('..')

import datastore as ds
import logging
import datetime
import ingest_kp_index_forecast_noaa as ig

# ------------------------------------------------------------------------------

if __name__ == "__main__":

    format = "%(asctime)s [%(levelname)s] %(message)s"
    logging.basicConfig(format=format, level=logging.INFO, datefmt="%Y-%m-%d %H:%M:%S")

    ig.ingest_kp_index_forecast_noaa()

    df = ds.get_timeseries('kp_index_forecast_noaa', '1900-01-01 00:00:00', '2100-01-01 00:00:00')
    print(df)

# ------------------------------------------------------------------------------
