//sdo_sets = ["0094", "0171", "0193", "0211", "0304", "0335", "1600", "1700", "HMIIC", "HMIBC"];
sdo_sets = ["0094", "0193", "0304", "1700", "HMIIC", "HMIBC"];
titles = 
{"0094": 'AIA 94', 
 "0171": 'AIA 171',
 "0193": 'AIA 193', 
 "0211": 'AIA 211', 
 "0304": 'AIA 304', 
 "0335": 'AIA 335', 
 "1600": 'AIA 1600', 
 "1700": 'AIA 1700', 
 "HMIIC": 'HMI Continuum', 
 "HMIBC": 'HMI Magnetogram'};

sdocolors = {
  '0094': {
  0.125: '#043d20',
  0.250: '#105a40',
  0.375: '#247460',
  0.500: '#408d80',
  0.625: '#64a8a0',
  0.750: '#90c3c0',
  0.875: '#c4e0e0',
  },
  '0171': {
  0.125: '#2e2000',
  0.250: '#5c4000',
  0.375: '#8b6000',
  0.500: '#b98000',
  0.625: '#e7a000',
  0.750: '#ffc007',
  0.875: '#ffe085',
  },
  '0193': {
  0.125: '#5a2004',
  0.250: '#7f4010',
  0.375: '#9c6024',
  0.500: '#b48040',
  0.625: '#c9a064',
  0.750: '#ddc090',
  0.875: '#eee0c4',
  },
  '0211': {
  0.125: '#5a203d',
  0.250: '#7f405a',
  0.375: '#9c6074',
  0.500: '#b4808d',
  0.625: '#c9a0a8',
  0.750: '#ddc0c3',
  0.875: '#eee0e0',
  },
  '0304': {
  0.125: '#2e0000',
  0.250: '#5c0000',
  0.375: '#8b0000',
  0.500: '#b90f00',
  0.625: '#e74b00',
  0.750: '#ff8807',
  0.875: '#ffc485',
  },
  '0335': {
  0.125: '#04205a',
  0.250: '#10407f',
  0.375: '#24609c',
  0.500: '#4080b4',
  0.625: '#64a0c9',
  0.750: '#90c0dd',
  0.875: '#c4e0ee',
  },
  '1600': {
  0.125: '#3d3d04',
  0.250: '#5a5a10',
  0.375: '#747424',
  0.500: '#8d8d40',
  0.625: '#a8a864',
  0.750: '#c3c390',
  0.875: '#e0e0c4',
  },
  '1700': {
  0.125: '#5a2020',
  0.250: '#7f4040',
  0.375: '#9c6060',
  0.500: '#b48080',
  0.625: '#c9a0a0',
  0.750: '#ddc0c0',
  0.875: '#eee0e0',
  },
  '4500': {
  0.125: '#202000',
  0.250: '#404000',
  0.375: '#606000',
  0.500: '#808000',
  0.625: '#a0a000',
  0.750: '#c0c003',
  0.875: '#e0e042',
  },
  'HMIB': {
  0.125: '#222222',
  0.250: '#444444',
  0.375: '#666666',
  0.500: '#888888',
  0.625: '#AAAAAA',
  0.750: '#CCCCCC',
  0.875: '#EEEEEE',
  },
  'HMIBC': {
  0.125: '#222222',
  0.250: '#444444',
  0.375: '#666666',
  0.500: '#888888',
  0.625: '#AAAAAA',
  0.750: '#CCCCCC',
  0.875: '#EEEEEE',
  },
  'HMII': {
  0.125: '#222222',
  0.250: '#444444',
  0.375: '#666666',
  0.500: '#888888',
  0.625: '#AAAAAA',
  0.750: '#CCCCCC',
  0.875: '#EEEEEE',
  },
  'HMIIC': {
  0.125: '#222222',
  0.250: '#444444',
  0.375: '#666666',
  0.500: '#888888',
  0.625: '#AAAAAA',
  0.750: '#CCCCCC',
  0.875: '#EEEEEE',
  }    
}

Date.prototype.addDays = function(days) {
    var date = new Date(this.valueOf());
    date.setDate(date.getDate() + days);
    return date;
}

function plot_element_sdomovie(videodiv) {  
  var axes, ytransform, plotgroup, swdata, subplot
  var vid_els = {};
  var vid_src_els = {};
  var plotelement = {};

  sdo_sets.forEach(function(sdo_set, index) {
    videodiv_el = d3.select(videodiv);
    vid_wrapper = videodiv_el.insert("div")
      .attr("id","SDO_video_container_" + sdo_set)
      .classed("vid_wrapper", true);

    vid_els[sdo_set] = vid_wrapper.insert("video")
      .attr("id","SDO_video_" + sdo_set)
      .attr("width", "255px")
      .on("dblclick", function(d) {
        this.webkitRequestFullScreen();
      });

    vid_wrapper.append("div")
      .classed("video_overlay_text_container", true)
      .html("<h1>" + titles[sdo_set] + "</h1>");

    vid_els[sdo_set].attr("height", vid_els[sdo_set].node().clientWidth);

    vid_src_els[sdo_set] = vid_els[sdo_set].append("source")
      .attr("id","myVideoMP4source_" + sdo_set);
  });

  function videoURL(t, sdo_set) {
    t_iso = t.toISOString();
    dateslashed = t_iso.slice(0,10).replace(/-/g,'/');
    dateshort = t_iso.slice(0,10).replace(/-/g,'');
    url = "/sdo/img/dailymov/" + dateslashed + "/" + dateshort + "_1024_"+ sdo_set +".mp4"; 
    console.log("VideoURL", url)
    return url;
    // return "https://sdo.gsfc.nasa.gov/assets/img/dailymov/" + dateslashed + "/" + dateshort + "_1024_"+sdo_set+".mp4"; 
  }

  function getDates(startDate, stopDate) {
      var dateArray = new Array();
      var currentDate = startDate;
      while (currentDate <= stopDate) {
          endDate = currentDate.addDays(1)
          datum = {'tstart': currentDate,
                   'tend': endDate,
                   'status': 'unloaded',
                   'videoblobs': {} };
          dateArray.push(datum);
          currentDate = endDate;
      }
      return dateArray;
  }

  plotelement.subplot = function(value) {
    if (!arguments.length) return subplot;
    subplot = value;
    ytransform = subplot.ytransform();
    plotgroup = subplot.plotgroup();
    return plotelement;
  }

  plotelement.loadData = function() {
    t0_sdo = new Date(2010, 5, 19);
    t1_sdo = new Date()-86400e3;
    t0 = new Date(Math.max(t0_sdo, globalx.xtransform_z.domain()[0].getTime() - 86400)); // include 1 day previously to avoid empty first bar
    t1 = new Date(Math.min(t1_sdo, globalx.xtransform_z.domain()[1].getTime()));

    t0.setUTCHours(0,0,0,0); // Remove the time
    dateArray = getDates(t0, t1);
    swdata = dateArray;

    plotelement.setup(swdata);
    plotelement.update();

    return plotelement;
  }

  plotelement.setup = function(swdata) {
    plotgroup.selectAll(".sdomovie_button")
        .remove();
    
    plotgroup.selectAll(".sdomovie_button")
      .data(swdata)
      .enter()
        .append("rect")
        .classed("sdomovie_button", true)
        .classed("unloaded", true)
        .attr("fill", "lightgray")
        .attr("stroke", "none")
        .attr("rx", 4)
        .attr("ry", 4)
        .attr("height", ytransform.range()[0])
        .attr("clip-path", "url(#" +  plotelement.subplot().id() + "_clip)")
        .style("pointer-events", "all")
        .on("click", handleVideoButtonClick);

    return plotelement;
  }

  plotelement.update = function() {

    var daywidth = globalx.xtransform_z(86400e3)-globalx.xtransform_z(0);

    plotgroup.selectAll(".sdomovie_button")
      .attr("x", function(d) { return globalx.xtransform_z(d.tstart);} )
      .attr("y", 0)
      .attr("width", daywidth);
    
    return plotelement;
  }

  function handleVideoButtonClick(d) {
  // State can be 
  // "not loaded" -> click will start loading
  // "loading" -> click will do nothing
  // "playing" -> click will pause
  // "paused" -> click will play
    el = d3.select(this);
    if (d.status == 'unloaded') {
      // el.style("fill", "#d7be93")
      el.style("stroke", 'gray');
      el.style("stroke-width", "2px");
      if (!d.videoblob) { loadVideos(d, el) } else { assignVideos(d, el) };
    } else if (d.status == 'playing') {
      sdo_sets.forEach( function(sdo_set, i) {
        vid_els[sdo_set].node().pause();        
      })
      d.status = 'paused';
    } else if (d.status == 'paused') {
      sdo_sets.forEach( function(sdo_set, i) {
        vid_els[sdo_set].node().play();
      });
      d.status = 'playing';
    }
  }

  function loadVideos(d, el) {
    
    sdo_sets.forEach(function(sdo_set, i) {
      var req = new XMLHttpRequest();
      var url = videoURL(d.tstart, sdo_set);
      console.log("loadVideos", url);
      req.open('GET', url, true);
      req.responseType = 'blob';

      req.onload = function() {
        // Onload is triggered even on 404
        // so we need to check the status code
        if (this.status === 200) {
          var videoBlob = this.response;
          var vid = URL.createObjectURL(videoBlob); // IE10+
          // Video is now downloaded
          // and we can set it as source on the video element
          d.videoblobs[sdo_set] = vid;
          console.log("Loaded video blob for " + sdo_set);
          assignVideos(d, el);
         }
      }
      req.onerror = function() {
         console.log("There was an error loading video URL " + video_url)
      }
      req.send();
    });
 
  }

  function assignVideos(d, el) {
    sdo_sets.forEach( function(sdo_set, i) {
      vid_src_els[sdo_set].node().setAttribute('src', d.videoblobs[sdo_set]);//videoURL(d.tstart, sdo_set)); 
      vid_els[sdo_set].node().load();
      // vid_els[sdo_set].node().play();      
    })
    d.status = 'paused';
    el.style("fill", "darkgray");

    // // Update the seek bar as the video plays
    // vid_els[sdo_set].node().addEventListener("timeupdate", function() {
    //   // Calculate the slider value
    //   var fracday = vid_el.currentTime / vid_el.duration;
    //   // console.log("Played: ", fracday);
    //   // curtime = new Date(d.tstart);
    //   // curtime.setUTCSeconds(fracday*86400);
    //   // console.log(value);
    //   // Update the slider value
    //   // seekBar.value = value;
    //   // el.style("opacity", value);
    // });
    // vid_els[sdo_set].node().addEventListener('progress', function() {
    //   var range = 0;
    //   var bf = this.buffered;
    //   var time = this.currentTime;

    //   while(!(bf.start(range) <= time && time <= bf.end(range))) {
    //       range += 1;
    //   }
    //   var loadStartPercentage = bf.start(range) / this.duration;
    //   var loadEndPercentage = bf.end(range) / this.duration;
    //   var loadPercentage = loadEndPercentage - loadStartPercentage;
    //   // console.log("Loaded: ", loadPercentage)
    // });
  }  

  plotelements.push(plotelement);
  return plotelement;
}