function plot_element_image_sequence()
{
	var controller = new AbortController();
	var signal = controller.signal;
	var fetch_in_progress = 0;
	var ytransform;
	var plotgroup;
	var subplot;
	var plotelement = {};
	var fill = "none";
	var swdata = [];
	var server = '';
	var dataset = '';
	var parameters = '';
	var time = 'time';
	var imagearea = '';
	var x_animate = 0;
	var x_animate_step = 4;
	var playButton;
	var stopButton;
	var prevButton;
	var nextButton;
	var x_animate_loop = true;
	var duration = 0;
	var width = 1;
	var barPadding = 0.1;
	var barwidth = 1;
	var baroffset = 0;
	var cursor_index = 0;
	var url_loaded = '';
	var dt = 0;
	var t0_view = 0;
	var t1_view = 0;
	var timer;

	plotelement.setupButtons = function()
	{
		playButton = d3.select("#play-button");
		playButton
			.on("click", function() {
				var button = d3.select(this);
				if (button.text() == "Pause")
				{
					clearInterval(timer);
					button.text("Play");
				}
				else
				{
					var nImagePlotElements = 0;
					function oncompletion()
					{
						nImagePlotElements--;
						if (nImagePlotElements === 0)
						{
							console.log('Loading of all image plot elements completed');
							timer = setInterval(step, 50);
							button.text("Pause");
						}
					}
					button.text("Loading...");
					for (var i = 0; i < plotelements.length; i++)
					{
						if (typeof plotelements[i].loadImagesInView === 'function')
						{
							nImagePlotElements++;
						}
					}
					for (var i = 0; i < plotelements.length; i++)
					{
						if (typeof plotelements[i].loadImagesInView === 'function')
						{
							plotelements[i].loadImagesInView(oncompletion);
						}
					}
				}
			})

		stopButton = d3.select("#stop-button");
		stopButton
			.on("click", function() {
				var button = d3.select(this);
				clearInterval(timer);
				x_animate = 0;
				d3.selectAll(".animationindicator")
					.attr("x1", x_animate)
					.attr("x2", x_animate);
				for (var i = 0; i < plotelements.length; i++)
				{
					if (typeof plotelements[i].reset === 'function')
					{
						plotelements[i].reset();
					}
				}
				playButton.text("Play");
			})

		prevButton = d3.select("#prev-button");
		prevButton
			.on("click", function() {
				dostep(-1);
			})

		nextButton = d3.select("#next-button");
		nextButton
			.on("click", function() {
				dostep(1);
			})
	}

	plotelement.reset = function()
	{
		cursor_index = 0;
		url_loaded = '';
	}

	plotelement.loadImagesInView = function(oncompletion)
	{
		var imagesToLoad = 0;

		function onload()
		{
			imagesToLoad--;
			if (imagesToLoad === 0)
			{
				oncompletion();
			}
		}
		if (swdata.length > 1)
		{
			var dt = swdata[1]['datetime'] - swdata[0]['datetime'];
			var t0_view_extended = new Date(t0_view.getTime() - dt);
			for (var i = 0; i < swdata.length; i++)
			{
				var date = swdata[i]['datetime'];
				if (date >= t0_view_extended && date <= t1_view)
				{
					if (swdata[i]['url_low_res'] !== undefined)
					{
						imagesToLoad++;
					}
				}
			}
		}
		for (var i = 0; i < swdata.length; i++)
		{
			var date = swdata[i]['datetime'];
			if (date >= t0_view_extended && date <= t1_view)
			{
				if (swdata[i]['url_low_res'] !== undefined)
				{
					var url = swdata[i]['url_prefix'] + '/' + swdata[i]['url_low_res'];
					var img = document.createElement('img');
					img.src = url;
					img.onload = onload;
					img.onerror = onload;
					swdata[i]['img'] = img;
				}
			}
		}
	}

	plotelement.updateImage = function(x_animate)
	{
		var date_cursor = globalx.xtransform_z.invert(x_animate);
		var start = cursor_index - 1;
		if (start < 0) start = 0;
		for (var i = start; i < swdata.length; i++)
		{
			if (swdata[i]['datetime'] >= date_cursor)
			{
				cursor_index = i;
				break;
			}
		}
		if (cursor_index < 1) return;
		data_index = cursor_index - 1;
		if (cursor_index == swdata.length - 1)
		{
			if (date_cursor >= swdata[swdata.length - 1]['datetime'])
			{
				data_index = swdata.length - 1;
			}
		}
		var url = swdata[data_index]['url_prefix'] + '/' + swdata[data_index]['url_low_res'];
		if (url != url_loaded)
		{
			url_loaded = url;
			var url_high_res = swdata[data_index]['url_prefix'] + '/' + swdata[data_index]['url_high_res'];
			if (imagearea != '')
			{
				const node = document.getElementById(imagearea);
				while (node && node.firstChild)
				{
					node.removeChild(node.lastChild);
				}
				if (typeof swdata[data_index]['img'] != 'undefined')
				{
					swdata[data_index]['img'].style.width = "384px"; //node.style.width;
					node.appendChild(swdata[data_index]['img']);
					node.onclick = function() {
						window.open(url_high_res);
					}
				}
			}
		}
	}

	function step()
	{
		dostep(1);
	}

	function dostep(direction)
	{
		x_animate = x_animate + x_animate_step * direction;
		var now = new Date();
		var date_cursor = globalx.xtransform_z.invert(x_animate);
		if (x_animate < 0 || x_animate > globalx.width || date_cursor > now)
		{
			for (var i = 0; i < plotelements.length; i++)
			{
				if (typeof plotelements[i].reset === 'function')
				{
					plotelements[i].reset();
				}
			}
			x_animate = 0;
			if (!x_animate_loop)
			{
				clearInterval(timer);
				playButton.text("Play");
			}
			return;
		}

		d3.selectAll(".animationindicator")
			.attr("x1", x_animate)
			.attr("x2", x_animate);

		for (var i = 0; i < plotelements.length; i++)
		{
			if (typeof plotelements[i].updateImage === 'function')
			{
				plotelements[i].updateImage(x_animate);
			}
		}
	}

	plotelement.loadData = function()
	{
		function formatDate(date)
		{
			return date.toISOString().slice(0, 19);
		}

		function transformData(d)
		{
			dd = {};
			dd['datetime'] = new Date(d['time']);
			if (d['url_prefix'] == '')
			{
				dd['marker'] = 0;
			}
			else
			{
				dd['url_prefix'] = d['url_prefix'];
				dd['url_low_res'] = d['url_low_res'];
				dd['url_high_res'] = d['url_high_res'];
				dd['marker'] = 3;
//				dd['duration'] = 2*60*1000
			}
			return dd;
		}

		for (var i = 0; i < plotelements.length; i++)
		{
			if (typeof plotelements[i].reset === 'function')
			{
				plotelements[i].reset();
			}
		}
		x_animate = 0;

		// use a larger time window to make panning more smooth
		t0_view = new Date(globalx.xtransform_z.domain()[0].getTime());
		t1_view = new Date(globalx.xtransform_z.domain()[1].getTime());
		dt = t1_view.getTime() - t0_view.getTime();
		t0_load = new Date(t0_view.getTime() - dt);
		t1_load = new Date(t1_view.getTime() + dt);

		var zoom_level_resolution = 60;
		var min_zoom_level = 0;
		var min_pixels = 10;

		var x = Math.max(1.0, min_pixels / (globalx.xtransform_z(zoom_level_resolution*1000) - globalx.xtransform_z(0)));
		var y = Math.max(min_zoom_level, Math.ceil(Math.log2(x)));
		var z = zoom_level_resolution * (1.0 << y);

		var subsample_time = '' + z + 'S';

		if (fetch_in_progress > 0)
		{
			controller.abort();
			  controller = new AbortController();
			signal = controller.signal;
		}

		var url = server;
		url += '/data?id=' + dataset;
		url += '&parameters=' + time + ',' + parameters;
		url += '&time.min=' + formatDate(t0_load) + 'Z';
		url += '&time.max=' + formatDate(t1_load) + 'Z';

		if (server.includes('hapi-r'))
		{
			url += '&resample=' + subsample_time;
		}
		fetch_in_progress++;
		fetch(url, { signal })
			.then((resp) => resp.text())
			.then(function(csvdata) {
				fetch_in_progress--;
				csvdata = 'time,' + parameters + '\n' + csvdata;
				data = d3.csvParse(csvdata, transformData);
				for (var i = 0; i < data.length -1; i++) {
					data[i]['duration'] = data[i+1]['datetime'] - data[i]['datetime']
				}
				// data[data.length]['duration'] = 5*60*1000;

				swdata = data;
				  plotelement.setup();
				plotelement.update();
			})
			.catch(function(error) {
				fetch_in_progress--;
				console.log(error);
			});
	}

	plotelement.subplot = function(value)
	{
		if (!arguments.length) return subplot;
		subplot = value;
		ytransform = subplot.ytransform();
		plotgroup = subplot.plotgroup();
		return plotelement;
	}

	plotelement.server = function(value)
	{
		if (!arguments.length) return server;
		server = value;
		return plotelement;
	}

	plotelement.dataset = function(value)
	{
		if (!arguments.length) return dataset;
		dataset = value;
		return plotelement;
	}

	plotelement.parameters = function(value)
	{
		if (!arguments.length) return parameters;
		parameters = value;
		return plotelement;
	}

	plotelement.time = function(value)
	{
		if (!arguments.length) return time;
		time = value;
		return plotelement;
	}

	plotelement.fill = function(value)
	{
		if (!arguments.length) return fill;
		fill = value;
		return plotelement;
	}

	plotelement.imagearea = function(value)
	{
		if (!arguments.length) return fill;
		imagearea = value;
		return plotelement;
	}

	plotelement.setup = function()
	{
		plotgroup.selectAll(".imgtl")
			.remove();

		plotgroup.selectAll(".imgtl")
			.data(swdata)
			.enter()
			.append("rect")
			.attr("class", "imgtl")
			.style("fill", fill)
			.attr("height", function(d) {  return ytransform.range()[0] - ytransform(d.marker); })
			.attr("clip-path", "url(#" +  plotelement.subplot().subplotid() + "_clip)");

		return plotelement;
	}

	plotelement.update = function(data)
	{
		if (swdata.length > 1) {
			duration = swdata[1]['datetime'] - swdata[0]['datetime'];
			width = globalx.xtransform_z(duration) - globalx.xtransform_z(0);
			barwidth = width * (1 - barPadding);
			baroffset = width * barPadding / 2;

			plotgroup.selectAll(".imgtl")
				.attr("x", function(d) { return globalx.xtransform_z(d.datetime) + (globalx.xtransform_z(d.duration) - globalx.xtransform_z(0)) * barPadding / 2; })
				.attr("y", function(d) { return ytransform(d.marker); })
				.attr("width", function(d) {return (globalx.xtransform_z(d.duration) - globalx.xtransform_z(0)) * (1 - barPadding)})
//				.attr("width", barwidth);
		}
		return plotelement;
	}

	plotelements.push(plotelement);
	return plotelement;
}
