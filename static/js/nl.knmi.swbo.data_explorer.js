var globalx;
var plotlayout = {};
var subplots = [];
var plotelements = [];
var source = [];
var hapi_tree = {};

function dataExplorerClear()
{
    subplots = [];
    plotelements = [];
    d3.selectAll("svg > *").remove();
    const myNode = document.getElementById("chartcontainer");
    while (myNode.firstChild)
    {
        myNode.removeChild(myNode.lastChild);
    }
}

function dataExplorer()
{
    source = [
        {title: "/hapi-r", type: "server", folder: true, children: []},
        {title: "https://cdaweb.gsfc.nasa.gov/hapi", type: "server", folder: true, children: []}
    ];
    
    $("#tree").fancytree({
        source: source,
        click: function(event, data){
            var node = data.node;
            if (node.type == "server")
            {
                document.getElementById("details").innerHTML = "<b>server:</b> " + node.title;
                if (node.children != null && node.children.length != 0) return;
                var url = node.title + "/catalog";
                fetch(url)
                    .then((resp) => resp.json())
                    .then(function(data) {
                        node.data = data;
                        for (i = 0; i < data.catalog.length; i++)
                        {
                            node.addChildren({
                                title: data.catalog[i].id,
                                type: "dataset",
                                folder: true,
                                children: []
                            });
                            node.sortChildren();
                        }
                        node.setExpanded(true);
                    })
                    .catch(function(error) {
                        console.log(error);
                    });
            }
            else if (node.type == "dataset")
            {
                if (node.children != null && node.children.length != 0)
                {
                    document.getElementById("details").innerHTML = 
                        "<b>server:</b> " + node.parent.title + "<br><br>" +
                        "<b>dataset id:</b> " + node.title + "<br>" +
                        "<b>description:</b> " + node.data.description + "<br>" +
                        "<b>resource url:</b> <a href=\"" + node.data.resourceURL + "\" target=\"_blank\" >" + node.data.resourceURL + "</a><br>" +
                        "<b>start date:</b> " + node.data.startDate + "<br>" +
                        "<b>stop date:</b> " + node.data.stopDate + "<br>";
                    return;
                }
                var url = node.parent.title + "/info?id=" + node.title;
                fetch(url)
                    .then((resp) => resp.json())
                    .then(function(data) {
                        node.data = data;
                        for (i = 0; i < data.parameters.length; i++)
                        {
                            if (data.parameters[i].name.toLowerCase() != 'time')
                            {
                                node.addChildren({
                                    title: data.parameters[i].name,
                                    type: "parameter"
                                });
                            }
                        }
                        node.setExpanded(true);
                        document.getElementById("details").innerHTML = 
                            "<b>server:</b> " + node.parent.title + "<br><br>" +
                            "<b>dataset id:</b> " + node.title + "<br>" +
                            "<b>description:</b> " + node.data.description + "<br>" +
                            "<b>resource url:</b> <a href=\"" + node.data.resourceURL + "\" target=\"_blank\" >" + node.data.resourceURL + "</a><br>" +
                            "<b>start date:</b> " + node.data.startDate + "<br>" +
                            "<b>stop date:</b> " + node.data.stopDate + "<br>";
                    })
                    .catch(function(error) {
                        console.log(error);
                    });
            }
            else if (node.type == "parameter")
            {
                var iparameter = 0;
                var itime = 0;
                parameters = node.parent.data.parameters;
                for (i = 0; i < parameters.length; i++)
                {
                    if (parameters[i].name == node.title)
                    {
                        iparameter = i;
                    }
                    if (parameters[i].name.toLowerCase() == 'time')
                    {
                        itime = i;
                    }
                }
                document.getElementById("details").innerHTML = 
                    "<b>server:</b> " + node.parent.parent.title + "<br><br>" +
                    "<b>dataset id:</b> " + node.parent.title + "<br>" +
                    "<b>description:</b> " + node.parent.data.description + "<br>" +
                    "<b>resource url:</b> <a href=\"" + node.parent.data.resourceURL + "\" target=\"_blank\" >" + node.parent.data.resourceURL + "</a><br>" +
                    "<b>start date:</b> " + node.parent.data.startDate + "<br>" +
                    "<b>stop date:</b> " + node.parent.data.stopDate + "<br><br>" +
                    "<b>parameter name:</b> " + parameters[iparameter].name + "<br>" +
                    "<b>description:</b> " + parameters[iparameter].description + "<br>" +
                    "<b>units:</b> " + parameters[iparameter].units + "<br>";

                if (node.parent.data.stopDate != '')
                {
                    tmax = new Date(node.parent.data.stopDate);
                }
                else
                {
                    tmax  = new Date();
                }
                tmin = new Date(tmax.getTime() - 4*24*3600*1000);

                plotlayout = {
                    "tmin": formatDate(tmin) + 'Z',
                    "tmax": formatDate(tmax) + 'Z',
                    "subplots": [
                        {
                            "title": parameters[iparameter].description,
                            "ymin": 0,
                            "ymax": 100,
                            "elements": [
                                {
                                    "server": node.parent.parent.title,
                                    "dataset": node.parent.title,
                                    "parameters":  parameters[iparameter].name,
                                    "time": parameters[itime].name,
                                    "stroke": "darkred",
                                    "fill": "lightcoral",
                                    "fill_opacity": "100%",
                                    "mirror_area": false
                                }
                            ]
                        }
                    ]
                };
                dataExplorerPlot(plotlayout);
            }
        }
    });
}

function dataExplorerPlot(newplotlayout)
{
    plotlayout = newplotlayout;

    globalx = {
        margins: {left: 60, right: 60},
        get width() {
            return document.getElementById("chartcontainer").clientWidth - this.margins.left - this.margins.right
        },
    };

    var tmin1 = getTime(plotlayout['tmin']);
    var tmax1 = getTime(plotlayout['tmax']);

    globalx.xtransform = d3.scaleUtc().range([0, globalx.width]).domain([tmin1, tmax1]);
    globalx.xtransform_z = d3.scaleUtc().range([0, globalx.width]).domain([tmin1, tmax1]);

    var innerHeight = window.innerHeight;  

    dataExplorerClear();

    var subplots = plotlayout['subplots'];
    for (i = 0; i < subplots.length; i++)
    {
        subplot = new subplot_generator("chartcontainer")
            .title(subplots[i]['title'])
            .id("kp3")
            .ymin(subplots[i]['ymin'])
            .ymax(subplots[i]['ymax'])
            .height(0.3 * innerHeight - 10)
            .margins({top: 30, bottom: 30})    
            .xaxisannotation('visible')
            .ytickformat('.3g')
            .draw()
            .updateDateTimeAxis();

        var elements = subplots[i]['elements'];
        for (j = 0; j < elements.length; j++)
        {
            element = new plot_element_timeseries()
                .subplot(subplot)
                .server(elements[j]['server'])
                .dataset(elements[j]['dataset'])
                .parameters(elements[j]['parameters'])
                .time(elements[j]['time'])
                .stroke(elements[j]['stroke'])
                .fill(elements[j]['fill'])
                .fill_opacity(elements[j]['fill_opacity'])
                .mirror_area(elements[j]['mirror_area']);

            element
                .loadData();
        }
    }
}

function formatDate(date)
{
    return date.toISOString().slice(0, 19);
}

