function plot_element_kp()
{
  var axes, ytransform, plotgroup, kpdata, subplot;
  var plotelement = {};
  var kpduration;
  var barPadding = 0.2;
  var showkptext = true;
  var subsample_time = "";
  var firstrun = true;
  var server;
  var dataset;
  var parameters;
  var swdata = [];
  var groupid;
  var barclass;
  var bartextclass
  var plotgroup;
  var plotsubgroup
  var plotelement = {}
  var time = 'time';

  plotelement.subplot = function(value)
  {
      if (!arguments.length) return subplot;
      subplot = value;
      ytransform = subplot.ytransform();
      plotgroup = subplot.plotgroup();
      return plotelement;
  }

  plotelement.server = function(value)
  {
      if (!arguments.length) return server;
      server = value;
      return plotelement;
  }

  plotelement.dataset = function(value)
  {
      if (!arguments.length) return dataset;
      dataset = value;
      return plotelement;
  }

  plotelement.time = function(value)
  {
      if (!arguments.length) return time;
      time = value;
      return plotelement;
  }

  plotelement.parameters = function(value)
  {
      if (!arguments.length) return parameters;
      parameters = value;
      return plotelement;
  }

  plotelement.showkptext = function(value)
  {
      if (!arguments.length) return showkptext;
      showkptext = value;
      return plotelement;
  }

  plotelement.barpadding = function(value)
  {
      if (!arguments.length) return barPadding;
      barPadding = value;
      return plotelement;
  }

  function kpcolor(kp)
  {
      if (kp < 4.8) {return '#88CC66';} // green
      if (kp < 5.8) {return '#FFF033';} // yellow
      if (kp < 6.8) {return '#FFBB44';} // light orange
      if (kp < 7.8) {return '#DD9922';} // dark orange
      if (kp < 8.8) {return '#FF2020';} // red
      if (kp < 9.8) {return '#A02020';} // dark red
  }

  function kptext(kp)
  {
      kpint = Math.trunc(kp)
      kpdif = kp-kpint;
      if (kpdif < 0.1)
      {
          return kpint.toFixed(0);
      }
      if (kpdif > 0.2 && kpdif < 0.4)
      {
          return kpint.toFixed(0) + '+';
      }
      if (kpdif > 0.6 && kpdif < 0.9)
      {
          return (kpint+1).toFixed(0) + '-';
      }
  }

  function kpwidth(kpduration)
  {
      // Returns the width of the Kp interval in pixels
      return globalx.xtransform_z(kpduration)-globalx.xtransform_z(0);
  }

  plotelement.loadData = function()
  {
      function formatDate(date)
      {
          return date.toISOString().slice(0, 19) + 'Z';
      }

      function transformData(d)
      {
          d.time = parseDate(d.time);
          //d.kp_index = +d.kp_index;
          //if (timeseries == 'kp_index_forecast_metoffice') d.kp_index *= 3;
          return d;
      }

      // use a larger time window to make panning more smooth
      t0_view = new Date(globalx.xtransform_z.domain()[0].getTime());
      t1_view = new Date(globalx.xtransform_z.domain()[1].getTime());
      dt = t1_view.getTime() - t0_view.getTime();
      t0_load = new Date(t0_view.getTime() - dt);
      t1_load = new Date(t1_view.getTime() + dt);

      const zoom_level_resolution = 3*60*60;
      const min_zoom_level = 0;
      const min_pixels = 3;

      // x = Math.max(1.0, min_pixels / (globalx.xtransform_z(zoom_level_resolution*1000) - globalx.xtransform_z(0)));
      // y = Math.max(min_zoom_level, Math.ceil(Math.log2(x)));
      // z = zoom_level_resolution * (1.0 << y);

      // subsample_time = '' + z + 'S';

      function datawidth(xtransform, duration) {
        return xtransform(duration)-xtransform(0);
      }

      t0 = new Date(globalx.xtransform_z.domain()[0].getTime()); // - 180 * 1e3); // include 3 mins previously to avoid empty first bar
      t1 = new Date(globalx.xtransform_z.domain()[1].getTime()); // + 180 * 1e3);

      minpixels = 3;
      if (kpwidth(30*60*1e3) > minpixels) {
        subsample_time = '30M'; // default is 30 minutes
      } else if (kpwidth(3*3600*1e3) > minpixels) {
        subsample_time = '3H'; // default is 3 hours
      } else if (kpwidth(86400*1e3) > minpixels) {
        subsample_time = '1D'; // day
      } else if (kpwidth(7*86400*1e3) > minpixels) {
        subsample_time = '7D'; // day
      } else if (kpwidth(31*86400*1e3) > minpixels) {
        subsample_time = '31D'; // week
      } else {
        subsample_time = '365D'; // year
      }
      //var dataset = timeseries;

      var url = server;
      url += '/data?id=' + dataset;
      url += '&parameters=' + time + ',' + parameters;
      url += '&time.min=' + formatDate(t0_load);// + 'Z';
      url += '&time.max=' + formatDate(t1_load);// + 'Z';
      url += '&include=brief_header'

      if (server.includes('hapi-r'))
      {
          url += '&resample=' + subsample_time;
      }

      d3.csv(url, transformData, function(error, data) {
          if (error) throw error;
          if (data.length > 1) {
            swdata = data;
            plotelement.update();
          }
      });
  }

  plotelement.setup = function(kpdata) {
    // Set group and classes
    groupid   = dataset  + "_" + parameters + "_group";
    barclass = dataset + "_" + parameters + "_bar";
    bartextclass = dataset + "_" + parameters + "_bartext";

    // Set bar geometry parameters
    if (swdata.length >= 1) {
      kpduration = swdata[1]['time'] - swdata[0]['time'];
    } else {
      kpduration = 3*60*1000;
    }
    kp_width = globalx.xtransform_z(kpduration)-globalx.xtransform_z(0);
    var barwidth = kp_width * (1-barPadding);
    var baroffset = kp_width * barPadding/2;

    plotsubgroup = plotgroup.append("g")
        .attr("id", groupid);

    plotsubgroup.selectAll("." + barclass).data(swdata).enter()
      .append("rect")
      .attr("class", barclass)
      .attr("clip-path", "url(#" +  plotelement.subplot().subplotid() + "_clip)")
      .attr("fill", function(d) { return kpcolor(d[parameters]); } )
      .attr("x", function(d) { return globalx.xtransform_z(d.time)+baroffset;} )
      .attr("y", function(d) { return ytransform(d[parameters]); })
      .attr("width", barwidth)
      .attr("height", function(d) { return ytransform.range()[0]-ytransform(d[parameters]); } );

      // if (showkptext) {
      //   plotgroup.selectAll("." + bartextclass).data(swdata).enter()
      //     .append("text")
      //       .attr("class", bartextclass)
      //       .attr("clip-path", "url(#" +  plotelement.subplot().subplotid() + "_clip)")
      //       .attr("fill", "black")
      //       .text(function(d) {return kptext(d[parameters])})
      //       .style("text-anchor", "middle")
      //       .style("font-family", "sans-serif")
      //       .style("font-size", "11px");
      // } else {
      //   plotgroup.selectAll("." + bartextclass)
      //     .remove();
      // }

      return plotelement;
  }

  plotelement.update = function() {
    if (firstrun) {
      plotelement.setup();
      firstrun = false;
    }

    // Set bar geometry parameters
    if (swdata.length >= 1) {
      kpduration = swdata[1]['time'] - swdata[0]['time'];
    } else {
      kpduration = 3*60*1000;
    }

    kp_width = globalx.xtransform_z(kpduration)-globalx.xtransform_z(0);
    var barwidth = kp_width * (1-barPadding);
    var baroffset = kp_width * barPadding/2;

    sel = plotgroup.selectAll("." + barclass).data(swdata)
      .attr("x", function(d) { return globalx.xtransform_z(d.time)+baroffset;} )
      .attr("y", function(d) { return ytransform(d[parameters]); })
      .attr("width", barwidth)
      .attr("height", function(d) { return ytransform.range()[0]-ytransform(d[parameters]); } )
      .attr("fill", function(d) { return kpcolor(d[parameters]); } );

    sel.enter()
      .append("rect")
      .attr("class", barclass)
      .attr("clip-path", "url(#" +  plotelement.subplot().subplotid() + "_clip)")
      .attr("x", function(d) { return globalx.xtransform_z(d.time)+baroffset;} )
      .attr("y", function(d) { return ytransform(d[parameters]); })
      .attr("width", barwidth)
      .attr("height", function(d) { return ytransform.range()[0]-ytransform(d[parameters]); } )
      .attr("fill", function(d) { return kpcolor(d[parameters]); } );

    sel.exit().remove();

    if (showkptext && (barwidth > 25)) {
      sel = plotgroup.selectAll("." + bartextclass).data(swdata)
      .text(function(d) {return kptext(d[parameters])})
      .attr("clip-path", "url(#" +  plotelement.subplot().subplotid() + "_clip)")
      .style("text-anchor", "middle")
      .style("font-family", "sans-serif")
      .style("font-size", "11px")
      .attr("x", function(d) { return globalx.xtransform_z(d.time)+barwidth/2+baroffset;} )
      .attr("y", function(d) {
        if (d[parameters] <= 7) {
         return ytransform(d[parameters]) - 4;
        } else {
         return ytransform(d[parameters]) + 15;
        }
        })
      .style("fill", function(d) {
        if (d[parameters] <= 7) {
          return "black";
        } else {
          return "white";
        }
      });

      sel.enter()
      .append("text")
      .attr("class", bartextclass)
      .attr("clip-path", "url(#" +  plotelement.subplot().subplotid() + "_clip)")
      .text(function(d) {return kptext(d[parameters])})
      .style("text-anchor", "middle")
      .style("font-family", "sans-serif")
      .style("font-size", "11px")
      .attr("x", function(d) { return globalx.xtransform_z(d.time)+barwidth/2+baroffset;} )
      .attr("y", function(d) {
        if (d[parameters] <= 7) {
         return ytransform(d[parameters]) - 4;
        } else {
         return ytransform(d[parameters]) + 15;
        }
        })
      .style("fill", function(d) {
        if (d[parameters] <= 7) {
          return "black";
        } else {
          return "white";
        }
      });

      sel.exit().remove();
    } else {
      plotgroup.selectAll("." + bartextclass).remove();
    }

    return plotelement;
  }

  plotelements.push(plotelement);
  return plotelement;
}
