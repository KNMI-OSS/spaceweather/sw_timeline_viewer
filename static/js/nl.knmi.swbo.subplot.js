function subplot_generator(elid) {
    // subplot object -> attaches to a div, will contain a row
    // uses global xaxis object,
    // Draws axes (and title)
    // on zoom, it should update the plot elements, such as timeseries lines, attached to it
    var chartDiv = document.getElementById(elid);

    // Default title, can be overwritten
    var title = "Title";
    var subplotid = "id_not_set";

    // Default y-axis limits and dimensions
    var ymin = -1000;
    var ymax = 1000;
    var margins = {top: 0, bottom: 0};
    var height = 50;
    var ytransform = d3.scaleLinear().range([height, 0]).domain([ymin,ymax]);
    var yAxis = d3.axisLeft(ytransform); // Generate SVG code for y-axis
    var ydynamic = true;

    // Default annotation
    var xaxisannotation = "visible";
    var ytickformat = "";

    // The subplot also provides a vertical bar at the current time (now)
    var now = new Date();

    // X-axes: There are primary and secondary axes, so that separate ticks can be shown for, for example, months and days
    var xAxis = d3.axisBottom(globalx.xtransform)
          .tickSize(6)
          .tickPadding(3);
    var xAxis2 = d3.axisBottom(globalx.xtransform)
          .tickSize(18)
          .tickPadding(3);
    // xDayTicks are the thin vertical bars at day boundaries
    var xDayTicks = d3.axisBottom(globalx.xtransform).ticks(d3.utcDay).tickSize(height); // Generate SVG code for x-axis

    // Set up vars for zooming and panning
    var curzoom_seconds = (globalx.xtransform.domain()[1] - globalx.xtransform.domain()[0])/1000
    var minzoom = curzoom_seconds/(150*365.25*86400) // 150 years - perhaps implement overriding these zoom boundaries from the json layout config
    var maxzoom = curzoom_seconds/(5*60) // 5 minutes
    var zoomable = true;
    var zoom = d3.zoom()
      .on("zoom", perform_zoom)
      .scaleExtent([minzoom, maxzoom]);

    var animation = false;

    var foregroundaxes; // The foregroundaxes will be an SVG group that overlays the actual data, so that the data does not hide the tickmarks, etc.
    var plotgroup;      // Plotgroup will be an SVG group to which the actual data will be added.
    var mouseindicator; // The mouseindicator will show thin vertical lines at the location (time) of the mouse cursor.
    var zoomrect;       // The zoomrect will be an invisible SVG element on which d3.js detects zoom action from the mouse or trackpad.

    // Create the subplot object and getter/setters for the properties
    subplot = {};

    subplot.elid = function() {
      return elid;
    }


    subplot.subplotid = function(value) {
      if (!arguments.length) return subplotid;
      subplotid = value;
      return subplot;
    }

    subplot.title = function(value) {
      if (!arguments.length) return title;
      title = value;
      return subplot;
    }

    subplot.id = function(value) {
      if (!arguments.length) return id;
      id = value;
      return subplot;
    }

    subplot.zoomable= function(value) {
      if (!arguments.length) return zoomable;
      zoomable = value;
      return subplot;
    }

    subplot.xaxisannotation = function(value) {
      if (!arguments.length) return xaxisannotation;
      xaxisannotation = value;
      return subplot;
    }

    subplot.animation = function(value) {
      if (!arguments.length) return animation;
      animation = value;
      return subplot;
    }

    subplot.height = function(value) {
      if (!arguments.length) return height;
      height = value;
      ytransform
        .range([height, 0]).domain([ymin,ymax]);
      yAxis = d3.axisLeft(ytransform).ticks(4).tickFormat(ytickformat);
      return subplot;
    }

    subplot.margins = function(value) {
      if (!arguments.length)
      {
        return margins;
      }
      margins = value;
      return subplot;
    }

    subplot.ytransform = function(value) {
      if (!arguments.length) return ytransform;
      ytransform = value;
      return subplot;
    }

    subplot.ytickformat = function(value) {
      if (!arguments.length) return ytickformat;
      ytickformat = value;
      yAxis.tickFormat(d3.format(ytickformat));
      return subplot;
    }

    subplot.ymin = function(value) {
      if (!arguments.length) return ymin;
      ymin = value;
      ytransform.domain([ymin, ymax]);
      return subplot;
    }

    subplot.ymax = function(value) {
      if (!arguments.length) return ymax;
      ymax = value;
      ytransform.domain([ymin, ymax]);
      return subplot;
    }

    subplot.ydynamic = function(value)
    {
        if (!arguments.length) return ydynamic;
        ydynamic = value;
        return subplot;
    }

    subplot.plotgroup = function(value) {
      if (!arguments.length) return plotgroup;
      plotgroup = value;
      return subplot;
    }

    // Code to render the SVG of the subplot
    subplot.draw = function() {
        var svg = d3.select(chartDiv).append("svg")
            .attr("width", globalx.width + globalx.margins.left + globalx.margins.right)
            .attr("height", height + margins.top + margins.bottom);

        defs = svg.append("defs")

        clip = defs.append("clipPath")
            .attr("id", subplot.subplotid() + "_clip")
            .append("rect")
            .attr("width", globalx.width)
            .attr("height", height);

        plotgroup = svg.append("g")
            .attr("class", "plotgroup")
            .attr("transform", "translate(" + globalx.margins.left + "," + margins.top + ")");

        foregroundaxes = svg.append("g")
              .attr("id", "foregroundaxesid")
              .attr("class", "foregroundaxes")
              .attr("transform", "translate(" + globalx.margins.left + "," + margins.top + ")");

        var nowindicator = foregroundaxes.append("line")
            .attr("class","nowindicator")
            .attr("clip-path", "url(#"+subplot.subplotid() +"_clip)")
            .attr("x1", globalx.xtransform(now))
            .attr("x2", globalx.xtransform(now))
            .attr("y1", ytransform(ymin))
            .attr("y2", ytransform(ymax));

        mouseindicator = foregroundaxes.append("line")
            .attr("class","mouseindicator")
            .attr("clip-path", "url(#"+subplot.subplotid() +"_clip)")
            .attr("x1", globalx.xtransform(now))
            .attr("x2", globalx.xtransform(now))
            .attr("y1", height)//ytransform(ymin))
            .attr("y2", 0);//ytransform(ymax));

        animationindicator = foregroundaxes.append("line")
            .attr("class","animationindicator")
            .attr("clip-path", "url(#"+subplot.subplotid() +"_clip)")
            .attr("x1", 0)
            .attr("x2", 0)
            .attr("y1", height)
            .attr("y2", 0);

        // setInterval(function() {
        //   var now = new Date()
        //   nowindicator.attr("x1", xtransform(now)).attr("x2", xtransform(now));
        // }, 1000)

        foregroundaxes.append("g")
            .attr("class", "axis dayticks")
            .call(xDayTicks)
            .selectAll(".tick text")
                .style("visibility", "hidden");

        foregroundaxes.append("g")
            .attr("class", "axis axis--x")
            .attr("transform", "translate(0," + height + ")")
            .call(xAxis);

        foregroundaxes.append("g")
            .attr("class", "axis axis--x2")
            .attr("transform", "translate(0," + height + ")")
            .call(xAxis2);

        foregroundaxes.append("g")
            .attr("class", "axis axis--y")
            .call(yAxis);

        foregroundaxes.append("text")
            .attr("class", "title")
            .attr("x", 8)
            .attr("y", 11)
            .text(title)

        //foregroundaxes.select(".axis--y").call(yAxis)

        if (subplotid == 'xrayflux') {
          foregroundaxes.append('line')
             .attr("x1", 0)
             .attr("y1", ytransform(1e-5))
             .attr("x2", globalx.width)
             .attr("y2", ytransform(1e-5))
             .attr("stroke-width", "0.5px")
             .attr("stroke-opacity", "50%");
          foregroundaxes.append('line')
           .attr("x1", 0)
           .attr("y1", ytransform(1e-4))
           .attr("x2", globalx.width)
           .attr("y2", ytransform(1e-4))
           .attr("stroke-width", "0.5px")
           .attr("stroke-opacity", "50%");
          foregroundaxes.append('line')
           .attr("x1", 0)
           .attr("y1", ytransform(1e-3))
           .attr("x2", globalx.width)
           .attr("y2", ytransform(1e-3))
           .attr("stroke-width", "0.5px")
           .attr("stroke-opacity", "50%");

          annot_font_height = (ytransform(1e-4) - ytransform(1e-3))*0.7 +"px"
          foregroundaxes.append('text')
           .attr("x", globalx.width - 7)
           .attr("y", ytransform(2e-5))
           .text("M")
           .attr("font-family", "sans-serif")
           .attr("font-size", annot_font_height)
           .attr("text-anchor", "middle")
          foregroundaxes.append('text')
           .attr("x", globalx.width - 7)
           .attr("y", ytransform(2e-4))
           .text("X")
           .attr("font-family", "sans-serif")
           .attr("font-size", annot_font_height)
           .attr("text-anchor", "middle")
        }


        if (zoomable) {
            zoomrect = svg.append("rect")
                .attr("class", "zoom")
                .attr("width", globalx.width)
                .attr("height", height)
                .attr("transform", "translate(" + globalx.margins.left + "," + margins.top + ")")
                .call(zoom)
                .on("mousemove", function() {
                    var newx = d3.mouse(this)[0];
                    t = globalx.xtransform_z.invert(newx);

                    subplots.forEach(
                        function(subplot, index) {
                            subplot.setmouseindicator(newx);
                        })
                    }
                )
                .on("mouseout", function() {
                    subplots.forEach(
                        function(subplot, index) {
                            subplot.hidemouseindicator();
                        })
                    }
                );
            }
        return subplot;
    }

    // The mouse indicator needs to be hidden when the mouse moves out of the zoomrect
    subplot.hidemouseindicator = function() {
        mouseindicator.classed("hidden", true);
    }

    subplot.setmouseindicator = function(newx) {
        mouseindicator.classed("hidden", false);
        mouseindicator.attr("x1", newx).attr("x2", newx);
    }


    subplot.setzoom = function(transform) {
        // Hack to get all the subplots to acquire the same zoom state
        // Otherwise, when zooming on different subplots, the zoom state jumps back to different original states.
        if (zoomable) { zoomrect.node().__zoom = transform; }
    }

    subplot.updateVerticalAxis = function() {
        foregroundaxes.select(".axis--y").call(yAxis);
        return subplot;
    }

    subplot.updateDateTimeAxis = function() {
    if (d3.event) {
        globalx.xtransform_z = d3.event.transform.rescaleX(globalx.xtransform);
    } else {
        globalx.xtransform_z = globalx.xtransform;
    }
    xAxis = d3.axisBottom(globalx.xtransform_z)
        .tickSize(6)
        .tickPadding(3);
    xAxis2 = d3.axisBottom(globalx.xtransform_z)
        .tickSize(18)
        .tickPadding(3);

    var days = (globalx.xtransform_z.domain()[1]-globalx.xtransform_z.domain()[0])/1e3/3600/24;
    var interval_p, interval_s
    if (days < 0.005) {
      interval_p = d3.utcMinute;
      interval_s = d3.utcDay;
      tickformat_p = "%H:%M";
      tickformat_s = "%Y-%m-%d";
    } else if (days < 0.05) {
        interval_p = d3.utcMinute.every(5);
        interval_s = d3.utcDay;
        tickformat_p = "%H:%M";
        tickformat_s = "%Y-%m-%d";
    } else if (days < 0.2) {
        interval_p = d3.utcMinute.every(15);
        interval_s = d3.utcDay;
        tickformat_p = "%H:%M";
        tickformat_s = "%Y-%m-%d";
    } else if (days < 0.5) {
        interval_p = d3.utcMinute.every(30);
        interval_s = d3.utcDay;
        tickformat_p = "%H:%M";
        tickformat_s = "%Y-%m-%d";
    } else if (days < 1) {
        interval_p = d3.utcHour.every(1);
        interval_s = d3.utcDay;
        tickformat_p = "%H:%M";
        tickformat_s = "%Y-%m-%d";
    } else if (days < 4) {
        interval_p = d3.utcHour.every(3);
        interval_s = d3.utcDay;
        tickformat_p = "%H:%M";
        tickformat_s = "%Y-%m-%d";
    } else if (days < 6) {
        interval_p = d3.utcHour.every(6);
        interval_s = d3.utcDay;
        tickformat_p = "%H:%M";
        tickformat_s = "%Y-%m-%d";
    } else if (days < 12) {
        interval_p = d3.utcHour.every(12);
        interval_s = d3.utcDay;
        tickformat_p = "%H:%M";
        tickformat_s = "%Y-%m-%d";
    } else if (days < 0.25*365) {
        interval_p = d3.utcDay;
        interval_s = d3.utcMonth;
        tickformat_p = "%d";
        tickformat_s = "%b %Y";
    } else if (days < 3*365) {
        interval_p = d3.utcMonth;
        interval_s = d3.utcYear;
        tickformat_p = "%b";
        tickformat_s = "%Y";
    } else if (days < 30*365) {
        interval_p = d3.utcYear;
        interval_s = d3.utcYear.every(10);
        tickformat_p = "%Y";
        tickformat_s = "%Ys";
    } else {
      interval_p = d3.utcYear.every(10);
      interval_s = d3.utcYear.every(100);
      tickformat_p = "%Y";
      tickformat_s = "%Ys";
    }

    // Primary (top) x-axis
    xAxis.ticks(interval_p).tickFormat(d3.utcFormat(tickformat_p));
    foregroundaxes.selectAll(".axis--x").call(xAxis)
      .selectAll(".tick text")
        .style("visibility", xaxisannotation)
        .style("text-anchor", "start")
        .attr("x", 4)
        .attr("y", 3);

    // Secondary (lower) x-axis
    xAxis2.ticks(interval_s).tickFormat(d3.utcFormat(tickformat_s));
    foregroundaxes.select(".axis--x2").call(xAxis2)
      .selectAll("text")
        .style("text-anchor", "start")
        .style("visibility", xaxisannotation)
        .attr("x", 6)
        .attr("y", 18);

    // Daily/monthly/yearly grid
    var days = (globalx.xtransform_z.domain()[1]-globalx.xtransform_z.domain()[0])/1e3/3600/24;
    var pixelsPerDay = globalx.width / days;
    xDayTicks = d3.axisBottom(globalx.xtransform_z).ticks(d3.utcDay).tickSize(height);
    if (pixelsPerDay < 30)
    {
        xDayTicks = d3.axisBottom(globalx.xtransform_z).ticks(d3.utcMonth).tickSize(height);
        var pixelsPerMonth = pixelsPerDay * 30;
        if (pixelsPerMonth < 30)
        {
            var pixelsPerYear = pixelsPerDay * 365;
            xDayTicks = d3.axisBottom(globalx.xtransform_z).ticks(d3.utcYear).tickSize(height);
        }
    }
    foregroundaxes.select(".dayticks").style('visibility','visible').call(xDayTicks)
        .selectAll(".tick text")
        .style("visibility", "hidden");

    // Now indicator
    xnow = globalx.xtransform_z(new Date);
    foregroundaxes.select(".nowindicator")
      .attr("x1", xnow)
      .attr("x2", xnow)
      .attr("y1", ytransform(ymin))
      .attr("y2", ytransform(ymax));

    return subplot;
    }


    subplots.push(subplot);
    return subplot;
}

var now = new Date();
var tmaxzoom = d3.utcDay.offset(now, 180);

var parseDate = d3.utcParse("%Y-%m-%dT%H:%M:%SZ");

var timer = null;

function perform_zoom() {
  // This function is called each time the user performs a zoom/pan action with the mouse
  //   (panning by dragging or zooming using the scroll wheel)
  var newx = d3.mouse(this)[0]; // Save the cursor x-location

  // Loop over each of the subplots (rows) to update their x-axes
  subplots.forEach(
    function(subplot, index) {
      subplot.setzoom(d3.event.transform);
      subplot.updateDateTimeAxis();
    }
  );

  // Loop over each of the plot elements (e.g. GFZ Kp and NOAA predicted Kp are two plot elements connected to the same subplot)
  plotelements.forEach(
    function(plotelement, index) {
      plotelement.update();
    }
  );

  // Restart the timer...
  if(timer !== null) {
      clearTimeout(timer);
  }
  // And if there are no new zoom events for 200 ms, the data that is displayed for each plotelement will be reloaded
  timer = setTimeout(function() {
    plotelements.forEach(
      function(plotelement, index) {
        plotelement.loadData();
      })
  }, 50);

}

var iso8601DurationRegex = /(-)?P(?:([.,\d]+)Y)?(?:([.,\d]+)M)?(?:([.,\d]+)W)?(?:([.,\d]+)D)?(?:T(?:([.,\d]+)H)?(?:([.,\d]+)M)?(?:([.,\d]+)S)?)?/;

parseISO8601Duration = function(iso8601Duration)
{
    var matches = iso8601Duration.match(iso8601DurationRegex);
    return {
        sign: matches[1] === undefined ? '+' : '-',
        years: matches[2] === undefined ? 0 : matches[2],
        months: matches[3] === undefined ? 0 : matches[3],
        weeks: matches[4] === undefined ? 0 : matches[4],
        days: matches[5] === undefined ? 0 : matches[5],
        hours: matches[6] === undefined ? 0 : matches[6],
        minutes: matches[7] === undefined ? 0 : matches[7],
        seconds: matches[8] === undefined ? 0 : matches[8]
    };
};

getISO8601DurationAsSeconds = function(iso8601Duration)
{
    var matches = iso8601Duration.match(iso8601DurationRegex);
    var t = 0;
    if (matches[2] != undefined) t += matches[2] * 365 * 24 * 3600;
    if (matches[3] != undefined) t += matches[3] * 30 * 24 * 3600;
    if (matches[4] != undefined) t += matches[4] * 7 * 24 * 3600;
    if (matches[5] != undefined) t += matches[5] * 24 * 3600;
    if (matches[6] != undefined) t += matches[6] * 3600;
    if (matches[7] != undefined) t += matches[7] * 60;
    if (matches[8] != undefined) t += matches[8];
    if (matches[1] != undefined) t *= -1.0;
    return t;
};

function getTime(isotime)
{
    if (isotime.startsWith('P') || isotime.startsWith('-P'))
    {
        //duration
        var now = new Date();
        return new Date(now.getTime() + getISO8601DurationAsSeconds(isotime) * 1000);
    }
    else
    {
        //time
        return new Date(isotime);
    }
}

