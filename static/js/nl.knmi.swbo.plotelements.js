function plot_element_timeline()
{
    var controller = new AbortController();
    var signal = controller.signal;
    var fetch_in_progress = 0;
    var ytransform;
    var plotgroup;
    var subplot;
    var plotelement = {};
    var fill = "none";
    var swdata = [];
    var server = '';
    var dataset = '';
    var parameters = '';
    var time = 'time';
    var imagearea = '';
    var x_animate = 0;
    var x_animate_step = 4;
    var playButton;
    var stopButton;
    var prevButton;
    var nextButton;
    var x_animate_loop = true;
    var duration = 0;
    var width = 1;
    var barPadding = 0.1;
    var barwidth = 1;
    var baroffset = 0;
    var cursor_index = 0;
    var url_loaded = '';
    var dt = 0;
    var t0_view = 0;
    var t1_view = 0;
    var timer;

    plotelement.setupButtons = function()
    {
        playButton = d3.select("#play-button");
        playButton
            .on("click", function() {
                var button = d3.select(this);
                if (button.text() == "Pause")
                {
                    clearInterval(timer);
                    button.text("Play");
                }
                else
                {
                    var nImagePlotElements = 0;
                    function oncompletion()
                    {
                        nImagePlotElements--;
                        if (nImagePlotElements === 0)
                        {
                            console.log('Loading of all image plot elements completed');
                            timer = setInterval(step, 50);
                            button.text("Pause");
                        }
                    }
                    button.text("Loading...");
                    for (var i = 0; i < plotelements.length; i++)
                    {
                        if (typeof plotelements[i].loadImagesInView === 'function')
                        {
                            nImagePlotElements++;
                        }
                    }
                    for (var i = 0; i < plotelements.length; i++)
                    {
                        if (typeof plotelements[i].loadImagesInView === 'function')
                        {
                            plotelements[i].loadImagesInView(oncompletion);
                        }
                    }
                }
            })

        stopButton = d3.select("#stop-button");
        stopButton
            .on("click", function() {
                var button = d3.select(this);
                clearInterval(timer);
                x_animate = 0;
                d3.selectAll(".animationindicator")
                    .attr("x1", x_animate)
                    .attr("x2", x_animate);
                for (var i = 0; i < plotelements.length; i++)
                {
                    if (typeof plotelements[i].reset === 'function')
                    {
                        plotelements[i].reset();
                    }
                }
                playButton.text("Play");
            })

        prevButton = d3.select("#prev-button");
        prevButton
            .on("click", function() {
                dostep(-1);
            })

        nextButton = d3.select("#next-button");
        nextButton
            .on("click", function() {
                dostep(1);
            })
    }

    plotelement.reset = function()
    {
        cursor_index = 0;
        url_loaded = '';
    }

    plotelement.loadImagesInView = function(oncompletion)
    {
        var imagesToLoad = 0;

        function onload()
        {
            imagesToLoad--;
            if (imagesToLoad === 0)
            {
                oncompletion();
            }
        }
        if (swdata.length > 1)
        {
            var dt = swdata[1]['datetime'] - swdata[0]['datetime'];
            var t0_view_extended = new Date(t0_view.getTime() - dt);
            for (var i = 0; i < swdata.length; i++)
            {
                var date = swdata[i]['datetime'];
                if (date >= t0_view_extended && date <= t1_view)
                {
                    if (swdata[i]['url_low_res'] !== undefined)
                    {
                        imagesToLoad++;
                    }
                }
            }
        }
        for (var i = 0; i < swdata.length; i++)
        {
            var date = swdata[i]['datetime'];
            if (date >= t0_view_extended && date <= t1_view)
            {
                if (swdata[i]['url_low_res'] !== undefined)
                {
                    var url = swdata[i]['url_prefix'] + '/' + swdata[i]['url_low_res'];
                    var img = document.createElement('img');
                    img.src = url;
                    img.onload = onload;
                    img.onerror = onload;
                    swdata[i]['img'] = img;
                }
            }
        }
    }

    plotelement.updateImage = function(x_animate)
    {
        var date_cursor = globalx.xtransform_z.invert(x_animate);
        var start = cursor_index - 1;
        if (start < 0) start = 0;
        for (var i = start; i < swdata.length; i++)
        {
            if (swdata[i]['datetime'] >= date_cursor)
            {
                cursor_index = i;
                break;
            }
        }
        if (cursor_index < 1) return;
        data_index = cursor_index - 1;
        if (cursor_index == swdata.length - 1)
        {
            if (date_cursor >= swdata[swdata.length - 1]['datetime'])
            {
                data_index = swdata.length - 1;
            }
        }
        var url = swdata[data_index]['url_prefix'] + '/' + swdata[data_index]['url_low_res'];
        if (url != url_loaded)
        {
            url_loaded = url;
            var url_high_res = swdata[data_index]['url_prefix'] + '/' + swdata[data_index]['url_high_res'];
            if (imagearea != '')
            {
                const node = document.getElementById(imagearea);
                while (node && node.firstChild)
                {
                    node.removeChild(node.lastChild);
                }
                if (typeof swdata[data_index]['img'] != 'undefined')
                {
                    swdata[data_index]['img'].style.width = "384px"; //node.style.width;
                    node.appendChild(swdata[data_index]['img']);
                    node.onclick = function() {
                        window.open(url_high_res);
                    }
                }
            }
        }
    }

    function step()
    {
        dostep(1);
    }

    function dostep(direction)
    {
        x_animate = x_animate + x_animate_step * direction;
        var now = new Date();
        var date_cursor = globalx.xtransform_z.invert(x_animate);
        if (x_animate < 0 || x_animate > globalx.width || date_cursor > now)
        {
            for (var i = 0; i < plotelements.length; i++)
            {
                if (typeof plotelements[i].reset === 'function')
                {
                    plotelements[i].reset();
                }
            }
            x_animate = 0;
            if (!x_animate_loop)
            {
                clearInterval(timer);
                playButton.text("Play");
            }
            return;
        }

        d3.selectAll(".animationindicator")
            .attr("x1", x_animate)
            .attr("x2", x_animate);

        for (var i = 0; i < plotelements.length; i++)
        {
            if (typeof plotelements[i].updateImage === 'function')
            {
                plotelements[i].updateImage(x_animate);
            }
        }
    }

    plotelement.loadData = function()
    {
        function formatDate(date)
        {
            return date.toISOString().slice(0, 19);
        }

        function transformData(d)
        {
            dd = {};
            dd['datetime'] = new Date(d['time']);
            if (d['url_prefix'] == '')
            {
                dd['marker'] = 0;
            }
            else
            {
                dd['url_prefix'] = d['url_prefix'];
                dd['url_low_res'] = d['url_low_res'];
                dd['url_high_res'] = d['url_high_res'];
                dd['marker'] = 3;
            }
            return dd;
        }

        for (var i = 0; i < plotelements.length; i++)
        {
            if (typeof plotelements[i].reset === 'function')
            {
                plotelements[i].reset();
            }
        }
        x_animate = 0;

        // use a larger time window to make panning more smooth
        t0_view = new Date(globalx.xtransform_z.domain()[0].getTime());
        t1_view = new Date(globalx.xtransform_z.domain()[1].getTime());
        dt = t1_view.getTime() - t0_view.getTime();
        t0_load = new Date(t0_view.getTime() - dt);
        t1_load = new Date(t1_view.getTime() + dt);

        var zoom_level_resolution = 60;
        var min_zoom_level = 0;
        var min_pixels = 10;

        var x = Math.max(1.0, min_pixels / (globalx.xtransform_z(zoom_level_resolution*1000) - globalx.xtransform_z(0)));
        var y = Math.max(min_zoom_level, Math.ceil(Math.log2(x)));
        var z = zoom_level_resolution * (1.0 << y);

        var subsample_time = '' + z + 'S';

        if (fetch_in_progress > 0)
        {
            controller.abort();
  	        controller = new AbortController();
            signal = controller.signal;
        }

        var url = server;
        url += '/data?id=' + dataset;
        url += '&parameters=' + time + ',' + parameters;
        url += '&time.min=' + formatDate(t0_load) + 'Z';
        url += '&time.max=' + formatDate(t1_load) + 'Z';

        if (server.includes('hapi-r'))
        {
            url += '&resample=' + subsample_time;
        }
        fetch_in_progress++;
        fetch(url, { signal })
            .then((resp) => resp.text())
            .then(function(csvdata) {
                fetch_in_progress--;
                csvdata = 'time,' + parameters + '\n' + csvdata;
                data = d3.csvParse(csvdata, transformData);
                swdata = data;
      	        plotelement.setup();
                plotelement.update();
            })
            .catch(function(error) {
                fetch_in_progress--;
                console.log(error);
            });
    }

    plotelement.subplot = function(value)
    {
        if (!arguments.length) return subplot;
        subplot = value;
        ytransform = subplot.ytransform();
        plotgroup = subplot.plotgroup();
        return plotelement;
    }

    plotelement.server = function(value)
    {
        if (!arguments.length) return server;
        server = value;
        return plotelement;
    }

    plotelement.dataset = function(value)
    {
        if (!arguments.length) return dataset;
        dataset = value;
        return plotelement;
    }

    plotelement.parameters = function(value)
    {
        if (!arguments.length) return parameters;
        parameters = value;
        return plotelement;
    }

    plotelement.time = function(value)
    {
        if (!arguments.length) return time;
        time = value;
        return plotelement;
    }

    plotelement.fill = function(value)
    {
        if (!arguments.length) return fill;
        fill = value;
        return plotelement;
    }

    plotelement.imagearea = function(value)
    {
        if (!arguments.length) return fill;
        imagearea = value;
        return plotelement;
    }

    plotelement.setup = function()
    {
        plotgroup.selectAll(".imgtl")
            .remove();

        plotgroup.selectAll(".imgtl")
            .data(swdata)
            .enter()
            .append("rect")
            .attr("class", "imgtl")
            .style("fill", fill)
            .attr("height", function(d) {  return ytransform.range()[0] - ytransform(d.marker); })
            .attr("clip-path", "url(#" +  plotelement.subplot().id() + "_clip)");

        return plotelement;
    }

    plotelement.update = function(data)
    {
        if (swdata.length > 1) {
            duration = swdata[1]['datetime'] - swdata[0]['datetime'];
            width = globalx.xtransform_z(duration) - globalx.xtransform_z(0);
            barwidth = width * (1 - barPadding);
            baroffset = width * barPadding / 2;

            plotgroup.selectAll(".imgtl")
                .attr("x", function(d) { return globalx.xtransform_z(d.datetime) + baroffset; })
                .attr("y", function(d) { return ytransform(d.marker); })
                .attr("width", barwidth);
        }
        return plotelement;
    }

    plotelements.push(plotelement);
    return plotelement;
}

function subplot_generator(elid) {
    // subplot object -> attaches to a div, will contain a row
    // uses global xaxis object,
    // Draws axes (and title)
    // on zoom, it should update the plot elements, such as timeseries lines, attached to it
    var chartDiv = document.getElementById(elid);

    // Default title, can be overwritten
    var title = "Title";
    var id = "id_not_set";

    // Default y-axis limits and dimensions
    var ymin = -1000;
    var ymax = 1000;
    var margins = {top: 0, bottom: 0};
    var height = 50;
    var ytransform = d3.scaleLinear().range([height, 0]).domain([ymin,ymax]);
    var yAxis = d3.axisLeft(ytransform); // Generate SVG code for y-axis
    var ydynamic = true;

    // Default annotation
    var xaxisannotation = "visible";
    var ytickformat = "";

    // The subplot also provides a vertical bar at the current time (now)
    var now = new Date();

    // X-axes: There are primary and secondary axes, so that separate ticks can be shown for, for example, months and days
    var xAxis = d3.axisBottom(globalx.xtransform)
          .tickSize(6)
          .tickPadding(3);
    var xAxis2 = d3.axisBottom(globalx.xtransform)
          .tickSize(18)
          .tickPadding(3);
    // xDayTicks are the thin vertical bars at day boundaries
    var xDayTicks = d3.axisBottom(globalx.xtransform).ticks(d3.utcDay).tickSize(height); // Generate SVG code for x-axis

    // Set up vars for zooming and panning
    var zoomable = true;
    var zoom = d3.zoom()
      .on("zoom", perform_zoom);


    var animation = false;

    var foregroundaxes; // The foregroundaxes will be an SVG group that overlays the actual data, so that the data does not hide the tickmarks, etc.
    var plotgroup;      // Plotgroup will be an SVG group to which the actual data will be added.
    var mouseindicator; // The mouseindicator will show thin vertical lines at the location (time) of the mouse cursor.
    var zoomrect;       // The zoomrect will be an invisible SVG element on which d3.js detects zoom action from the mouse or trackpad.

    // Create the subplot object and getter/setters for the properties
    subplot = {};

    subplot.elid = function(value) {
      return elid;
    }

    subplot.title = function(value) {
      if (!arguments.length) return title;
      title = value;
      return subplot;
    }

    subplot.id = function(value) {
      if (!arguments.length) return id;
      id = value;
      return subplot;
    }

    subplot.zoomable= function(value) {
      if (!arguments.length) return zoomable;
      zoomable = value;
      return subplot;
    }

    subplot.xaxisannotation = function(value) {
      if (!arguments.length) return xaxisannotation;
      xaxisannotation = value;
      return subplot;
    }

    subplot.animation = function(value) {
      if (!arguments.length) return animation;
      animation = value;
      return subplot;
    }

    subplot.height = function(value) {
      if (!arguments.length) return height;
      height = value;
      ytransform
        .range([height, 0]).domain([ymin,ymax]);
      yAxis = d3.axisLeft(ytransform).ticks(4).tickFormat(ytickformat);
      return subplot;
    }

    subplot.margins = function(value) {
      if (!arguments.length)
      {
        return margins;
      }
      margins = value;
      return subplot;
    }

    subplot.ytransform = function(value) {
      if (!arguments.length) return ytransform;
      ytransform = value;
      return subplot;
    }

    subplot.ytickformat = function(value) {
      if (!arguments.length) return ytickformat;
      ytickformat = value;
      yAxis.tickFormat(d3.format(ytickformat));
      return subplot;
    }

    subplot.ymin = function(value) {
      if (!arguments.length) return ymin;
      ymin = value;
      ytransform.domain([ymin, ymax]);
      return subplot;
    }

    subplot.ymax = function(value) {
      if (!arguments.length) return ymax;
      ymax = value;
      ytransform.domain([ymin, ymax]);
      return subplot;
    }

    subplot.ydynamic = function(value)
    {
        if (!arguments.length) return ydynamic;
        ydynamic = value;
        return subplot;
    }

    subplot.plotgroup = function(value) {
      if (!arguments.length) return plotgroup;
      plotgroup = value;
      return subplot;
    }

    // Code to render the SVG of the subplot
    subplot.draw = function() {
        var svg = d3.select(chartDiv).append("svg")
            .attr("width", globalx.width + globalx.margins.left + globalx.margins.right)
            .attr("height", height + margins.top + margins.bottom);

        defs = svg.append("defs")
        clip = defs.append("clipPath")
            .attr("id", id + "_clip")
            .append("rect")
            .attr("width", globalx.width)
            .attr("height", height);

        plotgroup = svg.append("g")
            .attr("class", "plotgroup")
            .attr("transform", "translate(" + globalx.margins.left + "," + margins.top + ")");

        foregroundaxes = svg.append("g")
              .attr("id", "foregroundaxesid")
              .attr("class", "foregroundaxes")
              .attr("transform", "translate(" + globalx.margins.left + "," + margins.top + ")");

        var nowindicator = foregroundaxes.append("line")
            .attr("class","nowindicator")
            .attr("clip-path", "url(#"+id+"_clip)")
            .attr("x1", globalx.xtransform(now))
            .attr("x2", globalx.xtransform(now))
            .attr("y1", ytransform(ymin))
            .attr("y2", ytransform(ymax));

        mouseindicator = foregroundaxes.append("line")
            .attr("class","mouseindicator")
            .attr("clip-path", "url(#"+id+"_clip)")
            .attr("x1", globalx.xtransform(now))
            .attr("x2", globalx.xtransform(now))
            .attr("y1", height)//ytransform(ymin))
            .attr("y2", 0);//ytransform(ymax));

        animationindicator = foregroundaxes.append("line")
            .attr("class","animationindicator")
            .attr("clip-path", "url(#"+id+"_clip)")
            .attr("x1", 0)
            .attr("x2", 0)
            .attr("y1", height)
            .attr("y2", 0);

        // setInterval(function() {
        //   var now = new Date()
        //   nowindicator.attr("x1", xtransform(now)).attr("x2", xtransform(now));
        // }, 1000)

        foregroundaxes.append("g")
            .attr("class", "axis dayticks")
            .call(xDayTicks)
            .selectAll(".tick text")
                .style("visibility", "hidden");

        foregroundaxes.append("g")
            .attr("class", "axis axis--x")
            .attr("transform", "translate(0," + height + ")")
            .call(xAxis);

        foregroundaxes.append("g")
            .attr("class", "axis axis--x2")
            .attr("transform", "translate(0," + height + ")")
            .call(xAxis2);

        foregroundaxes.append("g")
            .attr("class", "axis axis--y")
            .call(yAxis);

        foregroundaxes.append("text")
            .attr("class", "title")
            .attr("x", 8)
            .attr("y", 11)
            .text(title)

        if (zoomable) {
            zoomrect = svg.append("rect")
                .attr("class", "zoom")
                .attr("width", globalx.width)
                .attr("height", height)
                .attr("transform", "translate(" + globalx.margins.left + "," + margins.top + ")")
                .call(zoom)
                .on("mousemove", function() {
                    var newx = d3.mouse(this)[0];
                    t = globalx.xtransform_z.invert(newx);

                    subplots.forEach(
                        function(subplot, index) {
                            subplot.setmouseindicator(newx);
                        })
                    }
                )
                .on("mouseout", function() {
                    subplots.forEach(
                        function(subplot, index) {
                            subplot.hidemouseindicator();
                        })
                    }
                );
            }
        return subplot;
    }

    // The mouse indicator needs to be hidden when the mouse moves out of the zoomrect
    subplot.hidemouseindicator = function() {
        mouseindicator.classed("hidden", true);
    }

    subplot.setmouseindicator = function(newx) {
        mouseindicator.classed("hidden", false);
        mouseindicator.attr("x1", newx).attr("x2", newx);
    }


    subplot.setzoom = function(transform) {
        // Hack to get all the subplots to acquire the same zoom state
        // Otherwise, when zooming on different subplots, the zoom state jumps back to different original states.
        if (zoomable) { zoomrect.node().__zoom = transform; }
    }

    subplot.updateVerticalAxis = function() {
        foregroundaxes.select(".axis--y").call(yAxis);
        return subplot;
    }

    subplot.updateDateTimeAxis = function() {
    if (d3.event) {
        globalx.xtransform_z = d3.event.transform.rescaleX(globalx.xtransform);
    } else {
        globalx.xtransform_z = globalx.xtransform;
    }
    xAxis = d3.axisBottom(globalx.xtransform_z)
        .tickSize(6)
        .tickPadding(3);
    xAxis2 = d3.axisBottom(globalx.xtransform_z)
        .tickSize(18)
        .tickPadding(3);

    var days = (globalx.xtransform_z.domain()[1]-globalx.xtransform_z.domain()[0])/1e3/3600/24;
    var interval_p, interval_s
    if (days < 0.05) {
        interval_p = d3.utcMinute.every(5)
        interval_s = d3.utcDay
        tickformat_p = "%H:%M";
        tickformat_s = "%Y-%m-%d";
    } else if (days < 0.2) {
        interval_p = d3.utcMinute.every(15)
        interval_s = d3.utcDay
        tickformat_p = "%H:%M";
        tickformat_s = "%Y-%m-%d";
    } else if (days < 0.5) {
        interval_p = d3.utcMinute.every(30)
        interval_s = d3.utcDay
        tickformat_p = "%H:%M";
        tickformat_s = "%Y-%m-%d";
    } else if (days < 1) {
        interval_p = d3.utcHour.every(1);
        interval_s = d3.utcDay
        tickformat_p = "%H:%M";
        tickformat_s = "%Y-%m-%d";
    } else if (days < 4) {
        interval_p = d3.utcHour.every(3);
        interval_s = d3.utcDay
        tickformat_p = "%H:%M";
        tickformat_s = "%Y-%m-%d";
    } else if (days < 6) {
        interval_p = d3.utcHour.every(6);
        interval_s = d3.utcDay
        tickformat_p = "%H:%M";
        tickformat_s = "%Y-%m-%d";
    } else if (days < 12) {
        interval_p = d3.utcHour.every(12);
        interval_s = d3.utcDay
        tickformat_p = "%H:%M";
        tickformat_s = "%Y-%m-%d";
    } else if (days < 0.25*365) {
        interval_p = d3.utcDay;
        interval_s = d3.utcMonth
        tickformat_p = "%d";
        tickformat_s = "%b %Y";
    } else if (days < 3*365) {
        interval_p = d3.utcMonth;
        interval_s = d3.utcYear
        tickformat_p = "%b";
        tickformat_s = "%Y";
    } else {
        interval_p = d3.utcYear;
        interval_s = d3.utcYear.every(10)
        tickformat_p = "%Y";
        tickformat_s = "%Ys";
    }

    // Primary (top) x-axis
    xAxis.ticks(interval_p).tickFormat(d3.utcFormat(tickformat_p));
    foregroundaxes.selectAll(".axis--x").call(xAxis)
      .selectAll(".tick text")
        .style("visibility", xaxisannotation)
        .style("text-anchor", "start")
        .attr("x", 4)
        .attr("y", 3);

    // Secondary (lower) x-axis
    xAxis2.ticks(interval_s).tickFormat(d3.utcFormat(tickformat_s));
    foregroundaxes.select(".axis--x2").call(xAxis2)
      .selectAll("text")
        .style("text-anchor", "start")
        .style("visibility", xaxisannotation)
        .attr("x", 6)
        .attr("y", 18);

    foregroundaxes.select(".axis--y").call(yAxis)

    // Daily/monthly/yearly grid
    var days = (globalx.xtransform_z.domain()[1]-globalx.xtransform_z.domain()[0])/1e3/3600/24;
    var pixelsPerDay = globalx.width / days;
    xDayTicks = d3.axisBottom(globalx.xtransform_z).ticks(d3.utcDay).tickSize(height);
    if (pixelsPerDay < 30)
    {
        xDayTicks = d3.axisBottom(globalx.xtransform_z).ticks(d3.utcMonth).tickSize(height);
        var pixelsPerMonth = pixelsPerDay * 30;
        if (pixelsPerMonth < 30)
        {
            var pixelsPerYear = pixelsPerDay * 365;
            xDayTicks = d3.axisBottom(globalx.xtransform_z).ticks(d3.utcYear).tickSize(height);
        }
    }
    foregroundaxes.select(".dayticks").style('visibility','visible').call(xDayTicks)
        .selectAll(".tick text")
        .style("visibility", "hidden");

    // Now indicator
    xnow = globalx.xtransform_z(new Date);
    foregroundaxes.select(".nowindicator")
      .attr("x1", xnow)
      .attr("x2", xnow)
      .attr("y1", ytransform(ymin))
      .attr("y2", ytransform(ymax));

    return subplot;
    }


    subplots.push(subplot);
    return subplot;
}

var now = new Date();
var tmaxzoom = d3.utcDay.offset(now, 180);

var parseDate = d3.utcParse("%Y-%m-%dT%H:%M:%SZ");

var timer = null;

function perform_zoom() {
  // This function is called each time the user performs a zoom/pan action with the mouse
  //   (panning by dragging or zooming using the scroll wheel)
  var newx = d3.mouse(this)[0]; // Save the cursor x-location

  // Loop over each of the subplots (rows) to update their x-axes
  subplots.forEach(
    function(subplot, index) {
      subplot.setzoom(d3.event.transform);
      subplot.updateDateTimeAxis();
    }
  );

  // Loop over each of the plot elements (e.g. GFZ Kp and NOAA predicted Kp are two plot elements connected to the same subplot)
  plotelements.forEach(
    function(plotelement, index) {
      plotelement.update();
    }
  );

  // Restart the timer...
  if(timer !== null) {
      clearTimeout(timer);
  }
  // And if there are no new zoom events for 200 ms, the data that is displayed for each plotelement will be reloaded
  timer = setTimeout(function() {
    plotelements.forEach(
      function(plotelement, index) {
        plotelement.loadData();
      })
  }, 50);

}

var iso8601DurationRegex = /(-)?P(?:([.,\d]+)Y)?(?:([.,\d]+)M)?(?:([.,\d]+)W)?(?:([.,\d]+)D)?(?:T(?:([.,\d]+)H)?(?:([.,\d]+)M)?(?:([.,\d]+)S)?)?/;

parseISO8601Duration = function(iso8601Duration)
{
    var matches = iso8601Duration.match(iso8601DurationRegex);
    return {
        sign: matches[1] === undefined ? '+' : '-',
        years: matches[2] === undefined ? 0 : matches[2],
        months: matches[3] === undefined ? 0 : matches[3],
        weeks: matches[4] === undefined ? 0 : matches[4],
        days: matches[5] === undefined ? 0 : matches[5],
        hours: matches[6] === undefined ? 0 : matches[6],
        minutes: matches[7] === undefined ? 0 : matches[7],
        seconds: matches[8] === undefined ? 0 : matches[8]
    };
};

getISO8601DurationAsSeconds = function(iso8601Duration)
{
    var matches = iso8601Duration.match(iso8601DurationRegex);
    var t = 0;
    if (matches[2] != undefined) t += matches[2] * 365 * 24 * 3600;
    if (matches[3] != undefined) t += matches[3] * 30 * 24 * 3600;
    if (matches[4] != undefined) t += matches[4] * 7 * 24 * 3600;
    if (matches[5] != undefined) t += matches[5] * 24 * 3600;
    if (matches[6] != undefined) t += matches[6] * 3600;
    if (matches[7] != undefined) t += matches[7] * 60;
    if (matches[8] != undefined) t += matches[8];
    if (matches[1] != undefined) t *= -1.0;
    return t;
};

function getTime(isotime)
{
    if (isotime.startsWith('P') || isotime.startsWith('-P'))
    {
        //duration
        var now = new Date();
        return new Date(now.getTime() + getISO8601DurationAsSeconds(isotime) * 1000);
    }
    else
    {
        //time
        return new Date(isotime);
    }
}

