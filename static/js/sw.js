// global xaxis object -> determines level of zoom, on zoom, updates all subplots
Date.prototype.addDays=function(d){return new Date(this.valueOf()+864E5*d);};

function plot_element_sw(datacomponent) {
  // plot element object -> one specific for Kp, one for IMF bt, one for IMF bz, etc., 
  // provides setup and update methods
  // could be reused with different settings (for data source, style properties)

  var axes, ytransform, plotgroup, swdata, subplot;

  var plotelement = {};

  plotelement.subplot = function(value) {
    if (!arguments.length) return subplot;
    subplot = value;
    ytransform = subplot.ytransform();
    plotgroup = subplot.plotgroup();
    return plotelement;
  }

  plotelement.loadData = function() {
    function formatDate(date) {
      return date.toISOString().slice(0, 19);
    } 

    function datawidth(xtransform, duration) {
      w = xtransform(duration)-xtransform(0);  
      console.log('w: ', w);
      return w;
      
    }

   function transformData(d) {
      if (d[datacomponent] === "") { d[datacomponent] = 1e-3; } // Do not set to zero, because some variables are plotted on log scale
      d.datetime = parseDate(d.time);
      return d;
    }

    t0 = new Date(globalx.xtransform_z.domain()[0].getTime());
    t1 = new Date(globalx.xtransform_z.domain()[1].getTime());

    minpixels = 1;
    if (datawidth(globalx.xtransform_z, 60*1e3) > minpixels) {
      subsample_time = '1T';
    } else if (datawidth(globalx.xtransform_z, 3*60*1e3) > minpixels) {
      subsample_time = '3T';      
    } else if (datawidth(globalx.xtransform_z, 5*60*1e3) > minpixels) {
      subsample_time = '5T';
    } else if (datawidth(globalx.xtransform_z, 10*60*1e3) > minpixels) {
      subsample_time = '10T';
    } else if (datawidth(globalx.xtransform_z, 20*60*1e3) > minpixels) {
      subsample_time = '20T';          
    } else if (datawidth(globalx.xtransform_z, 30*60*1e3) > minpixels) {
      subsample_time = '30T';                
    } else if (datawidth(globalx.xtransform_z, 3600*1e3) > minpixels) {
      subsample_time = '1H';    
    } else if (datawidth(globalx.xtransform_z, 3*3600*1e3) > minpixels) {
      subsample_time = '3H';    
    } else if (datawidth(globalx.xtransform_z, 6*3600*1e3) > minpixels) {
      subsample_time = '6H';    
    } else if (datawidth(globalx.xtransform_z, 12*3600*1e3) > minpixels) {
      subsample_time = '12H';    
    } else if (datawidth(globalx.xtransform_z, 24*3600*1e3) > minpixels) {
      subsample_time = '1D';    
    } else if (datawidth(globalx.xtransform_z, 48*3600*1e3) > minpixels) {
      subsample_time = '2D';    
    } else {
      subsample_time = '4D';
    }

    url = '/get_timeseries/solar_wind_plasma_rt/' + formatDate(t0) + "/" + formatDate(t1) + '/' + subsample_time;
    if (datacomponent == "speed") {
    	url = '/get_timeseries_parameter/solar_wind_plasma_rt/speed/' + formatDate(t0) + "/" + formatDate(t1) + '/' + subsample_time;
    } else if (datacomponent == "speedace") {
    	url = '/get_timeseries_parameter/solar_wind_plasma_ace/speed/' + formatDate(t0) + "/" + formatDate(t1) + '/' + subsample_time;
    } else if (datacomponent == "speeddscovr") {
    	url = '/get_timeseries_parameter/solar_wind_plasma_dscovr/speed/' + formatDate(t0) + "/" + formatDate(t1) + '/' + subsample_time;
    } else if (datacomponent == "density") {
    	url = '/get_timeseries_parameter/solar_wind_plasma_rt/density/' + formatDate(t0) + "/" + formatDate(t1) + '/' + subsample_time;
    } else if (datacomponent == "temperature") {
    	url = '/get_timeseries_parameter/solar_wind_plasma_rt/temperature/' + formatDate(t0) + "/" + formatDate(t1) + '/' + subsample_time;
    } else if (datacomponent == "pressure") {
    	url = '/get_timeseries_parameter/solar_wind_plasma_rt/pressure/' + formatDate(t0) + "/" + formatDate(t1) + '/' + subsample_time;
    }
    console.log(datacomponent);

    d3.csv(url, transformData, function(error, data) {
      if (error) throw error;
      swdata = data;
      // max = d3.max(data, function(d) { return d[datacomponent]; });
      // console.log("max", max);
      plotelement.setup();
      plotelement.update();
    });
  }

  plotelement.setup = function() {

    //plotgroup.selectAll("." + datacomponent + "line")
    //    .remove();
    //plotgroup.selectAll("." + datacomponent + "area")
    //    .remove();

    plotgroup.append("path")
      .attr("clip-path", "url(#" +  plotelement.subplot().id() + "_clip)")
      .attr("class", datacomponent + "area");

    plotgroup.append("path")
      .attr("clip-path", "url(#" +  plotelement.subplot().id() + "_clip)")
      .attr("class", datacomponent + "line");

    return plotelement;
  }

  plotelement.update = function() {

    var area = d3.area()
      .defined(function(d) { return d[datacomponent] > 1e-2; })
      .x(function(d) { return globalx.xtransform_z(d.datetime); })
      .y0(function(d) { return ytransform(1e-6); })
      .y1(function(d) { return ytransform(d[datacomponent]); })

    plotgroup.selectAll("." + datacomponent + "area")
      .datum(swdata)
      .attr("d", area);      

    var line = d3.line()
      .defined(function(d) { return d[datacomponent] > 1e-2; })
      .x(function(d) { return globalx.xtransform_z(d.datetime); })
      .y(function(d) { return ytransform(d[datacomponent]); })

    plotgroup.selectAll("." + datacomponent + "line")
      .datum(swdata)
      .attr("d", line);
    
    return plotelement;
  }

  plotelements.push(plotelement);
  return plotelement;
}
