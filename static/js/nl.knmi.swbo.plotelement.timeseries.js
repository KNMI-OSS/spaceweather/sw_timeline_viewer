function plot_element_timeseries()
{
    var controller = new AbortController();
    var signal = controller.signal;
    var fetch_in_progress = 0;
    var subsample_time_stored = '';
    var axes;
    var ytransform;
    var plotgroup;
    var plotsubgroup;
    var subplot;
    var plotelement = {};
    var fill = "none";
    var fill_opacity = "100%";
    var stroke = "black";
    var stroke_width = "1pt";
    var mirror_area = false;
    var swdata = [];
    var server = '';
    var dataset;
    var parameters;
    var time = 'time';


    plotelement.loadData = function()
    {
        function formatDate(date)
        {
            return date.toISOString().slice(0, 19);
        }

        function transformData(d)
        {
            dd = {};
            for (const key in d)
            {
                if (key == 'time')
                {
                    dd['datetime'] = new Date(d['time']);
                }
                else
                {
                    dd[key] = parseFloat(d[key]);
                    //if (dd[key] < -1e30) dd[key] = NaN;
                }
            }
            return dd;
        }

        // use a larger time window to make panning more smooth
        t0_view = new Date(globalx.xtransform_z.domain()[0].getTime());
        t1_view = new Date(globalx.xtransform_z.domain()[1].getTime());
        dt = t1_view.getTime() - t0_view.getTime();
        t0_load = new Date(t0_view.getTime() - 0.5*dt);
        t1_load = new Date(t1_view.getTime() + 0.5*dt);

        var zoom_level_resolution = 60;
        var min_zoom_level = 0;
        var min_pixels = 3;

        x = Math.max(1.0, min_pixels / (globalx.xtransform_z(zoom_level_resolution*1000) - globalx.xtransform_z(0)));
        y = Math.max(min_zoom_level, Math.ceil(Math.log2(x)));
        z = zoom_level_resolution * (1.0 << y);

        subsample_time = '' + z + 'S';

        const merge_replace = 0;
        const merge_append_left = 1;
        const merge_append_right = 2;
        const merge_none = 3;

        var merge_flag = merge_replace;

        if (subsample_time == subsample_time_stored)
        {
            if (swdata.length != 0)
            {
                t0_buffer = (swdata[0]['datetime'])
                t1_buffer = (swdata[swdata.length - 1]['datetime'])
                if (t0_load < t0_buffer && t1_load > t0_buffer)
                {
                    t1_load = t0_buffer;
                    merge_flag = merge_append_left;
                }
                else if (t1_load > t1_buffer && t0_load < t1_buffer)
                {
                    t0_load = t1_buffer;
                    merge_flag = merge_append_right;
                }
                else
                {
                    merge_flag = merge_none;
                }
            }
        }
        else
        {
            merge_flag = merge_replace;
        }

        if (merge_flag != merge_none)
        {
            if (fetch_in_progress > 0)
            {
                controller.abort();
                  controller = new AbortController();
                signal = controller.signal;
            }

            var url = server;
            url += '/data?id=' + dataset;
            url += '&parameters=' + time + ',' + parameters;
            url += '&time.min=' + formatDate(t0_load) + 'Z';
            url += '&time.max=' + formatDate(t1_load) + 'Z';

            if (server.includes('hapi-r'))
            {
                url += '&resample=' + subsample_time;
            }

            fetch_in_progress++;
            fetch(url, { signal })
                .then((resp) => resp.text())
                .then(function(csvdata) {
                    fetch_in_progress--;
                    csvdata = 'time,' + parameters + '\n' + csvdata;
                    data = d3.csvParse(csvdata, transformData);
                    if (merge_flag == merge_append_left)
                    {
                        swdata = data.concat(swdata);
                    }
                    else if (merge_flag == merge_append_right)
                    {
                        swdata = swdata.concat(data);
                    }
                    else
                    {
                        swdata = data;
                    }
                    subsample_time_stored = subsample_time;
                    if (subplot.ydynamic())
                    {
                        var ymin = 1e10;
                        var ymax = -1e10;
                        for (i = 0; i < swdata.length; i++)
                        {
                            if (swdata[i][parameters] < ymin) ymin = swdata[i][parameters];
                            if (swdata[i][parameters] > ymax) ymax = swdata[i][parameters];
                        }
                        ytransform.domain([ymin,ymax]);
                        subplot.updateVerticalAxis();
                    }
                    plotelement.update();
                })
                .catch(function(error) {
                    fetch_in_progress--;
                    console.log(error);
                });
        }
    }

    plotelement.subplot = function(value)
    {
        if (!arguments.length) return subplot;
        subplot = value;
        ytransform = subplot.ytransform();
        plotgroup = subplot.plotgroup();
        return plotelement;
    }

    plotelement.server = function(value)
    {
        if (!arguments.length) return server;
        server = value;
        return plotelement;
    }

    plotelement.dataset = function(value)
    {
        if (!arguments.length) return dataset;
        dataset = value;
        return plotelement;
    }

    plotelement.parameters = function(value)
    {
        if (!arguments.length) return parameters;
        parameters = value;
        return plotelement;
    }

    plotelement.time = function(value)
    {
        if (!arguments.length) return time;
        time = value;
        return plotelement;
    }

    plotelement.fill = function(value)
    {
        if (!arguments.length) return fill;
        fill = value;
        return plotelement;
    }

    plotelement.fill_opacity = function(value)
    {
        if (!arguments.length) return fill_opacity;
        fill_opacity = value;
        return plotelement;
    }

    plotelement.stroke = function(value)
    {
        if (!arguments.length) return stroke;
        stroke = value;
        return plotelement;
    }

    plotelement.stroke_width = function(value)
    {
        if (!arguments.length) return stroke_width;
        stroke_width = value;
        return plotelement;
    }

    plotelement.mirror_area = function(value)
    {
        if (!arguments.length) return mirror_area;
        mirror_area = value;
        return plotelement;
    }

    plotelement.setup = function()
    {
        groupid   = dataset  + "_" + parameters + "_group";
        lineclass = dataset + "_" + parameters + "_line";
        areaclass = dataset + "_" + parameters + "_area";
        plotsubgroup = plotgroup.append("g")
            .attr("id", groupid);


        plotsubgroup.append("path")
            .attr("clip-path", "url(#" +  plotelement.subplot().subplotid() + "_clip)")
            .attr("class", areaclass)
            .style("stroke", "none")
            .style("fill", fill)
            .style("fill-opacity", fill_opacity);

        plotsubgroup.append("path")
            .attr("clip-path", "url(#" +  plotelement.subplot().subplotid() + "_clip)")
            .attr("class", lineclass)
            .style("stroke", stroke)
            .style("stroke-width", stroke_width)
            .style("fill", "none");

        return plotelement;
    }

    var firstrun = true;
    plotelement.update = function()
    {
        if (firstrun) {
            plotelement.setup();
            firstrun = false;
        }
        lineclass = dataset + "_" + parameters + "_line";
        areaclass = dataset + "_" + parameters + "_area";
        var line = d3.line()
            .defined(function(d) { return !isNaN(d[parameters]); })
            .x(function(d) { return globalx.xtransform_z(d.datetime); })
            .y(function(d) { return ytransform(d[parameters]); });

        plotgroup.selectAll("." + lineclass)
            .datum(swdata)
            .attr("d", line);

        var area = d3.area()
            .defined(function(d) { return !isNaN(d[parameters]); })
            .x(function(d) { return globalx.xtransform_z(d.datetime); })
            .y0(function(d) {
                if (isNaN(d[parameters]))
                {
                    return NaN;
                }
                else
                {
                    if (mirror_area)
                    {
                        return ytransform(-1*d[parameters]);
                    }
                    else
                    {
                        return ytransform(1e-20);
                    }
                }
            })
            .y1(function(d) { return ytransform(d[parameters]); });

        plotgroup.selectAll("." + areaclass)
            .datum(swdata)
            .attr("d", area);
        return plotelement;
    }

    plotelements.push(plotelement);
    return plotelement;
}
