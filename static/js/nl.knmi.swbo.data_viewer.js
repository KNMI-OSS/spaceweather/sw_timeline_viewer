var globalx;
var plotlayout = {};
var subplots = [];
var plotelements = [];

function dataViewerClear()
{
    subplots = [];
    plotelements = [];
    d3.selectAll("svg > *").remove();
    const element = document.getElementById("viewer-container");
    while (element.firstChild) {
        element.removeChild(element.lastChild);
    }
}

function dataViewerPlotJson(url)
{
    fetch(url)
        .then((resp) => resp.json())
        .then(function(data) {
            dataViewerPlot(data);
        });
}

function dataViewerPlot(newplotlayout)
{
    plotlayout = newplotlayout;

    window.addEventListener('resize', function(event) {
        dataViewerPlotInit();
    }, true);

    dataViewerPlotInit();
}

function dataViewerPlotInit()
{
    dataViewerClear();

    var viewercontainer = document.getElementById("viewer-container");
    var rect = viewercontainer.getBoundingClientRect();
    var bodyRect = document.body.getBoundingClientRect();
    var height = bodyRect.bottom - bodyRect.top - rect.top - 30;
    viewercontainer.style.height = '' + height + 'px';

    var imageCount = 0;
    var fixedHeight = 30;
    var flexHeight = 0;
    var subplots = plotlayout['subplots'];
    for (i = 0; i < subplots.length; i++)
    {
        if (subplots[i]['type'] === 'image_timeseries')
        {
            imageCount++;
            fixedHeight += subplots[i]['height'];
        }
        else
        {
            flexHeight += subplots[i]['height'];
        }
        fixedHeight += 35; /* Was 25, but then the buttons fall off on my screen */
    }
    var heightScaling = 1;
    if (plotlayout['yautoscaling'])
    {
        heightScaling = (height - fixedHeight) / flexHeight;
    }
    var autoUpdate = plotlayout['autoupdate'];

    if (imageCount > 0)
    {
        var div = '<div class="image-container" id="image-container"></div>';
        viewercontainer.innerHTML = div;
    }
    var div = '<div class="chart-container" id="chart-container"></div>';
    viewercontainer.innerHTML += div;
    var div = '<div class="chart-container" id="layout-container"></div>';
    viewercontainer.innerHTML += div;

    var imagecontainer = document.getElementById("image-container");
    var chartcontainer = document.getElementById("chart-container");
    var layoutcontainer = document.getElementById("layout-container");

    globalx = {
        margins: {left: 60, right: 40},
        get width() {
            return document.getElementById("chart-container").clientWidth - this.margins.left - this.margins.right
        },
    };

    var tmin1 = getTime(plotlayout['tmin']);
    var tmax1 = getTime(plotlayout['tmax']);

    globalx.xtransform = d3.scaleUtc().range([0, globalx.width]).domain([tmin1, tmax1]);
    globalx.xtransform_z = d3.scaleUtc().range([0, globalx.width]).domain([tmin1, tmax1]);

    var innerHeight = window.innerHeight;
    var lastImageTimeSeriesElement;

    imageCount = 0;
    for (i = 0; i < subplots.length; i++)
    {
        if (subplots[i]['type'] === 'kp_timeseries')
        {
            subplot = new subplot_generator("chart-container")
                .title(subplots[i]['title'])
                .subplotid(subplots[i]['subplotid'])
                .ymin(subplots[i]['ymin'])
                .ymax(subplots[i]['ymax'])
                .ydynamic(subplots[i]['ydynamic'])
                .height(subplots[i]['height'] * heightScaling)
                .margins({top: 5, bottom: 20})
                .xaxisannotation(subplots[i]['xaxisannotation'])
                .ytickformat(d3.format(subplots[i]['ytickformat']))
                .draw()
                .updateDateTimeAxis();

            var elements = subplots[i]['elements'];
            for (j = 0; j < elements.length; j++)
            {
                element = new plot_element_kp()
                    .subplot(subplot)
                    .server(elements[j]['server'])
                    .dataset(elements[j]['dataset'])
                    .parameters(elements[j]['parameters'])
                    .barpadding(elements[j]['barpadding'])
                    .showkptext(elements[j]['showkptext']);

                element
                    .loadData();
            }
        }
        else if (subplots[i]['type'] === 'image_timeseries')
        {
            subplot = new subplot_generator("chart-container")
                .title(subplots[i]['title'])
                .subplotid(subplots[i]['subplotid'])
                .ymin(subplots[i]['ymin'])
                .ymax(subplots[i]['ymax'])
                .height(subplots[i]['height'])
                .xaxisannotation(subplots[i]['xaxisannotation'])
                .animation(subplots[i]['animation']);

            if (subplots[i]['xaxisannotation'] === 'visible')
            {
                subplot.margins({top: 0, bottom: 40})
                    .draw()
                    .updateDateTimeAxis();
            }
            else
            {
                subplot.margins({top: 0, bottom: 20})
                    .draw()
                    .updateDateTimeAxis();
            }

            var elements = subplots[i]['elements'];
            for (j = 0; j < elements.length; j++)
            {
                var id = imageCount + 1;
                div = '<div id="imagecontainer' + id + '" class="image" style="border-color: ' + elements[j]['fill'] + ';"></div>'
                imagecontainer.innerHTML += div;

                element = new plot_element_image_sequence()
                    .server(elements[j]['server'])
                    .dataset(elements[j]['dataset'])
                    .parameters(elements[j]['parameters'])
                    .subplot(subplot)
                    .fill(elements[j]['fill'])
                    .imagearea(elements[j]['imagearea']);

                element
                    .loadData();

                lastImageTimeSeriesElement = element;
            }
            imageCount++;
        }
        else if (subplots[i]['type'] === 'timeseries')
        {
            subplot = new subplot_generator("chart-container")
                .title(subplots[i]['title'])
                .subplotid(subplots[i]['subplotid'])
            if (subplots[i]['ylog'])
            {
                subplot.ytransform(d3.scaleLog().range([10, 0]).domain([subplots[i]['ymin'],subplots[i]['ymax']]));
            }
            subplot
                .ymin(subplots[i]['ymin'])
                .ymax(subplots[i]['ymax'])
                .ydynamic(subplots[i]['ydynamic'])
                .height(subplots[i]['height'] * heightScaling)
                .xaxisannotation(subplots[i]['xaxisannotation'])
                .ytickformat(d3.format(subplots[i]['ytickformat']));

            if (subplots[i]['xaxisannotation'] === 'visible')
            {
                subplot.margins({top: 5, bottom: 40})
                    .draw()
                    .updateDateTimeAxis();
            }
            else
            {
                subplot.margins({top: 5, bottom: 20})
                    .draw()
                    .updateDateTimeAxis();
            }

            var elements = subplots[i]['elements'];
            for (j = 0; j < elements.length; j++)
            {
                element = new plot_element_timeseries()
                    .subplot(subplot)
                    .server(elements[j]['server'])
                    .dataset(elements[j]['dataset'])
                    .parameters(elements[j]['parameters'])
                    .stroke(elements[j]['stroke'])
                    .stroke_width(elements[j]['stroke_width'])
                    .fill(elements[j]['fill'])
                    .fill_opacity(elements[j]['fill_opacity'])
                    .mirror_area(elements[j]['mirror_area']);

                element
                    .loadData();
            }
        }
        else if (subplots[i]['type'] === 'symbols')
        {
            subplot = new subplot_generator("chart-container")
                .title(subplots[i]['title'])
                .subplotid(subplots[i]['subplotid'])
            subplot
                .ymin(subplots[i]['ymin'])
                .ymax(subplots[i]['ymax'])
                .ydynamic(subplots[i]['ydynamic'])
                .height(subplots[i]['height'] * heightScaling)
                .xaxisannotation(subplots[i]['xaxisannotation'])
                .ytickformat(d3.format(subplots[i]['ytickformat']));
        
            if (subplots[i]['xaxisannotation'] === 'visible')
            {
                subplot.margins({top: 5, bottom: 40})
                    .draw()
                    .updateDateTimeAxis();
            }
            else
            {
                subplot.margins({top: 5, bottom: 20})
                    .draw()
                    .updateDateTimeAxis();
            }
        
            var elements = subplots[i]['elements'];
            for (j = 0; j < elements.length; j++)
            {
                element = new plot_element_symbols()
                    .subplot(subplot)
                    .server(elements[j]['server'])    
                element
                    .loadData();
            }
        }
        
    }


    div = '<label for="file-save" class="file-button">Save layout</label><input id="file-save" type="button" onclick="dataViewerSaveLayout()"/>';
    layoutcontainer.innerHTML += div;

    div = '<label for="file-upload" class="file-button">Load layout</label><input id="file-upload" type="file"/>';
    layoutcontainer.innerHTML += div;

    if (imageCount > 0)
    {
        var div = '<div class="control-container" id="control-container"></div>';
        imagecontainer.innerHTML += div;
        var controlcontainer = document.getElementById("control-container");

        div = '<button id="play-button" class="animation-button">Play</button>'
        controlcontainer.innerHTML += div;

        div = '<button id="stop-button" class="animation-button">Stop</button>'
        controlcontainer.innerHTML += div;

        div = '<button id="prev-button" class="animation-button">Prev</button>'
        controlcontainer.innerHTML += div;

        div = '<button id="next-button" class="animation-button">Next</button>'
        controlcontainer.innerHTML += div;

        lastImageTimeSeriesElement.setupButtons();
    }

    document.getElementById('file-upload').addEventListener('change', dataViewerLoadLayout, false);

    var autoUpdateTimer = 0;
    if (autoUpdate > 0)
    {
        autoUpdateTimer = setInterval(dataViewerUpdate, autoUpdate * 1000);
    }
}

function dataViewerUpdate()
{

    var tmin1 = getTime(plotlayout['tmin']);
    var tmax1 = getTime(plotlayout['tmax']);
    globalx.xtransform = d3.scaleUtc().range([0, globalx.width]).domain([tmin1, tmax1]);
    globalx.xtransform_z = d3.scaleUtc().range([0, globalx.width]).domain([tmin1, tmax1]);
    for (var i = 0; i < plotelements.length; i++)
    {
        plotelements[i].loadData();
    }
}

function dataViewerSaveLayout()
{
    const a = document.createElement("a");
    a.href = URL.createObjectURL(new Blob([JSON.stringify(plotlayout, null, 2)], {
            type: "text/plain"
        }));
    a.setAttribute("download", "plot_layout.json");
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
}

function dataViewerLoadLayout(e)
{
    var file = e.target.files[0];
    if (!file)
    {
        return;
    }
    var reader = new FileReader();
    reader.onload = function(e) {
        var contents = e.target.result;
        plotlayout = JSON.parse(contents);
        dataViewerPlot(plotlayout);
    };
    reader.readAsText(file);
}
