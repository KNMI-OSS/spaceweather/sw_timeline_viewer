function plot_element_symbols()
{
    var controller = new AbortController();
    var signal = controller.signal;
    var fetch_in_progress = 0;
    var subsample_time_stored = '';
    var axes;
    var ytransform;
    var plotgroup;
    var plotsubgroup;
    var subplot;
    var plotelement = {};
    var fill = "none";
    var fill_opacity = "100%";
    var stroke = "black";
    var stroke_width = "1pt";
    var mirror_area = false;
    var swdata = [];
    var server = '';
    var dataset;
    var parameters;
    var time = 'time';


    plotelement.loadData = function()
    {
        function formatDate(date)
        {
            return date.toISOString().slice(0, 19);
        }

        function transformData(d)
        {
            dd = {};
            for (const key in d)
            {
                if (key == 'time')
                {
                    dd['datetime'] = new Date(d['time']);
                }
                else
                {
                    dd[key] = parseFloat(d[key]);
                    //if (dd[key] < -1e30) dd[key] = NaN;
                }
            }
            return dd;
        }

        // use a larger time window to make panning more smooth
        t0_view = new Date(globalx.xtransform_z.domain()[0].getTime());
        t1_view = new Date(globalx.xtransform_z.domain()[1].getTime());
        dt = t1_view.getTime() - t0_view.getTime();
        t0_load = new Date(t0_view.getTime() - 0.5*dt);
        t1_load = new Date(t1_view.getTime() + 0.5*dt);

        var zoom_level_resolution = 60;
        var min_zoom_level = 0;
        var min_pixels = 3;

        x = Math.max(1.0, min_pixels / (globalx.xtransform_z(zoom_level_resolution*1000) - globalx.xtransform_z(0)));
        y = Math.max(min_zoom_level, Math.ceil(Math.log2(x)));
        z = zoom_level_resolution * (1.0 << y);

        subsample_time = '' + z + 'S';

        const merge_replace = 0;
        const merge_append_left = 1;
        const merge_append_right = 2;
        const merge_none = 3;

        var merge_flag = merge_replace;

        if (subsample_time == subsample_time_stored)
        {
            if (swdata.length != 0)
            {
                t0_buffer = (swdata[0]['datetime'])
                t1_buffer = (swdata[swdata.length - 1]['datetime'])
                if (t0_load < t0_buffer && t1_load > t0_buffer)
                {
                    t1_load = t0_buffer;
                    merge_flag = merge_append_left;
                }
                else if (t1_load > t1_buffer && t0_load < t1_buffer)
                {
                    t0_load = t1_buffer;
                    merge_flag = merge_append_right;
                }
                else
                {
                    merge_flag = merge_none;
                }
            }
        }
        else
        {
            merge_flag = merge_replace;
        }

        if (merge_flag != merge_none)
        {
            if (fetch_in_progress > 0)
            {
                controller.abort();
                  controller = new AbortController();
                signal = controller.signal;
            }

            var url = server;
            // url += '/data?id=' + dataset;
            // url += '&parameters=' + time + ',' + parameters;
            // url += '&time.min=' + formatDate(t0_load) + 'Z';
            // url += '&time.max=' + formatDate(t1_load) + 'Z';

            // if (server.includes('hapi-r'))
            // {
            //     url += '&resample=' + subsample_time;
            // }

            fetch_in_progress++;
            fetch(url, { signal })
                .then((resp) => resp.text())
                .then(function(jsondata) {
                    fetch_in_progress--;
                    data = JSON.parse(jsondata);
                    swdata = data;
                    plotelement.update();
                })
                .catch(function(error) {
                    fetch_in_progress--;
                    console.log(error);
                });
        }
    }

    plotelement.subplot = function(value)
    {
        if (!arguments.length) return subplot;
        subplot = value;
        ytransform = subplot.ytransform();
        plotgroup = subplot.plotgroup();
        return plotelement;
    }

    plotelement.server = function(value)
    {
        if (!arguments.length) return server;
        server = value;
        return plotelement;
    }

    plotelement.dataset = function(value)
    {
        if (!arguments.length) return dataset;
        dataset = value;
        return plotelement;
    }

    plotelement.parameters = function(value)
    {
        if (!arguments.length) return parameters;
        parameters = value;
        return plotelement;
    }

    plotelement.time = function(value)
    {
        if (!arguments.length) return time;
        time = value;
        return plotelement;
    }

    plotelement.fill = function(value)
    {
        if (!arguments.length) return fill;
        fill = value;
        return plotelement;
    }

    plotelement.fill_opacity = function(value)
    {
        if (!arguments.length) return fill_opacity;
        fill_opacity = value;
        return plotelement;
    }

    plotelement.stroke = function(value)
    {
        if (!arguments.length) return stroke;
        stroke = value;
        return plotelement;
    }

    plotelement.stroke_width = function(value)
    {
        if (!arguments.length) return stroke_width;
        stroke_width = value;
        return plotelement;
    }

    plotelement.mirror_area = function(value)
    {
        if (!arguments.length) return mirror_area;
        mirror_area = value;
        return plotelement;
    }

    plotelement.setup = function()
    {
        groupid   = "symbols_group";
        lineclass = "symbols_line";
        arcclass = "symbols_arc";
        textclass = "symbols_text";

        plotsubgroup = plotgroup.append("g")
            .attr("id", groupid)
            .attr("clip-path", "url(#" +  plotelement.subplot().subplotid() + "_clip)");

        plotsubgroup.selectAll("." + lineclass)
            .data(swdata['lines'])
            .enter()
            .append("line")
                .attr("class", lineclass)
                .attr("x1", function(d) { return globalx.xtransform_z(new Date(d['x1'])) } )
                .attr("x2", function(d) { return globalx.xtransform_z(new Date(d['x2'])) } )
                .attr("y1", function(d) { return ytransform(d['y1']) } )
                .attr("y2", function(d) { return ytransform(d['y2']) } )
                .style("stroke", function(d) { return d['stroke-color']})
                .style("stroke-width", function(d) { return d['stroke-width']});

        plotsubgroup.selectAll("." + arcclass)
            .data(swdata['arcs'])
            .enter()
            .append("path")
                .attr("class", arcclass)
                .attr("d", d3.arc()
                    .innerRadius(function(d) { return d['innerRadius'] } )
                    .outerRadius(function(d) { return d['outerRadius'] } )
                    .startAngle(function(d) { return d['startAngle'] } )
                    .endAngle(function(d) { return d['endAngle'] } )
                )
                .attr("transform", function(d) { return "translate(" + globalx.xtransform_z(new Date(d['x'])) + " " + ytransform(0) + ")" } )
                .style("fill", function(d) { return d['fill']} )
                .style("stroke", function(d) { return d['stroke-color']})
                .style("stroke-width", function(d) { return d['stroke-width']});

        plotsubgroup.selectAll("." + textclass)
            .data(swdata['texts'])
            .enter()
            .append("text")
                .attr("class", textclass)
                .attr("x", function(d) { return globalx.xtransform_z(new Date(d['x'])) } )
                .attr("y", function(d) { return ytransform(d['y']) } )
                .attr("transform", function(d) { return "translate(" + d['xshift'] + " " + d['yshift'] + ")" } )
                .text( function(d) {return d['text'] } )
                .style("text-anchor", function(d) {return d['text-anchor'] } )
                .style("dominant-baseline", function(d) {return d['dominant-baseline'] } )
                .style("font-family", "Helvetica")
                .style("font-size", '10px');


        return plotelement;
    }

    var firstrun = true;
    plotelement.update = function()
    {
        if (firstrun) {
            plotelement.setup();
            firstrun = false;
        }
        lineclass = "symbols_line";
        arcclass = "symbols_arc";
               
        plotsubgroup.selectAll("." + lineclass)
            .attr("x1", function(d) { return globalx.xtransform_z(new Date(d['x1'])) } )
            .attr("x2", function(d) { return globalx.xtransform_z(new Date(d['x2'])) } );


        plotsubgroup.selectAll("." + arcclass)
            .attr("transform", function(d) { return "translate(" + globalx.xtransform_z(new Date(d['x'])) + " " + ytransform(0) + ")" } );

        // Determine zoom level
        t0_view = new Date(globalx.xtransform_z.domain()[0].getTime());
        t1_view = new Date(globalx.xtransform_z.domain()[1].getTime());
        hours_in_view = (t1_view.getTime() - t0_view.getTime()) / 3600e3; 
        textvisible = hours_in_view < 48 ? 'visible' : 'hidden';
        
        plotsubgroup.selectAll("." + textclass)
            .attr("x", function(d) { return globalx.xtransform_z(new Date(d['x'])) } )
            .attr("y", function(d) { return ytransform(d['y']) } )
            .style("visibility", textvisible);
                       
        return plotelement;
    }

    plotelements.push(plotelement);
    return plotelement;
}
