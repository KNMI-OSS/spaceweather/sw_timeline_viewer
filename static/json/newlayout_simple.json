{
    "options": {
        "fill_vertical": true,
        "fill_horizontal": true,
        "max_data_points_in_view": 1400
    },
    "default_zoom": {
        "tmin": "-PT72H",
        "tmax": "PT12H"
    },
    "global_margins": {
        "top": 20,
        "bottom": 0,
        "left": 65,
        "right": 50
    },
    "subplots": [
        {
            "title": "Sunspot number and 10.7 cm radio flux",
            "visible": true,
            "minimized": true,
            "ydomain": {
                "range_default": [0, 350],
                "range_bounds": [0, 500],
                "step": 1
            },
            "y_label": "Solar activity (SSN, sfu)",
            "scale": "linear",
            "tick_format": "1d",
            "xaxis_annotation": "hidden",
            "yaxis_annotation": "visible",
            "annot_options": {
                "annot_horizontal_grid_lines": [
                    {"y": 100},
                    {"y": 200}
                ],
                "annot_text_items": [
                    {"y": 100, "text": "100"},
                    {"y": 200, "text": "200"}
                ]
            },
            "height": 150,
            "margins": {
                "top": 40,
                "bottom": 10
            },
            "plot_elements": [
                {
                    "minimized": true,
                    "server": "http://localhost:9000/hapi",
                    "dataset": "f10_7_P1D",
                    "cadence_suffix_from_catalog": true,
                    "time_parameter": "time",
                    "parameter": "f10_7",
                    "plot_options": {
                        "plot_type": "area",
                        "stroke": "#000000",
                        "stroke-width": "0",
                        "stroke-opacity": "0",
                        "fill": "#C5D0D9",
                        "fill-opacity": "1",
                        "mirror_zero_x": false
                    }
                },
                {
                    "minimized": true,
                    "server": "http://localhost:9000/hapi",
                    "dataset": "sunspots_silso_P1D",
                    "cadence_suffix_from_catalog": true,
                    "time_parameter": "time",
                    "parameter": "sunspot_number",
                    "plot_options": {
                        "plot_type": "line",
                        "stroke": "#000000",
                        "stroke-width": "1px",
                        "stroke-opacity": "100%"                    }
                }
            ]
        },
        {
            "title": "GFZ-Potsdam Hp30 index and SWPC 3-day Kp forecast",
            "visible": true,
            "minimized": true,
            "ydomain": {
                "default": [0, 9],
                "min_range": [0, 9],
                "max_range": [1, 15],
                "step": 0.3333,
                "mirror": false
            },
            "y_label": "Kp/Hp index",
            "scale": "linear",
            "tickformat": "1d",
            "xaxis_annotation": "hidden",
            "yaxis_annotation": "visible",
            "height": 150,
            "margins": {
                "top": 20,
                "bottom": 10
            },
            "plot_elements": [
                {
                    "minimized": true,
                    "server": "http://localhost:9000/hapi",
                    "dataset": "hp30_index_PT30M",
                    "cadence_suffix_from_catalog": true,
                    "time_parameter": "time",
                    "parameter": "Hp30",
                    "plot_options": {
                        "plot_type": "bars",
                        "bar_offset_seconds": 1800,
                        "bar_padding": 0.2,
                        "bar_text": true,
                        "stroke-width": "0px",
                        "fill_colors": [
                            {"threshold": 4.6, "color": "#88CC66"},
                            {"threshold": 5.6, "color": "#FFF033"},
                            {"threshold": 6.6, "color": "#FFBB44"},
                            {"threshold": 7.6, "color": "#DD9922"},
                            {"threshold": 8.6, "color": "#FF2020"},
                            {"threshold": 9.6, "color": "#A02020"},
                            {"threshold":1000, "color": "#300000"}
                        ]
                    }
                },
                {
                    "minimized": true,
                    "server": "http://localhost:9000/hapi",
                    "dataset": "kp_index_forecast_noaa_new_PT3H",
                    "cadence_suffix_from_catalog": true,
                    "time_parameter": "time",
                    "parameter": "kp_index",
                    "issue_time_parameter": "timetag_issue",
                    "plot_options": {
                        "plot_type": "bars",
                        "bar_offset_seconds": 1800,
                        "bar_padding": 0,
                        "bar_text": false,
                        "fill_colors": [
                            {"threshold": 4.6, "color": "#88CC66"},
                            {"threshold": 5.6, "color": "#FFF033"},
                            {"threshold": 6.6, "color": "#FFBB44"},
                            {"threshold": 7.6, "color": "#DD9922"},
                            {"threshold": 8.6, "color": "#FF2020"},
                            {"threshold": 9.6, "color": "#A02020"},
                            {"threshold":1000, "color": "#300000"}
                        ],
                        "fill-opacity": "30%",
                        "stroke-width": "1px",
                        "stroke_colors": [
                            {"threshold":1000, "color": "#888888"}
                        ]
                    }
                }
            ]
        },
        {
            "title": "Interplanetary Magnetic Field (nT)",
            "visible": true,
            "minimized": true,
            "ydomain": {
                "default": [-40, 40],
                "min_range": [-150, 150],
                "max_range": [-150, 150],
                "step": 1,
                "mirror": true
            },
            "scale": "linear",
            "tick_format": "1d",
            "xaxis_annotation": "hidden",
            "yaxis_annotation": "visible",
            "height": 280,
            "margins": {
                "top": 10,
                "bottom": 10
            },
            "plot_elements": [
                {
                    "minimized": true,
                    "server": "https://cdaweb.gsfc.nasa.gov/hapi",
                    "dataset": "OMNI_HRO2_1MIN",
                    "cadence_suffix_from_catalog": false,
                    "cadence": "PT1M",
                    "time_parameter": "Time",
                    "parameter": "F",
                    "plot_options": {
                        "plot_type": "area",
                        "stroke": "#000000",
                        "stroke-width": "0",
                        "stroke-opacity": "1",
                        "fill": "#C5D0D9",
                        "fill-opacity": "0.5",
                        "mirror_zero_x": true
                    }
                },
                {
                    "minimized": true,
                    "server": "https://cdaweb.gsfc.nasa.gov/hapi",
                    "dataset": "OMNI_HRO2_1MIN",
                    "cadence_suffix_from_catalog": false,
                    "cadence": "PT1H",
                    "time_parameter": "Time",
                    "parameter": "BZ_GSM",
                    "plot_options": {
                        "plot_type": "area",
                        "stroke": "#000000",
                        "stroke-width": "0",
                        "stroke-opacity": "1",
                        "fill": "#C4472E",
                        "fill-opacity": "1",
                        "mirror_zero_x": false
                    }
                },
                {
                    "minimized": true,
                    "server": "https://cdaweb.gsfc.nasa.gov/hapi",
                    "dataset": "OMNI2_H0_MRG1HR",
                    "cadence_suffix_from_catalog": false,
                    "cadence": "PT1H",
                    "time_parameter": "Time",
                    "parameter": "F1800",
                    "plot_options": {
                        "plot_type": "area",
                        "stroke": "#C5D0D9",
                        "stroke-width": "1",
                        "stroke-opacity": "1",
                        "fill": "#C5D0D9",
                        "fill-opacity": "0.2",
                        "mirror_zero_x": true
                    }
                },
                {
                    "minimized": true,
                    "server": "https://cdaweb.gsfc.nasa.gov/hapi",
                    "dataset": "OMNI2_H0_MRG1HR",
                    "cadence_suffix_from_catalog": false,
                    "cadence": "PT1H",
                    "time_parameter": "Time",
                    "parameter": "BZ_GSM1800",
                    "plot_options": {
                        "plot_type": "area",
                        "stroke": "#000000",
                        "stroke-width": "0",
                        "stroke-opacity": "1",
                        "fill": "#C4472E",
                        "fill-opacity": "0.2",
                        "mirror_zero_x": false
                    }
                }
            ]
        },
        {
            "title": "Solar wind speed (km/s)",
            "visible": true,
            "minimized": true,
            "ydomain": {
                "default": [200, 800],
                "min_range": [0, 3000],
                "max_range": [0, 3000],
                "step": 1,
                "mirror": false
            },
            "scale": "linear",
            "tick_format": "1d",
            "xaxis_annotation": "hidden",
            "yaxis_annotation": "visible",
            "height": 200,
            "margins": {
                "top": 10,
                "bottom": 10
            },
            "plot_elements": [
                {
                    "minimized": true,
                    "server": "https://cdaweb.gsfc.nasa.gov/hapi",
                    "dataset": "OMNI2_H0_MRG1HR",
                    "cadence_suffix_from_catalog": false,
                    "cadence": "PT1M",
                    "time_parameter": "Time",
                    "parameter": "V1800",
                    "plot_options": {
                        "plot_type": "area",
                        "stroke": "#C8911E",
                        "stroke-width": "1",
                        "stroke-opacity": "1",
                        "fill": "#C8911E",
                        "fill-opacity": "0.5",
                        "mirror_zero_x": false
                    }
                },
                {
                    "minimized": true,
                    "server": "https://cdaweb.gsfc.nasa.gov/hapi",
                    "dataset": "OMNI_HRO2_1MIN",
                    "cadence_suffix_from_catalog": false,
                    "cadence": "PT1M",
                    "time_parameter": "Time",
                    "parameter": "flow_speed",
                    "plot_options": {
                        "plot_type": "area",
                        "stroke": "#C8911E",
                        "stroke-width": "1",
                        "stroke-opacity": "1",
                        "fill": "#C8911E",
                        "fill-opacity": "0.5",
                        "mirror_zero_x": false
                    }
                }
            ]
        }
    ]
}
