#!/usr/bin/env python3
import os

stations = ['lyb0p',
            'dmc0p',
            'san0p',
            'nya0p',
            'sao0p',
            'lam0p',
            'mzs0p',
            'nya1p',
            'nic0p',
            'han0p',
            'thu0p',
            'ush0p',
            'tuc0p',
            'hel0p',
            'klu0p']

for station in stations:
    os.system(f'curl -X DELETE "http://macmini.local:9000/api-r/dataset?id=gnss_scint_{station}"')
