#!/usr/bin/env python3
import logging
import os
import json
import sys
# sys.path.append(os.environ['HOME'] + '/devel/space-weather-backoffice-data-store/')
from datastore.datastore import add_rdataset

format = '%(asctime)s [%(levelname).1s] %(message)s'
logging.basicConfig(format=format, level=logging.INFO,
                    datefmt='%Y-%m-%d %H:%M:%S')
dataset_dir = os.path.dirname(os.path.realpath(__file__)) + '/../datasets'
for file in os.listdir(dataset_dir):
    if file.startswith('rdataset_') and file.endswith('.json'):
        with open(os.path.join(dataset_dir, file)) as json_file:
            logging.info('Adding dataset: ' + file)
            rdataset = json.load(json_file)
            add_rdataset(rdataset)
