#!/usr/bin/env python3
import os

stations = ['lyb0p',
            'dmc0p',
            'san0p',
            'nya0p',
            'sao0p',
            'lam0p',
            'mzs0p',
            'nya1p',
            'nic0p',
            'han0p',
            'thu0p',
            'ush0p',
            'tuc0p',
            'hel0p',
            'klu0p']

areas =     {'btn0s': 'ANTARCTIC',
             'dmc0s': 'ANTARCTIC',
             'nya1s': 'ARCTIC',
             'kil0n': 'AFRICA',
             'lyb0p': 'ARCTIC',
             'dmc0p': 'ANTARCTIC',
             'san0p': 'ANTARCTIC',
             'nya0p': 'ARCTIC',
             'sao0p': 'SOUTH AMERICA',
             'dmc1s': 'ANTARCTIC',
             'dmc2s': 'ANTARCTIC',
             'lam0p': 'MEDITERRANEAN',
             'mzs0p': 'ANTARCTIC',
             'nya0s': 'ARCTIC',
             'nya1p': 'ARCTIC',
             'lam0s': 'MEDITERRANEAN',
             'lyb0s': 'ARCTIC',
             'nic0p': 'MEDITERRANEAN',
             'han0p': 'SOUTH EAST ASIA',
             'thu0p': 'ARCTIC',
             'ush0p': 'SOUTH AMERICA',
             'tuc0p': 'SOUTH AMERICA',
             'hel0p': 'EUROPE',
             'klu0p': 'SOUTH EAST ASIA'}

stations = sorted(stations, key=lambda x: areas[x])

rdataset_template = """{
    "id": "gnss_scint_{station}",
    "description": "GNSS scintillation for the {station} station",
    "timeStampLocation": "begin",
    "resourceURL": "http://eswua.ingv.it/",
    "resourceID": "INGV ESWUA",
    "contact": "",
    "contactID": "",
    "resampleMethod": "max",
    "subsets": [
        {
            "cadence": "PT1M",
            "additionalDescription": ""
        },
        {
            "cadence": "PT5M",
            "additionalDescription": "downsampled to 5 minutes"
        },
        {
            "cadence": "PT30M",
            "additionalDescription": "downsampled to 30 minutes"
        },
        {
            "cadence": "PT3H",
            "additionalDescription": "downsampled to 3 hours"
        },
        {
            "cadence": "PT12H",
            "additionalDescription": "downsampled to 12 hours"
        },
        {
            "cadence": "P3D",
            "additionalDescription": "downsampled to 3 days"
        },
        {
            "cadence": "P10D",
            "additionalDescription": "downsampled to 10 days"
        }
    ],
    "parameters": [
        {
            "name": "time",
            "type": "isotime",
            "units": "UTC",
            "fill": "",
            "description": "Time",
            "label": "",
            "key": true
        },
        {
            "name": "phi60l1slant_max",
            "type": "double",
            "units": "",
            "fill": null,
            "description": "Maximum phi60l1slant",
            "label": "",
            "key": false
        },
        {
            "name": "totals4l1_max",
            "type": "double",
            "units": "",
            "fill": null,
            "description": "Maximum totals4l1",
            "label": "",
            "key": false
        },
        {
            "name": "phi60l1slant_sats_mod",
            "type": "double",
            "units": "",
            "fill": null,
            "description": "Number of satellites measuring phi60l1slant over the mod threshold",
            "label": "",
            "key": false
        },
        {
            "name": "phi60l1slant_sats_sev",
            "type": "double",
            "units": "",
            "fill": null,
            "description": "Number of satellites measuring phi60l1slant over the sev threshold",
            "label": "",
            "key": false
        },
        {
            "name": "totals4l1_sats_mod",
            "type": "double",
            "units": "",
            "fill": null,
            "description": "Number of satellites measuring totals4l1 over the mod threshold",
            "label": "",
            "key": false
        },
        {
            "name": "totals4l1_sats_sev",
            "type": "double",
            "units": "",
            "fill": null,
            "description": "Number of satellites measuring totals4l1 over the sev threshold",
            "label": "",
            "key": false
        }
    ]
}
"""

layout_template_start = """    {
    "tmin": "2022-01-01T00:00:00Z",
    "tmax": "PT12H",
    "yautoscaling": true,
    "autoupdate": 0,
    "subplots": [
        {
            "type": "kp_timeseries",
            "subplotid": "kp_index",
            "title": "Kp index",
            "ymin": 0,
            "ymax": 9,
            "ydynamic": false,
            "ytickformat": "1d",
            "xaxisannotation": "hidden",
            "height": 100,
            "elements": [
                {
                    "server": "/hapi-r",
                    "dataset": "kp_index",
                    "parameters": "kp_index",
                    "time": "time",
                    "barpadding": 0.1,
                    "showkptext": true,
                    "datatype": "gfz"
                }
            ]
        },
        {
            "type": "timeseries",
            "title": "GOES X-ray flux",
            "subplotid": "xrayflux",
            "ymin": 1e-9,
            "ymax": 1e-2,
            "ydynamic": false,
            "ylog": true,
            "ytickformat": "1.0e",
            "xaxisannotation": "hidden",
            "height": 140,
            "elements": [
                {
                    "server": "/hapi-r",
                    "dataset": "xray_flux_rt",
                    "parameters": "xray_flux_long",
                    "time": "time",
                    "stroke": "#378194",
                    "fill": "#378194",
                    "fill_opacity": "50%",
                    "mirror_area": false
                },
                {
                    "server": "/hapi-r",
                    "dataset": "xray_flux_combined",
                    "parameters": "xray_flux_long",
                    "time": "time",
                    "stroke": "#378194",
                    "fill": "#378194",
                    "fill_opacity": "50%",
                    "mirror_area": false
                }
            ]
        },
        {
            "type": "timeseries",
            "subplotid": "protonflux",
            "title": "Proton flux from GOES satellites",
            "ymin": 1e-2,
            "ymax": 1e4,
            "ydynamic": false,
            "ylog": true,
            "ytickformat": "1.0e",
            "xaxisannotation": "hidden",
            "height": 100,
            "elements": [
                {
                "server": "/hapi-r",
                "dataset": "proton_flux",
                "parameters": "proton_flux_10MeV",
                "time": "time",
                "stroke": "#3781C4",
                "stroke_width": "1pt",
                "fill": "#3781C4",
                "fill_opacity": "20%",
                "mirror_area": false
                },
                {
                "server": "/hapi-r",
                "dataset": "proton_flux",
                "parameters": "proton_flux_50MeV",
                "time": "time",
                "stroke": "#3781C4",
                "stroke_width": "1pt",
                "fill": "#3781C4",
                "fill_opacity": "20%",
                "mirror_area": false
                },
                {
                "server": "/hapi-r",
                "dataset": "proton_flux",
                "parameters": "proton_flux_100MeV",
                "time": "time",
                "stroke": "#3781C4",
                "stroke_width": "1pt",
                "fill": "#3781C4",
                "fill_opacity": "20%",
                "mirror_area": false
                }
            ]
        },"""

layout_template_per_station = """
        {
            "type": "timeseries",
            "subplotid": "gnss_scint_sigmaphi_{station}_num",
            "title": "{area} {station}: Number of sats above mod/sev threshold",
            "ymin": 0,
            "ymax": 8,
            "ydynamic": false,
            "ylog": false,
            "ytickformat": "",
            "xaxisannotation": "hidden",
            "height": 100,
            "elements": [
                {
                    "server": "/hapi-r",
                    "dataset": "gnss_scint_{station}",
                    "parameters": "phi60l1slant_sats_mod",
                    "time": "time",
                    "stroke": "orange",
                    "stroke_width": "1pt",
                    "fill": "orange",
                    "fill_opacity": "50%",
                    "mirror_area": false
                },
                {
                    "server": "/hapi-r",
                    "dataset": "gnss_scint_{station}",
                    "parameters": "phi60l1slant_sats_sev",
                    "time": "time",
                    "stroke": "red",
                    "stroke_width": "1pt",
                    "fill": "red",
                    "fill_opacity": "50%",
                    "mirror_area": false
                }
            ]
        },
"""

layout_template_end = """
        {
            "type": "symbols",
            "subplotid": "icao_advisories",
            "title": "ICAO space weather advisories",
            "ymin": -1,
            "ymax": 1,
            "ydynamic": false,
            "ylog": false,
            "ytickformat": "",
            "xaxisannotation": "visible",
            "height": 100,
            "elements": [
                {
                    "server": "/static/json/icao_advisories.json"
                }
            ]
        }
    ]
}"""

for station in stations:
    rdataset_filename = f'{os.environ["HOME"]}/devel/sw_timeline_viewer/datasets/rdataset_gnss_scint_{station}.json'
    with open(rdataset_filename, 'w') as fh:
        fh.write(rdataset_template.replace('{station}', station))

layout_filename = f'{os.environ["HOME"]}/devel/sw_timeline_viewer/static/json/icao_scint.json'
with open(layout_filename, 'w') as fh:
    fh.write(layout_template_start)
    for station in stations:
        fh.write(layout_template_per_station.replace('{station}', station).replace('{area}', areas[station]))
    fh.write(layout_template_end)
