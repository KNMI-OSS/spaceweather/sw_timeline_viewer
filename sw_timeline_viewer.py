#!/usr/bin/env python3

import click
import json
import os
import logging
import threading
import time
import socket
from flask import Flask, request, Response, send_from_directory
from flask_cors import CORS, cross_origin
import traceback
from datastore import datastore
from datastore.ingestors import ingest_update
from datastore.config import config

app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

# ------------------------------------------------------------------------------

@app.route("/")
@cross_origin()
def root():
    # return app.send_static_file('html/heliophysics_data_viewer.html')
    return send_from_directory('frontend/public', 'index.html')

# ------------------------------------------------------------------------------

@app.route("/global.css")
@cross_origin()
def globalcss():
    return send_from_directory('frontend/public', 'global.css')

# ------------------------------------------------------------------------------

@app.route("/build/<path:filename>")
@cross_origin()
def sveltebuildfiles(filename):
    return send_from_directory('frontend/public/build', filename)

# ------------------------------------------------------------------------------

@app.route("/v2/<path:filename>")
@cross_origin()
def hapi_viewer_v2(filename):
    return send_from_directory('dist', filename)

# ------------------------------------------------------------------------------

@app.route("/favicon.ico")
def favicon():
    return app.send_static_file('img/favicon.ico')

# ------------------------------------------------------------------------------

def update_datastore():
    while True:
        time.sleep(300) # every 5 minutes
        logging.info("Checking to update datastore");
        try:
            ingest_update.ingest_update()
        except Exception as e:
            logging.error('Update failed: ' + str(e))

# ------------------------------------------------------------------------------

# below here are the (H)API endpoints

hapi_version = '2.1.1'

api_status_codes = {
    "1200": {"http_code": 200, "status":{"code": 1200, "message": "OK"}},
    "1201": {"http_code": 200, "status":{"code": 1201, "message": "OK - no data"}},
    "1400": {"http_code": 400, "status":{"code": 1400, "message": "error: user input error"}},
    "1401": {"http_code": 400, "status":{"code": 1401, "message": "error: unknown API parameter name"}},
    "1402": {"http_code": 400, "status":{"code": 1402, "message": "error: error in time.min"}},
    "1403": {"http_code": 400, "status":{"code": 1403, "message": "error: error in time.max"}},
    "1404": {"http_code": 400, "status":{"code": 1404, "message": "error: time.min equal to or after time.max"}},
    "1405": {"http_code": 400, "status":{"code": 1405, "message": "error: time outside valid range"}},
    "1406": {"http_code": 400, "status":{"code": 1406, "message": "error: unknown dataset id"}},
    "1407": {"http_code": 400, "status":{"code": 1407, "message": "error: unknown dataset parameter"}},
    "1408": {"http_code": 400, "status":{"code": 1408, "message": "error: too much time or data requested"}},
    "1409": {"http_code": 400, "status":{"code": 1409, "message": "error: unsupported output format"}},
    "1410": {"http_code": 400, "status":{"code": 1410, "message": "error: unsupported include value"}},
    "1411": {"http_code": 400, "status":{"code": 1411, "message": "error: out of order or duplicate parameters"}},
    "1500": {"http_code": 500, "status":{"code": 1500, "message": "error: internal server error"}},
    "1501": {"http_code": 500, "status":{"code": 1501, "message": "error: upstream request error"}}
}

# ------------------------------------------------------------------------------

def getResponse(status, message, hapi = False):
    if hapi:
        message['HAPI'] = hapi_version
    message['status'] = api_status_codes[status]['status']
    http_code = api_status_codes[status]['http_code']
    response = json.dumps(message, indent=4) + '\n'
    return Response(response, status=http_code, mimetype='application/json')

# ------------------------------------------------------------------------------

def getCsvResponse(status, message, header, hapi = False):
    http_code = api_status_codes[status]['http_code']
    if header == {}:
        return Response(message, status=http_code, mimetype='text/csv')
    if type(header) != str:
        if hapi:
            header['HAPI'] = hapi_version
        header['status'] = api_status_codes[status]['status']
        header = json.dumps(header, indent=4)
        lines = header.split('\n')
        header = ''
        for line in lines:
            header += '#' + line + '\n'
    else:
        header = header + '\n'
    return Response(header + message, status=http_code, mimetype='text/csv')

# ------------------------------------------------------------------------------

@app.route('/hapi/capabilities', methods=['GET'])
def hapi_capabilities():

    try:
        status, reply = datastore.get_capabilities(request.args)
        return getResponse(status, reply, hapi=True)

    except:
        logging.info(traceback.format_exc())
        return getResponse('1500', {}, hapi=True)

# ------------------------------------------------------------------------------

@app.route('/hapi/catalog', methods=['GET'])
def hapi_catalog():

    try:
        status, reply = datastore.get_datasets(request.args, 'catalog')
        return getResponse(status, reply, hapi=True)

    except:
        logging.info(traceback.format_exc())
        return getResponse('1500', {}, hapi=True)

# ------------------------------------------------------------------------------

@app.route('/hapi/info', methods=['GET'])
def hapi_info():

    try:
        status, reply = datastore.get_dataset(request.args)
        return getResponse(status, reply, hapi=True)

    except:
        logging.info(traceback.format_exc())
        return getResponse('1500', {}, hapi=True)

# ------------------------------------------------------------------------------

@app.route('/hapi/data', methods=['GET'])
def hapi_data():

    try:
        status, format, reply, reply_header = datastore.get_data(request.args)
        if format == 'json':
            return getResponse(status, reply, hapi=True)

        elif format == 'csv':
            return getCsvResponse(status, reply, reply_header, hapi=True)

    except:
        logging.info(traceback.format_exc())
        return getResponse('1500', {}, hapi=True)

# ------------------------------------------------------------------------------

@app.route('/api/capabilities', methods=['GET'])
def api_capabilities():

    try:
        status, reply = datastore.get_capabilities(request.args)
        return getResponse(status, reply)

    except:
        logging.info(traceback.format_exc())
        return getResponse('1500', {})

# ------------------------------------------------------------------------------

@app.route('/api/datasets', methods=['GET', 'POST'])
def api_datasets():

    try:
        if request.method == 'GET':
            status, reply = datastore.get_datasets(request.args, 'datasets')
            return getResponse(status, reply)

        if request.method == 'POST':
            status, reply = datastore.add_dataset(request.json)
            return getResponse(status, reply)

    except:
        logging.info(traceback.format_exc())
        return getResponse('1500', {})

# ------------------------------------------------------------------------------

@app.route('/api/dataset', methods=['GET', 'PUT', 'DELETE', 'POST'])
def api_dataset():

    try:
        if request.method == 'GET':
            status, reply = datastore.get_dataset(request.args)
            return getResponse(status, reply)

        if request.method == 'PUT':
            status, reply = datastore.add_data(request.json, replace=True)
            return getResponse(status, reply)

        if request.method == 'DELETE':
            status, reply = datastore.remove_dataset(request.args)
            return getResponse(status, reply)

        if request.method == 'POST':
            status, reply = datastore.add_data(request.json)
            return getResponse(status, reply)

    except:
        logging.info(traceback.format_exc())
        return getResponse('1500', {})

# ------------------------------------------------------------------------------

@app.route('/api/data', methods=['GET', 'DELETE'])
def api_data():

    try:
        if request.method == 'GET':
            status, format, reply, reply_header = datastore.get_data(request.args)
            if format == 'json':
                return getResponse(status, reply)

            elif format == 'csv':
                return getCsvResponse(status, reply, reply_header)

        if request.method == 'DELETE':
            status, reply = datastore.remove_data(request.args)
            return getResponse(status, reply)

    except:
        logging.info(traceback.format_exc())
        return getResponse('1500', {})

# ------------------------------------------------------------------------------

@app.route('/hapi-r/capabilities', methods=['GET'])
@cross_origin()
def hapi_r_capabilities():

    try:
        status, reply = datastore.get_capabilities(request.args)
        return getResponse(status, reply, hapi=True)

    except:
        logging.info(traceback.format_exc())
        return getResponse('1500', {}, hapi=True)

# ------------------------------------------------------------------------------

@app.route('/hapi-r/catalog', methods=['GET'])
@cross_origin()
def hapi_r_catalog():

    try:
        status, reply = datastore.get_rdatasets(request.args, 'catalog')
        return getResponse(status, reply, hapi=True)

    except:
        logging.info(traceback.format_exc())
        return getResponse('1500', {}, hapi=True)

# ------------------------------------------------------------------------------

@app.route('/hapi-r/info', methods=['GET'])
@cross_origin()
def hapi_r_info():

    try:
        status, reply = datastore.get_rdataset(request.args)
        return getResponse(status, reply, hapi=True)

    except:
        logging.info(traceback.format_exc())
        return getResponse('1500', {}, hapi=True)

# ------------------------------------------------------------------------------

@app.route('/hapi-r/data', methods=['GET'])
@cross_origin()
def api_r_data():

    try:
        status, format, reply, reply_header = datastore.get_rdata(request.args)
        if format == 'json':
            return getResponse(status, reply, hapi=True)

        elif format == 'csv':
            return getCsvResponse(status, reply, reply_header, hapi=True)

    except:
        logging.info(traceback.format_exc())
        return getResponse('1500', {}, hapi=True)

# ------------------------------------------------------------------------------

@app.route('/api-r/capabilities', methods=['GET'])
@cross_origin()
def api_r_capabilities():

    try:
        status, reply = datastore.get_capabilities(request.args)
        return getResponse(status, reply)

    except:
        logging.info(traceback.format_exc())
        return getResponse('1500', {})

# ------------------------------------------------------------------------------

@app.route('/api-r/datasets', methods=['GET', 'POST'])
@cross_origin()
def api_rdatasets():

    try:
        if request.method == 'GET':
            status, reply = datastore.get_rdatasets(request.args, 'datasets')
            return getResponse(status, reply)

        if request.method == 'POST':
            status, reply = datastore.add_rdataset(request.json)
            return getResponse(status, reply)

    except:
        logging.info(traceback.format_exc())
        return getResponse('1500', {})

# ------------------------------------------------------------------------------

@app.route('/api-r/dataset', methods=['GET', 'PUT', 'DELETE', 'POST'])
@cross_origin()
def api_rdataset():

    try:
        if request.method == 'GET':
            status, reply = datastore.get_rdataset(request.args)
            return getResponse(status, reply)

        if request.method == 'PUT':
            status, reply = datastore.add_rdata(request.json, replace=True)
            return getResponse(status, reply)

        if request.method == 'DELETE':
            status, reply = datastore.remove_rdataset(request.args)
            return getResponse(status, reply)

        if request.method == 'POST':
            status, reply = datastore.add_rdata(request.json)
            return getResponse(status, reply)

    except:
        logging.info(traceback.format_exc())
        return getResponse('1500', {})

# ------------------------------------------------------------------------------

@app.route('/api-r/data', methods=['GET', 'DELETE'])
@cross_origin()
def api_rdata():

    try:
        if request.method == 'GET':
            status, format, reply, reply_header = datastore.get_rdata(request.args)
            if format == 'json':
                return getResponse(status, reply)

            elif format == 'csv':
                return getCsvResponse(status, reply, reply_header)

        if request.method == 'DELETE':
            status, reply = datastore.remove_data(request.args)
            return getResponse(status, reply)

    except:
        logging.info(traceback.format_exc())
        return getResponse('1500', {})

# ------------------------------------------------------------------------------

def secho(text, file=None, nl=None, err=None, color=None, **styles):
    pass

# ------------------------------------------------------------------------------

def echo(text, file=None, nl=None, err=None, color=None, **styles):
    pass

# ------------------------------------------------------------------------------

if __name__ == "__main__":

    log_filename = 'sw_viewer.log'

    format = '%(asctime)s [%(levelname).1s] %(message)s'
    logging.basicConfig(filename=log_filename, format=format, level=logging.INFO, datefmt='%Y-%m-%d %H:%M:%S')

    print('')
    print('')
    print('KNMI Space Weather Back-Office Data Server')
    print('version 1.0.0 (2021-07-28)')
    print('')

    # try:
    #     config, config_file = datastore.config.loadConfig()
    #     print("Loading configuration from " + config_file)
    # except Exception as error:
    #     print(error)
    #     sys.exit()

    print('Using database ' + config['database_path'])
    if config['datastore_update']:
        print('Starting automatic Datastore update')
        thread = threading.Thread(target=update_datastore)
        thread.daemon = True
        thread.start()
    else:
        print('Automatic Datastore update disabled')

    print('')
    print('Starting API server on http://' + socket.gethostname() + ':' + str(config['server_port']))
    print('Stop server using: kill ' + str(os.getpid()))
    print('')

    print('Logging goes to ' + log_filename)

    click.echo = echo
    click.secho = secho

#    app.run(host=socket.gethostname(), port=config['server_port'], debug=config['server_debug'],
#        use_reloader=True, threaded=True)
    app.run(host="0.0.0.0", port=config['server_port'], debug=config['server_debug'],
        use_reloader=False, threaded=True)

# ------------------------------------------------------------------------------
