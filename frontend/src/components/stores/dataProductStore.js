import { writable, derived, get } from 'svelte/store';
import { asyncDerived, asyncWritable } from '@square/svelte-store'
import { csv, csvParseRows } from 'd3';
import { DateTime } from 'luxon';
import { layoutData, timeDomain } from './layoutStore.js';
import dayjs from 'dayjs';
import duration from 'dayjs/plugin/duration';
import utc from 'dayjs/plugin/utc';
dayjs.extend(duration);
dayjs.extend(utc);

function parse_server(server) {
    if (server.includes("localhost") & server.includes("hapi")) {
        // Use a relative URL when the hapi server is set to localhost.
        // In this case we want to use the same server for both HAPI and the frontend content.
        return '/hapi';
    } else {
        return server;
    }
}


export const selected_issue_timetag = writable({});

export const hapiServerInfo = asyncDerived(
    layoutData,
    async function($layoutData) {
        let servers = {}; // this will be filled asynchronously when fetches are ready
        // this list of servers is build up directly, used so that we do not have to check the same catalog twice
        let checkedServers = [];
        for (const subplot of $layoutData.subplots) {
            for (const el of subplot.plot_elements) {
                if (!(checkedServers.includes(el.server)) && el.server.endsWith("hapi")) {
                    let url = parse_server(el.server) + '/catalog';
                    let response = await fetch(url);
                    if (response.ok) {
                        let parsedJSON = await response.json();
                        servers[el.server] = parsedJSON.catalog;
                    } else {
                        throw new Error(response.statusText);
                    }
                    checkedServers.push(el.server);
                }
            }
        }
        return servers;
    }, true
);

export const hapiServerInfoCadences = asyncDerived(
    hapiServerInfo,
    async function($hapiServerInfo) {
        let output = {};
        for (const server in $hapiServerInfo) {
            output[server] = {};
            for (const i in $hapiServerInfo[server]) {
                let dataset_id = $hapiServerInfo[server][i]['id'];
                let cadence_info = idSplitCadence(dataset_id);
                try {
                    if (!(cadence_info.dataset_id in output[server])) {
                        // Create the key for the dataset if it does not exist
                        output[server][cadence_info.dataset_id] = [];
                    }
                    output[server][cadence_info.dataset_id].push(cadence_info.cadence_info);
                } catch(error) {
                    console.error("Error retrieving cadence: ",  error);
                }
            }
        }
        return output;
    }
);

function idSplitCadence(dataset_id) {
    // HAPI dataset_ids are strings of the form: solar_wind_plasma_PT3H
    // This function takes a dataset_id, and checks whether the last part
    // (if split by underscores) is an ISO duration string. If so, parse the
    // ISO duration and return a cleaned-up dataset_id, together with the
    // duration string and parsed duration.
    const split_id = dataset_id.split('_');
    const last_part = split_id[split_id.length-1];
    // Check if last part is a cadence
    const cadence_sec = dayjs.duration(last_part).asSeconds()
    if (isNaN(cadence_sec)) {
        return {'dataset_id': dataset_id,
                'cadence_info': {'cadence_string': undefined,
                                 'cadence_sec': undefined}};
    } else {
        return {'dataset_id': split_id.slice(0, split_id.length-1).join("_"),
                'cadence_info': {'cadence_string': last_part,
                                 'cadence_sec': cadence_sec}};
    }
}

function cadencesInCatalog(dataset_id, catalog) {
    // HAPI dataset_ids are strings of the form: solar_wind_plasma_PT3H
    // This function takes a dataset_id and HAPI product catalog, and looks
    // for instances of the same dataset_id with different duration suffixes.
    let id_components = idSplitCadence(dataset_id);
    let cadence_suffixes = [];
    if (catalog) {
        // Loop over items in catalog and collect available cadences (if any)
        for (const catalogItem of catalog) {
            if (catalogItem.id.startsWith(id_components.dataset_id)) {
                const catalog_id_components = idSplitCadence(catalogItem.id);
                if (catalog_id_components.dataset_id == id_components.dataset_id &&
                    catalog_id_components.cadence_info.cadence_string) {
                    cadence_suffixes.push(catalog_id_components.cadence_info);
                }
            }
        }
    } else {
        // No catalog available, return info based on dataset_id only
        // This is one cadence only, which could be undefined.
        cadence_suffixes = [id_components.cadence_info];
    }
    return {'dataset_id': id_components.dataset_id,
            'cadence_suffixes': cadence_suffixes.sort((a,b) => {return a.cadence_sec-b.cadence_sec})};
}

export const plotElements = derived(layoutData, function($layoutData) {
    let result = [];
    for (const subplot of $layoutData.subplots) {
        for (const el of subplot.plot_elements) {
            result.push(el);
        }
    }
    return result;
})

export const dataProductInfo = asyncDerived([plotElements, hapiServerInfo], async function([$plotElements, $hapiServerInfo]) {
    // Some plot elements might make use of the same database table, but use different columns from that table.
    // So for efficiency, they are collected and combined here into a master list.
    let products = {};
    for (const el of $plotElements) {
        if (el.dataset.endsWith('.json')) {
            continue
            // .json data, such as for ICAO advisories, are read directly from the server, no use to get info for HAPI request
        }
        let cadence_suffixes, dataset_id;
        // Look up if there are multiple versions of this product at different cadences
        if (el.cadence_suffix_from_catalog) {
            let cadence_suffix_info = cadencesInCatalog(el.dataset, $hapiServerInfo[el.server]);
            cadence_suffixes = cadence_suffix_info.cadence_suffixes;
            dataset_id = cadence_suffix_info.dataset_id;
        } else {
            cadence_suffixes = [];
            dataset_id = el.dataset;
        }
        if (!(dataset_id in products)) {
            // Create the product info and its local data store
            products[dataset_id] = {
                           "server": el.server,
                           "parameters_unsorted": [el.parameter],
                           "cadence_suffix_from_catalog": el.cadence_suffix_from_catalog,
                           "cadences": cadence_suffixes};
            try {
                let info_url = parse_server(el.server) + "/info?id=" + dataset_id;
                if (cadence_suffixes.length > 0) {
                    info_url = info_url + "_" + cadence_suffixes[0].cadence_string;
                }
                let response = await fetch(info_url);
                if (!response.ok) {
                    throw new Error(response.statusText);
                } else {
                    let parsedJSON = await response.json();
                    products[dataset_id].info = parsedJSON;
                    if (cadence_suffixes.length == 0) { // add cadence information from hapi info json
                        let cadence_str;
                        if ('cadence' in products[dataset_id].info) {
                            cadence_str = products[dataset_id].info.cadence;
                        } else if ('cadence' in el) {
                            cadence_str = el.cadence
                        } else {
                            console.error("Unable to find cadence info for " + datset_id);
                        }
                        cadence_suffixes.push({'cadence_string': cadence_str,
                                               'cadence_sec': dayjs.duration(cadence_str).asSeconds()})
                    }
                }
            } catch (error) {
                console.error(error);
            }
        } else {
            // Add additional main parameters
            if (!(el.parameter in products[dataset_id].parameters_unsorted)) {
                products[dataset_id].parameters_unsorted.push(el.parameter);
            }
        }
        // Add secondary parameters, if available
        for (const par_field of ["issue_time_parameter", "baseline_parameter", "color_scale_parameter"]) {
            if (par_field in el) {
                if (!(par_field in products[dataset_id].parameters_unsorted)) {
                    products[dataset_id].parameters_unsorted.push(el[par_field]); // Add to the main list of parameters to download
                }
            }
        }
    }
    // Sort the parameters, so that they appear in the same order as in the hapi server info list
    try {
        for (const product_name in products) {
            let all_params = products[product_name].info.parameters.map(par => par.name)
            products[product_name]['parameters'] = all_params.filter(par => products[product_name].parameters_unsorted.includes(par));
            products[product_name]['types'] = {}
            products[product_name]['fill_values'] = {}
            for (const par of products[product_name]['info']['parameters']) {
                products[product_name]['types'][par.name] = par.type;
                products[product_name]['fill_values'][par.name] = par.fill;
            }
        }
    } catch(error) {
        console.error(error);
    }
    return products;
});

export const urlsToLoad = asyncDerived(
    [dataProductInfo, timeDomain],
    async function([$dataProductInfo, $timeDomain]) {
        const maxDataPointsInView = 2000;
        let time_in_view = $timeDomain[1]-$timeDomain[0];
        let urls = {};
        for (const product_id in $dataProductInfo) {
            let product = $dataProductInfo[product_id];
            for (const cadence of product.cadences) {
                let dataPointsInView = time_in_view / (cadence.cadence_sec*1e3);
                // Select the highest cadence data that does not result in exceeding
                // the maximum data points in view
                if (dataPointsInView < maxDataPointsInView) {
                    let t0 = dayjs($timeDomain[0]).subtract(0.5*time_in_view, 'ms').subtract(cadence.cadence_sec*1e3);
                    let t1 = dayjs($timeDomain[1]).add(0.5*time_in_view, 'ms').add(cadence.cadence_sec*1e3);
                    let dataset_id;
                    if (product.cadence_suffix_from_catalog) {
                        dataset_id = product_id + "_" + cadence.cadence_string;
                    } else {
                        dataset_id = product_id;
                    }
                    let url = parse_server(product.server) + "/data?id=" + dataset_id +
                        "&parameters=" + product.parameters.join(",") +
                        "&time.min=" + t0.toISOString().slice(0, 19) + 'Z' +
                        "&time.max=" + t1.toISOString().slice(0, 19) + 'Z';
                    urls[product_id] = {'url': url, 'parameters': product.parameters, 'cadence': cadence};
                    break;
                }
            }
        }
        return urls;
    }
);

export const dataStore = asyncDerived(
    [urlsToLoad, dataProductInfo],
    async function([$urlsToLoad, $dataProductInfo]) {
        let data = {};
        let urls = $urlsToLoad;
        for (const product_id in $urlsToLoad) {
            data[product_id] = {};
            data[product_id]['cadence'] = $urlsToLoad[product_id]['cadence'];
            let response = await fetch($urlsToLoad[product_id]['url']);
            if (!response.ok) {
                throw new Error(response.statusText);
            } else {
                let csvtext = await response.text();
                data[product_id]['data'] = csvParseRows(csvtext, (csvcolumns, i) => {

                    // Loop over the rows in the CSV file
                    // and assign the values to their named parameters.
                    // For instance, a CSV file that looks like this:
                    // 2022-08-07T00:00:00Z,1
                    // 2022-08-07T03:00:00Z,2
                    // Will be converted to this:
                    // [{time: '2022-08-07T00:00:00Z', kp_index: 1},
                    //  {time: '2022-08-07T03:00:00Z', kp_index: 2]

                    let obj = {};
                    obj['time'] = DateTime.fromISO(csvcolumns[0]).toJSDate();
                    for (let i = 0; i < $urlsToLoad[product_id]['parameters'].length; i++) {
                        let par = $urlsToLoad[product_id]['parameters'][i];
                        let fill = $dataProductInfo[product_id]['fill_values'][par];
                        let type = $dataProductInfo[product_id]['types'][par];
                        let columntext = csvcolumns[i+1];
                        if (columntext == "" || columntext == fill) {
                            obj[$urlsToLoad[product_id]['parameters'][i]] = NaN;
                        } else if (type == "isotime") {
                            // Keep date as text string, for exact comparison with selected forecast date
                            obj[$urlsToLoad[product_id]['parameters'][i]] = columntext;
                            // obj[$urlsToLoad[product_id]['parameters'][i]] = DateTime.fromISO(columntext).toJSDate();;
                        } else if (type == "string") {
                            obj[$urlsToLoad[product_id]['parameters'][i]] = columntext;
                        } else {
                            obj[$urlsToLoad[product_id]['parameters'][i]] = +columntext;
                        }
                    }
                    return obj;
                });
            }
        }
        return data;
    }
);
