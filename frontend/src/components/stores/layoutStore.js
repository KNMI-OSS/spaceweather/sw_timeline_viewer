import dayjs from 'dayjs';
import { writable } from 'svelte/store';
import { asyncDerived, asyncWritable, derived } from '@square/svelte-store'
import { scaleLinear, scaleLog } from 'd3';

export const layoutURL = writable('/static/json/layout.json');
export const timeDomain = writable([undefined, undefined]);
export const secondsPerPixel = writable(1);

export const containerHeight = writable(1000);
export const containerWidth = writable(1000);

let t0 = dayjs().subtract(6, 'days');
let t1 = dayjs().add(3, 'days');
timeDomain.set([t0, t1]);

// Compute the display-height and y-coordinates of each plotting area from their given height and margins
const computeSubplotLayouts = function(layoutData, windowInnerHeight) {
    // The total height of the plot area and margins are needed to compute the display height,
    // if the fillVertical option is used to make them fill up the available vertical space
    let subplots = layoutData.subplots;
    let fillVertical = layoutData.options.fill_vertical;
    let tickSpacing = 25;
    let totalPlotAreaHeight = 0;
    let totalMarginHeight = 0;

    // First compute the total specified height
    subplots.forEach(function(subplot) {
      if (subplot.visible) {
        totalPlotAreaHeight = totalPlotAreaHeight + subplot.height;
        totalMarginHeight = totalMarginHeight + subplot.margins.top + subplot.margins.bottom;
      }
    });
    let totalHeights = {plotarea: totalPlotAreaHeight, margins: totalMarginHeight};

    // Now compute the subplot layout
    let ynew = 0;
    let y;
    let h;
    let subplotLayouts = [];

    subplots.forEach(function(subplot, i) {
        y = ynew + subplot.margins.top;
        if (fillVertical) {
            h = subplot.height * (windowInnerHeight-totalHeights.margins)/totalHeights.plotarea;
        } else {
            h = subplot.height;
        }

        // Y-axis
        let yScale;
        let ticks;
        let logTicks;

        let domain;
        let range = [y+h, y];
        if (subplot.scale === "log10") {
            domain = [Math.pow(10, subplot.ydomain.default[0]), Math.pow(10, subplot.ydomain.default[1])];
            yScale = scaleLog().domain(domain).range(range);
            ticks = yScale.ticks(h/tickSpacing);
            logTicks = ticks.map(logComponents);
        } else {
            domain = [subplot.ydomain.default[0], subplot.ydomain.default[1]];
            yScale = scaleLinear().domain(domain).range(range);
            ticks = yScale.ticks(h/tickSpacing);
        }
        subplotLayouts[i] = subplot;
        subplotLayouts[i]['id'] = i;
        subplotLayouts[i]['y'] = y;
        subplotLayouts[i]['h'] = h,
        subplotLayouts[i]['yScale'] = yScale;
        subplotLayouts[i]['ticks'] = ticks;
        subplotLayouts[i]['logTicks'] = logTicks;
        subplotLayouts[i]['visible'] = subplot.visible;

        if (subplotLayouts[i].visible) {
          ynew = ynew + subplot.margins.top + h + subplot.margins.bottom;
        } else {
          ynew = ynew;
        }
    });
    return subplotLayouts;
}


function logComponents(value) {
    let logValue = Math.log10(value);
    let exponent = Math.floor(logValue);
    let multiplier = Math.pow(10, -1*exponent+logValue).toFixed(0);
    return {'value': value, 'exponent': exponent, 'multiplier': multiplier};
}


export const layoutData = asyncWritable(
    layoutURL,
    async function($layoutURL) {
        const response = await fetch($layoutURL);
        if (response.ok) {
            try {
              const layout_object = await response.json();
              return layout_object;
            } catch(error) {
              console.error(error);
            }
        } else {
            throw new Error(response.statusText)
        }
    } //,
    // async function(newLayout) {
        // Optionally add code here to save the layout in local storage when settings are changed by the user,
        // or upload to a server
        // console.log(newLayout);
    // }
);


export const globalMarginLeft = asyncDerived(
  layoutData,
  async function($layoutData) {
     return $layoutData.global_margins.left;
  }
)


export const subplotLayouts = asyncDerived(
  [layoutData, containerHeight],
  function([$layoutData, $containerHeight]) {
      return computeSubplotLayouts($layoutData, $containerHeight);
  }, true
);
