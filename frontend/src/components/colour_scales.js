import {
interpolateBrBg,
interpolatePRGn,
interpolatePiYG,
interpolatePuOr,
interpolateRdBu,
interpolateRdGy,
interpolateRdYlBu,
interpolateRdYlGn,
interpolateSpectral,
interpolateBlues,
interpolateGreens,
interpolateGreys,
interpolateOranges,
interpolatePurples,
interpolateReds,
interpolateTurbo,
interpolateViridis,
interpolateInferno,
interpolateMagma,
interpolatePlasma,
interpolateCividis,
interpolateWarm,
interpolateCool,
interpolateCubehelixDefault,
interpolateBuGn,
interpolateBuPu,
interpolateGnBu,
interpolateOrRd,
interpolatePuBuGn,
interpolatePuBu,
interpolatePuRd,
interpolateRdPu,
interpolateYlGnBu,
interpolateYlGn,
interpolateYlOrBr,
interpolateYlOrRd,
interpolateRainbow,
interpolateSinebow
} from 'd3';

export const cmapnames = [
	'BrBg', 'PRGn', 'PiYG', 'PuOr', 'RdBu', 'RdGy', 'RdYlBu', 'RdYlGn',
	'Spectral', 'Blues', 'Greens', 'Greys', 'Oranges', 'Purples', 'Reds',
	'Turbo', 'Viridis', 'Inferno', 'Magma', 'Plasma', 'Cividis', 'Warm', 'Cool',
	'CubehelixDefault', 'BuGn', 'BuPu', 'GnBu', 'OrRd', 'PuBuGn', 'PuBu',
	'PuRd', 'RdPu', 'YlGnBu', 'YlGn', 'YlOrBr', 'YlOrRd', 'Rainbow', 'Sinebow'
]

export const interpolateColorNormalized = function(cmapname, t) {
	switch (cmapname) {
		case 'BrBg':
			return interpolateBrBg(t);
		case 'PRGn':
			return interpolatePRGn(t);
		case 'PiYg':
			return interpolatePiYG(t);
		case 'PuOr':
			return interpolatePuOr(t);
		case 'RdBu':
			return interpolateRdBu(t);
		case 'RdGy':
			return interpolateRdGy(t);
		case 'RdYlBu':
			return interpolateRdYlBu(t);
		case 'RdYlGn':
			return interpolateRdYlGn(t);
		case 'Spectral':
			return interpolateSpectral(t);
		case 'Blues':
			return interpolateBlues(t);
		case 'Greens':
			return interpolateGreens(t);
		case 'Greys':
			return interpolateGreys(t);
		case 'Oranges':
			return interpolateOranges(t);
		case 'Purples':
			return interpolatePurples(t);
		case 'Reds':
			return interpolateReds(t);
		case 'Turbo':
			return interpolateTurbo(t);
		case 'Viridis':
			return interpolateViridis(t);
		case 'Inferno':
			return interpolateInferno(t);
		case 'Magma':
			return interpolateMagma(t);
		case 'Plasma':
			return interpolatePlasma(t);
		case 'Cividis':
			return interpolateCividis(t);
		case 'Warm':
			return interpolateWarm(t);
		case 'Cool':
			return interpolateCool(t);
		case 'CubehelixDefault':
			return interpolateCubehelixDefault(t);
		case 'BuGn':
			return interpolateBuGn(t);
		case 'BuPu':
			return interpolateBuPu(t);
		case 'GnBu':
			return interpolateGnBu(t);
		case 'OrRd':
			return interpolateOrRd(t);
		case 'PuBuGn':
			return interpolatePuBuGn(t);
		case 'PuBu':
			return interpolatePuBu(t);
		case 'PuRd':
			return interpolatePuRd(t);
		case 'RdPu':
			return interpolateRdPu(t);
		case 'YlGnBu':
			return interpolateYlGnBu(t);
		case 'YlGn':
			return interpolateYlGn(t);
		case 'YlOrBr':
			return interpolateYlOrBr(t);
		case 'YlOrRd':
			return interpolateYlOrRd(t);
		case 'Rainbow':
			return interpolateRainbow(t);
		case 'Sinebow':
			return interpolateSinebo(t);
		default:
			return interpolateGreys(t);
	}
}

export const function interpolateColor = function(cmapname, value, vmin, vmax) {
	let t = (value - tmin) / (tmax - tmin);
	return interpolateColorNormalized(cmapname, t)
}