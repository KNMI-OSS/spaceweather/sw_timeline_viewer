# Notice: new repositories!
The development on this version of the space weather timeline viewer is about to stop, as the project has been split off into separate projects for the viewer, HAPI server, and in the future for (pre)processing Python scripts and associated libraries. Follow these links to the new project(s), as needed:
- KNMI HAPI timeline viewer: https://gitlab.com/KNMI-OSS/spaceweather/knmi-hapi-timeline-viewer
- KNMI HAPI server: https://gitlab.com/KNMI-OSS/spaceweather/knmi-hapi-timeline-server
- orbittools, Python package for reading, creating and manipulating orbit ephemeris data: https://gitlab.com/eelcodoornbos/orbittools

# Space Weather Timeline Viewer
Space Weather Viewer is a web-based application for visualizing space weather data. It focuses on a clean and crisp display of information, highlighting relationships between various data sources, and providing interactivity through smooth zooming and panning on time series data.

For details on the design and architecture, have a look at our [Poster Presented at the European Space Weather Week 2021](https://gitlab.com/KNMI-OSS/spaceweather/sw_timeline_viewer/-/blob/master/doc/presentations/esww2021_poster_eelcodoornbos_sw_timeline_viewer_small.pdf), and accompanying [demo video](https://www.youtube.com/watch?v=w9UfG5ASyvY).

[![Demo video screen shot](<doc/presentations/demo_video_screenshot.png>)](https://vimeo.com/751672108/bc133c4835)

The software includes a back-end written in Python (version 3.6 or higher). This back-end includes a data store and a web server, which serves the data, HTML, JavaScript and CSS for the front-end, which is then accessible from any web browser.

## Requirements
### For python
- pipenv: https://pipenv.pypa.io/en/latest/
- Further python requirements as listed in  `Pipfile`
### For javascript/svelte:
- nodejs and npm. nodejs v17.9.0 and npm 8.6.0 were used in development. These can be easily installed using NVM: https://github.com/nvm-sh/nvm
- Further javascript requirements as listed in `frontend/package.json`

## Quick start guide
Take the following steps to configure the viewer on your system:
- Move to the project directory.
- Install all packages needed in a pipenv virtual environment `pipenv install`.
- Use `datastore.cfg.example` to setup your own configuration file for the datastore (name it `datastore.cfg`).
- Make sure that Python also looks for modules and packages in the project directory by adding `export PYTHONPATH='/my/project_directory/sw_timeline_viewer'` to the bashrc (or zshrc, etc.) file and subsequently reloading it (`source activate ~/.bashrc`). This is needed because the datastore subdirectory is used as a python package.
- Install the frontend dependencies: `cd frontend; npm install; npm run build`. Alternatively, use `npm run dev` to use a live development server.
- Start the backend and flask server: `pipenv run sw_timeline_viewer.py`.
- Open your webbrowser and go the address listed in the terminal.
- Create the database tables by running `pipenv run scripts/setup_datastore.py`.
- Download real-time solar wind, GOES X-ray flux and Kp index data to populate the viewer `pipenv run datastore/ingestors/ingest_update.py`.
- Run additional ingestor scripts under `datastore/ingestors` if needed (e.g. `pipenv run datastore/ingestors/ingest_f10_7.py`).

## Known installation issues and workarounds

### Installing PyHDF and hdf4 on macOS

The PyHDF library can be used to access HDF4 files, used for storing ACE science data. PyHDF
requires the HDF4 libraries to be installed, which can be done using MacPorts:
``
When the HDF4 library is installed using MacPorts (recommended) or self-installed, it is necessary to
instruct pipenv where to find this library and its header file. This can be done by
passing LDFLAGS and CFLAGS on the command-line:
```
sudo port install hdf4
LDFLAGS='-L/opt/local/lib' CFLAGS='-I/opt/local/include' pipenv install pyhdf
```
